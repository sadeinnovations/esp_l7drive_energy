#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := l7drive_energy_firmware

TIMESTAMP := $(shell date +'%Y%m%d%H%M')
DEFAULT:
	make all
SET:
	@echo '// Auto-generated; modify manually only if you absolutely need to.\r\n#define FW_VERSION_MAJOR $(VERSION_MAJOR)\r\n#define FW_VERSION_MINOR $(VERSION_MINOR)\r\n#define FW_VERSION_TIMESTAMP "$(TIMESTAMP)"' > ./main/version.h

include $(IDF_PATH)/make/project.mk
