/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * Additions Copyright 2016 Espressif Systems (Shanghai) PTE LTD
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
/**
 * @file thing_shadow_sample.c
 * @brief A simple connected lock example demonstrating the use of Thing Shadow
 *
 * See example README for more details.
 */
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "esp32/pm.h"
#include "esp_bt.h"
#include "esp_err.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_pm.h"
#include "esp_system.h"
#include "esp_vfs_fat.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "AwsDataUpdate.h"
#include "CanController.h"

#include "LocationUpdate.h"
#include "Log.h"
#include "libGSM.h"
#include "DataStorageHandler.h"

#include "driver/gpio.h"
#include "esp_task_wdt.h"

#include "i2c_rw.h"
#include "bme280_service.h"
#include "sensirion_service.h"
#include "charger_service.h"

#include "pcf8574.h"

#include "aws_wifi.h"

#include "sntp_time.h"
#include "sensor_cache.h"
#include "aws_iot_shadow_interface.h"

#include "device_logic.h"

#include "led_strip_service.h"
#include "ota_update_service.h"

#include "version.h"
#include "etherNet.h"

#ifdef SADE_IOT_BT_ENABLED
#include "GattServer.h"
#endif

#define TEST_VALID 0xA5A54321
RTC_DATA_ATTR int16_t reset_count;
RTC_DATA_ATTR int32_t test_value;

void modemReset()
{
#ifdef CONFIG_SADE_IOT_MODEM_7000
    esp_err_t err;

    // set io-expander pins high / input
    pcf8574_config(0b11111111);
    // Power on the modem if it is turned off
    // if already on this short pulse does not do anything
    vTaskDelay(100 / portTICK_RATE_MS);
    pcf8574_pinWrite(PCF8574_GPIO_P6, 0);
    vTaskDelay(5000 / portTICK_RATE_MS);

#endif
#ifdef CONFIG_SADE_IOT_MODEM_868
    /* configure GPIO's for lock and SIM808 reset*/
    gpio_set_direction(GPIO_NUM_4, GPIO_MODE_INPUT);
    gpio_set_direction(GPIO_NUM_5, GPIO_MODE_OUTPUT);
    gpio_set_level(GPIO_NUM_5, 1);
#endif
}

void initModemTasks()
{
    // ==== Check connection ====
    get_time_from_ntp_server();
    //xTaskCreate(&https_get_task, "https_get_task", 16384, NULL, 4, NULL);

    //vTaskDelay(1000 / portTICK_RATE_MS);
#ifdef CONFIG_SADE_IOT_MODEM_ENABLED
    // xTaskCreatePinnedToCore(&gps_task, "gps_task", 4096, NULL, 4, NULL, 1);
    xTaskCreate(&gps_task, "gps_task", 6114, NULL, 9, NULL);
#endif
    /* Temporarily pin task to core, due to FPU uncertainty */
    // xTaskCreatePinnedToCore(&aws_iot_task, "aws_iot_task", 9216, NULL, 5, NULL, 1);
    //xTaskCreate(&aws_iot_task, "aws_iot_task", 16384, NULL, 9, NULL);
    aws_update_init();
}

#if 0
void initGlobalVariables() {

    esp_err_t err = nvs_flash_init();

    // err = esp_efuse_mac_get_default(mac);
    // using BT MAC as device mac so that we have same mac in application and in backend
    err = esp_read_mac(mac, ESP_MAC_BT);
    ESP_ERROR_CHECK(err);
    snprintf(deviceId, 18, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    size_t displayName_length = DISPLAY_NAME_SIZE;
    if (!get_data_str_from_nvs(DISPLAY_NAME_KEY, &displayName_length, displayName)) {
        snprintf(displayName, DISPLAY_NAME_SIZE, DISPLAY_NAME "%02X:%02X", mac[4], mac[5]);
        ESP_LOGD("Init: ", "displayName not set, generate new: %s", displayName);
        store_data_str_to_nvs(DISPLAY_NAME_KEY, displayName);
    }
    ESP_LOGI("DisplayName: ", "%s", displayName);
    ESP_LOGI("DeviceId: ", "%s", deviceId);

    size_t deviceSerialNumber_length = SERIAL_NUMBER_SIZE;
    if (get_manufacturing_data_str(SERIAL_NUMBER_KEY, &deviceSerialNumber_length, deviceSerialNumber)) {
        ESP_LOGI("Device Serial: ", "%s", deviceSerialNumber);
    }

    size_t apn_name_length = MAX_APN_NAME_SIZE;
    if (!get_data_str_from_nvs(APN_NAME_KEY, &apn_name_length, apnName)) {
        snprintf(apnName, &apn_name_length, CONFIG_GSM_APN);
    }

    size_t apn_user_length = MAX_APN_USER_SIZE;
    if (!get_data_str_from_nvs(APN_USER_KEY, &apn_user_length, apnUser)) {
        //snprintf(apnUser, &apn_user_length, APN_EMPTY);
    }

    size_t apn_pwd_length = MAX_APN_PWD_SIZE;
    if (!get_data_str_from_nvs(APN_PWD_KEY, &apn_pwd_length, apnPwd)) {
        //snprintf(apnPwd, &apn_pwd_length, APN_EMPTY);
    }
}
#endif

//=============
void app_main()
{
    if(test_value == TEST_VALID)
    {
        reset_count++;
    }
    else
    {
        reset_count = 1;
        test_value = TEST_VALID;
    }
    
    //while(1)
    //    vTaskDelay(5000 / portTICK_RATE_MS);
    esp_err_t err = nvs_flash_init();
    uint32_t WdTimeout = 120;
    esp_pm_config_esp32_t pm_config;

    pm_config.max_freq_mhz = 160;
    pm_config.min_freq_mhz = 40;
    pm_config.light_sleep_enable = false;

    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    // initGlobalVariables();
    ESP_LOGI("Info", "SADE IoT Platform ESP32, version %i.%i [%s]", FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_VERSION_TIMESTAMP);

    uint8_t mac[6];
    ESP_ERROR_CHECK(esp_read_mac(mac, ESP_MAC_BT));
    snprintf(device_parameters.deviceId, 18, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    /* init WatchDog -> update in AWS and GPS tasks. */
    /* to prevent total lockup if we have cellular/GPS connectivity issues*/    
    err = esp_task_wdt_init(WdTimeout, true);
    ESP_ERROR_CHECK(err);

    ESP_ERROR_CHECK(esp32_i2c_init());

    start_device_logic();
#ifdef CONFIG_SADE_IOT_ETHERNET_ENABLED
        ethernet_start();
        aws_update_init();    
#endif

#ifdef CONFIG_SADE_IOT_CAN_ENABLED
    startCanController();
#endif

#ifdef CONFIG_SADE_IOT_BT_ENABLED
    startBle();
#endif

#ifdef CONFIG_SADE_IOT_WLAN_ENABLED
    // Init wifi smartconfig gpio check
    gpio_set_direction(GPIO_NUM_34, GPIO_MODE_INPUT);
    gpio_set_pull_mode(GPIO_NUM_34, GPIO_PULLUP_ONLY);
    if (initialise_wifi() == 0)
    {
        initModemTasks();
    }
#endif

#ifdef CONFIG_SADE_IOT_MODEM_ENABLED
    static DRAM_ATTR esp_pm_lock_handle_t s_light_sleep_pm_lock;
    esp_pm_lock_create(ESP_PM_APB_FREQ_MAX, 0, "modemLock", &s_light_sleep_pm_lock);
    esp_pm_lock_acquire(s_light_sleep_pm_lock);
    modemReset();
    if (ppposInit())
    {
        s_wifi_event_group = xEventGroupCreate();
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
        initModemTasks();
    }
    else
    {
        ESP_LOGE("PPPoS EXAMPLE", "ERROR: GSM not initialized, HALTED");
        /* while (1) {
            vTaskDelay(1000 / portTICK_RATE_MS);
            // To-Do add 3x modem reset when we have GPIO connected to modem reset pin

            if (ppposInit()) {
                initModemTasks()
            }
        }*/
        
        #ifdef CONFIG_SADE_IOT_MODEM_7000
            // Turn off the modem if init is not successfull
            pcf8574_pinWrite(PCF8574_GPIO_P6, 1);
            vTaskDelay(2000 / portTICK_RATE_MS);
            pcf8574_pinWrite(PCF8574_GPIO_P6, 0);
            vTaskDelay(5000 / portTICK_RATE_MS);
        #endif
        esp_task_wdt_deinit();
        abort();
    }
#endif
  
    start_ota_service();    
    ESP_LOGE("HEAP check", "Free HEAP size after AWS: %i", xPortGetFreeHeapSize());

    // dcvs enable, BT bug prevents light sleep usage!
    // BT ESP consumption wo light sleep around 35mA with lightsleep 17mA
    err = esp_pm_configure(&pm_config);
}
