#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "esp32/pm.h"
#include "esp_bt.h"
#include "esp_err.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_pm.h"
#include "esp_system.h"
#include "esp_vfs_fat.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "AwsDataUpdate.h"
#include "CanController.h"

#include "Log.h"

#include "DataStorageHandler.h"

#include "driver/gpio.h"
#include "esp_task_wdt.h"

#include "i2c_rw.h"
#include "sensirion_service.h"
#include "pcf8574.h"

#include "aws_wifi.h"

#include "sntp_time.h"
#include "sensor_cache.h"
#include "aws_iot_shadow_interface.h"
#include <rom/rtc.h>

#include "mqtt_service.h"
#include "energy_service.h"
#include "fan.h"
#include "fan_service.h"
#include <driver/gpio.h>


#define TAG "device_logic"


void device_logic_task(void *parm)
{
 

    device_parameters.enter_deep_sleep = false;
    device_parameters.first_data_sent = false;
    device_parameters.single_measurement = false;
    device_parameters.sessionId = 0;
     
    esp_err_t err;
    #define POLE_FAN_STOPPED 38 // about between 32-44
    // relay outputs are 1 in boot --> releays drawn (closed)
    // dali used reset (0->1->0)
    printf("ioexp Dali relays opened -->0 (P5), dc/ac relay closed\n");
    pcf8574_pinWrite(PCF8574_GPIO_P7, 1);
    gpio_set_direction(GPIO_NUM_2, GPIO_MODE_INPUT);
    gpio_set_pull_mode(GPIO_NUM_2, GPIO_PULLUP_ONLY);

    set_fan_pwm(POLE_FAN_STOPPED);
    energy_service();
    fan_rpm_service();       
  
    
    TaskHandle_t sht31_task_handle = sht31_service(); 
    TaskHandle_t mqtt_task_handle = mqtt_service();


    jsonStruct_t deviceMac  =    {"deviceId",            &device_parameters.deviceId,      sizeof(device_parameters.deviceId),     SHADOW_JSON_STRING,     NULL};
    struct jsonStruct *pdeviceMac = &deviceMac;
    QueueHandle_t updatequeue = shadow_update_register();
    // QueueHandle_t deltaqueue = shadow_delta_register();
    xQueueSendToBack(updatequeue, (void *)&pdeviceMac, 10);

     while (1)
    {  
            if (device_parameters.sessionId == 0)
            {
                time_t timestamp = 0;
                // wait here until time is set correctly
                while (timestamp < 1557000000)
                {
                    time(&timestamp);
                    vTaskDelay(100 / portTICK_RATE_MS);
                }
                device_parameters.sessionId = timestamp;
            }
        // }

        vTaskDelay(1000 / portTICK_RATE_MS);
    }
    vTaskDelay(1000 / portTICK_RATE_MS);
}

void start_device_logic()
{
    xTaskCreate(&device_logic_task, "device_logic_task", 4096, NULL, 4, NULL);
}
