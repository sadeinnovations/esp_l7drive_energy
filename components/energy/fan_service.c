/* Pulse counter module - Example

   For other examples please check:
   https://github.com/espressif/esp-idf/tree/master/examples

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/portmacro.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/periph_ctrl.h"
#include "driver/ledc.h"
#include "driver/gpio.h"
#include "driver/pcnt.h"
#include "esp_attr.h"
#include "esp_log.h"
#include "soc/gpio_sig_map.h"
#include "time.h"
#include "sensirion_service.h"
#include "math.h"
#include "fan.h"
#include "energy_service.h"
#include "sensor_cache.h"
/*
 * An interrupt will be triggered when the counter value:
 *   - reaches 'thresh1' or 'thresh0' value,
 *   - reaches 'l_lim' value or 'h_lim' value,
 *   - will be reset to zero.
 */
static const char *TAG = "FAN_RPM_SERVICE";


#define DEW_POINT_DISTANCE_ALLOWED  2  //degrees
#define POLE_FAN_NORMAL_PWM         2       // normal operation PWM when air flow needed [0..30]
#define POLE_FAN_STOPPED_PWM        38      // Fan stopped (about 32-44)
#define ENV_CHECK_PERIOD            10000   // temp&hum sensor check period for dew point in msecs
#define FAN_SETTLE_PERIOD           10000   // temp&hum sensor check period for dew point in msecs
#define PCNT_TEST_UNIT              PCNT_UNIT_0
#define PCNT_H_LIM_VAL              1000
#define PCNT_L_LIM_VAL              -500
#define PCNT_THRESH1_VAL            4
#define PCNT_THRESH0_VAL            -4
#define PCNT_INPUT_SIG_IO           2       // Pulse Input GPIO
#define PCNT_INPUT_CTRL_IO          PCNT_PIN_NOT_USED  // Control GPIO HIGH=count up, LOW=count down
#define FAN_OVERIDE_MIN             30      // min pwm for overide to work 
#define FAN_OVERIDE_MAX             50      // max pwm for overide to work
#define FAN_RPM_PULSE_COUNT         100     // amount of pulses to count


xQueueHandle pcnt_evt_queue;            // A queue to handle pulse counter events
pcnt_isr_handle_t user_isr_handle = NULL; //user's ISR service handle

/* A sample structure to pass events from the PCNT
 * interrupt handler to the main program.
 */
typedef struct {
    int unit;  // the PCNT unit that originated an interrupt
    uint32_t status; // information on the event type that caused the interrupt
} pcnt_evt_t;

/* Decode what PCNT's unit originated an interrupt
 * and pass this information together with the event type
 * the main program using a queue.
 */
static void IRAM_ATTR pcnt_intr_handler(void *arg)
{
    uint32_t intr_status = PCNT.int_st.val;
    int i;
    pcnt_evt_t evt;
    portBASE_TYPE HPTaskAwoken = pdFALSE;

    for (i = 0; i < PCNT_UNIT_MAX; i++) {
        if (intr_status & (BIT(i))) {
            evt.unit = i;
            /* Save the PCNT event type that caused an interrupt
               to pass it to the main program */
            evt.status = PCNT.status_unit[i].val;
            PCNT.int_clr.val = BIT(i);
            xQueueSendFromISR(pcnt_evt_queue, &evt, &HPTaskAwoken);
            if (HPTaskAwoken == pdTRUE) {
                portYIELD_FROM_ISR();
            }
        }
    }
}


/* Initialize PCNT functions:
 *  - configure and initialize PCNT
 *  - set up the input filter
 *  - set up the counter events to watch
 */
static void pcnt_init(void)
{
    /* Prepare configuration for the PCNT unit */
    pcnt_config_t pcnt_config = {
        // Set PCNT input signal and control GPIOs
        .pulse_gpio_num = PCNT_INPUT_SIG_IO,
        .ctrl_gpio_num = PCNT_INPUT_CTRL_IO,
        .channel = PCNT_CHANNEL_0,
        .unit = PCNT_TEST_UNIT,
        // What to do on the positive / negative edge of pulse input?
        .pos_mode = PCNT_COUNT_INC,   // Count up on the positive edge
        .neg_mode = PCNT_COUNT_DIS,   // Keep the counter value on the negative edge
        // What to do when control input is low or high?
        .lctrl_mode = PCNT_MODE_KEEP, // Reverse counting direction if low
        .hctrl_mode = PCNT_MODE_KEEP,    // Keep the primary counter mode if high
        // Set the maximum and minimum limit values to watch
        .counter_h_lim = PCNT_H_LIM_VAL,
        .counter_l_lim = PCNT_L_LIM_VAL,
    };
    /* Initialize PCNT unit */
    pcnt_unit_config(&pcnt_config);

    /* Configure and enable the input filter */
    pcnt_set_filter_value(PCNT_TEST_UNIT, 5);
    pcnt_filter_enable(PCNT_TEST_UNIT);

    /* Set threshold 0 and 1 values and enable events to watch */
    pcnt_set_event_value(PCNT_TEST_UNIT, PCNT_EVT_THRES_1, PCNT_THRESH1_VAL);
    pcnt_event_enable(PCNT_TEST_UNIT, PCNT_EVT_THRES_1);
    pcnt_set_event_value(PCNT_TEST_UNIT, PCNT_EVT_THRES_0, PCNT_THRESH0_VAL);
    pcnt_event_enable(PCNT_TEST_UNIT, PCNT_EVT_THRES_0);
    /* Enable events on zero, maximum and minimum limit values */
    pcnt_event_enable(PCNT_TEST_UNIT, PCNT_EVT_ZERO);
    pcnt_event_enable(PCNT_TEST_UNIT, PCNT_EVT_H_LIM);
    pcnt_event_enable(PCNT_TEST_UNIT, PCNT_EVT_L_LIM);

    /* Initialize PCNT's counter */
    pcnt_counter_pause(PCNT_TEST_UNIT);
    pcnt_counter_clear(PCNT_TEST_UNIT);

    /* Register ISR handler and enable interrupts for PCNT unit */
    pcnt_isr_register(pcnt_intr_handler, NULL, 0, &user_isr_handle);
    pcnt_intr_enable(PCNT_TEST_UNIT);

    /* Everything is set up, now go to counting */
    pcnt_counter_resume(PCNT_TEST_UNIT);
}

float fan_rpm_read()
{
    vTaskDelay(FAN_SETTLE_PERIOD / portTICK_RATE_MS);
    pcnt_evt_queue = xQueueCreate(10, sizeof(pcnt_evt_t));
    pcnt_init();
    int16_t count = 0;
    pcnt_evt_t evt;
    portBASE_TYPE res;
    time_t timestampStart=0, timestampStop=0;
    int countA=0, countB=0;
    float rpm;

    
    while (1) {
        /* Wait for the event information passed from PCNT's interrupt handler.
         * Once received, decode the event type and print it on the serial monitor.
         */
        res = xQueueReceive(pcnt_evt_queue, &evt, 1000 / portTICK_PERIOD_MS);
        if (res == pdTRUE) {
            pcnt_get_counter_value(PCNT_TEST_UNIT, &count);
            if (evt.status & PCNT_STATUS_THRES1_M) {
                time(&timestampStart);
                countA=count;
            }
            if (evt.status & PCNT_STATUS_THRES0_M) {
                printf("THRES0 EVT\n");
            }   
            if (evt.status & PCNT_STATUS_L_LIM_M) {
                printf("L_LIM EVT\n");
            }
            if (evt.status & PCNT_STATUS_H_LIM_M) {
                printf("H_LIM EVT\n");
            }
            if (evt.status & PCNT_STATUS_ZERO_M) {
                printf("ZERO EVT\n");
            }
        } else {
            pcnt_get_counter_value(PCNT_TEST_UNIT, &count);
            if (count> FAN_RPM_PULSE_COUNT){
                countB=count;
                time(&timestampStop);
                rpm= 60*0.5*(countB-countA)/(timestampStop-timestampStart);
                ESP_LOGI(TAG, "start time %ld, end time %ld, start count %d, end count %d: rpm %f\n", timestampStart, timestampStop, countA, countB, rpm );
                return rpm;
                break;
            }
            else if (count==0) break;
        }
    }
    if(user_isr_handle) {
        //Free the ISR service handle.
        esp_intr_free(user_isr_handle);
        user_isr_handle = NULL;
    }    
    return 0.0;
}
void fan_rpm_task(void *parm)

{
    float temperature, humidity;
    float temperature2, humidity2;
    float dewPoint, dewPoint2, dpHelp;
    float rpm=0;
    bool sht31_1=false, sht31_2=false;
    bool T, H, DP;
    QueueHandle_t poleFanSpeed_xQueue;
    sensor_data_item sensordata;

    poleFanSpeed_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
    sensor_register(poleFanSpeed_xQueue, sizeof( sensor_data_item ), "poleFanSpeed"); 
    time(&sensordata.timestamp);
    sensordata.data = rpm;
    xQueueSendToBack( poleFanSpeed_xQueue, &sensordata,10);

    // read the temperature & humidity values

    while (1){
        for (int i=0;i<5;i++){
            temperature = sht31_get_temperature();
            humidity = sht31_get_humidity();
            if (temperature !=0){
                sht31_1 = true;
                break;
            }
            vTaskDelay(500 / portTICK_RATE_MS);
        }
        for (int i=0;i<5;i++){
            temperature2 = sht31_get_temperature2();
            humidity2 = sht31_get_humidity2();
            if (temperature2 !=0){
                sht31_2 = true;
                break;
            }
            vTaskDelay(500 / portTICK_RATE_MS);
        }

        // calculate the dew point
        if (sht31_1)
        {
            dpHelp = 17.27*temperature/(237.7+temperature)+log(humidity/100);
            dewPoint = 237.7*dpHelp/(17.27-dpHelp);
        }
        else dewPoint = -50; // no answer, not triggering
        if (sht31_2)
        {
            dpHelp = 17.27*temperature2/(237.7+temperature2)+log(humidity2/100);
            dewPoint2 = 237.7*dpHelp/(17.27-dpHelp);
        }
        else dewPoint2 = -50; // no answer, not triggering;

        ESP_LOGI(TAG,"dew point: %f, temperature: %f, humidity:  %f", dewPoint, temperature, humidity);
        ESP_LOGI(TAG,"dew point2: %f, temperature2: %f, humidity2: %f", dewPoint2, temperature2, humidity2);
        // printf("fanPwmOverride: %d\n", fanPwmOverride);
        
    // check whether shadow pwm-override mode
        if (fanPwmOverride > FAN_OVERIDE_MIN && fanPwmOverride < FAN_OVERIDE_MAX){
            T = (temperature > fanTempHystStart) || (temperature2 > fanTempHystStart);
            H = (humidity > fanHumMax) || (humidity2 > fanHumMax);
            DP =  (temperature < dewPoint+DEW_POINT_DISTANCE_ALLOWED) || (temperature2 < dewPoint2+DEW_POINT_DISTANCE_ALLOWED);
            
             
            if ( T || DP || H)
            {
                set_fan_pwm(POLE_FAN_NORMAL_PWM); 
                rpm = fan_rpm_read();
                sensordata.data = rpm; 
                time(&sensordata.timestamp);
                xQueueSendToBack( poleFanSpeed_xQueue, &sensordata,10);
            }
            else {
                set_fan_pwm(POLE_FAN_STOPPED_PWM);
                time(&sensordata.timestamp);
                sensordata.data = 0;
                xQueueSendToBack( poleFanSpeed_xQueue, &sensordata,10);
            }
        }
        else {
            set_fan_pwm(fanPwmOverride);
            rpm = fan_rpm_read();
            ESP_LOGI(TAG,"fan_service: rpm to send %f\n", rpm);
            time(&sensordata.timestamp);
            if(fanPwmOverride>50) sensordata.data = -rpm;
            else sensordata.data = rpm;
            xQueueSendToBack( poleFanSpeed_xQueue, &sensordata,10);
        }
        vTaskDelay(ENV_CHECK_PERIOD / portTICK_RATE_MS);
    }

    printf("killing rpm_task\n");
    vTaskDelete(NULL);
        
    
}

TaskHandle_t fan_rpm_service()
{
    ESP_LOGI(TAG, "starting fan_rpm task");
    TaskHandle_t task_handle;
    xTaskCreate(&fan_rpm_task, "fan_rpm_task", 4096, NULL, 8, &task_handle);
    return task_handle;
}
