#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "mqtt_service.h"
#include "aws_wifi.h"
#include "sensor_cache.h"

static const char *TAG = "MQTT_SERVICE";
const static int CONNECTED_BIT = BIT0;
esp_mqtt_client_handle_t mqttClient;
int qosBroker = 1;
char mac_addr[13] ={'0'};
char controlTopic[110]={'0'};




static esp_err_t mqtt_event_handler1(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;

    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT1_EVENT_CONNECTED");
            #ifdef CONFIG_L7_DRIVE_MODE_ENERGY
            // subscribing to the control topic of scsp broker
            msg_id = esp_mqtt_client_subscribe(client, controlTopic, 0);
            #endif
            // ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);

            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            ESP_LOGI(TAG,"1:  TOPIC = %.*s\r", event->topic_len, event->topic);
            ESP_LOGI(TAG,"1:  DATA = %.*s\r", event->data_len, event->data);
            
            #ifdef CONFIG_L7_DRIVE_MODE_ENERGY
                // resetting through control message
            if (strncmp(event->data, "kill",4) ==0) abort();
            #endif
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg1 = {
        .uri = CONFIG_BROKER_URL,
        .event_handle = mqtt_event_handler1,
        .lwt_qos = qosBroker,
    };

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg1);
    memcpy(&mqttClient,&client,sizeof(client));
    esp_mqtt_client_start(client);
}

void mqtt_task(void * param)
{
    for (int i=0, j=0;i<strlen(device_parameters.deviceId);i++){
        if (device_parameters.deviceId[i] != ':') {
            mac_addr[j] = device_parameters.deviceId[i];
            j++;
        }
    }    
    mac_addr[strlen(mac_addr)]='\0';

    #ifdef CONFIG_L7_DRIVE_MODE_ENERGY
    // creating control topic for scsp
    strcpy(controlTopic, "control/lt5g/+/l7drive/");
    strcat(controlTopic, mac_addr);
    strcat(controlTopic, "/+");
    printf("control topic :%s\n", controlTopic);
    #endif
     /* Wait for WiFI/LAN to show as connected */
    xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT,
                        false, true, portMAX_DELAY);
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);
    while (1){   
    time_t timestamp; 
    time(&timestamp); 
    if (timestamp > TIMESTAMP_EPOCH_CUTOFF) break;
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    ESP_LOGI(TAG, "Starting mqtt_app..");
    mqtt_app_start();
    while (1)
    {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
    ESP_LOGI(TAG, "kill task");
    vTaskDelete(NULL);
}

void send_to_broker_with_topic(int len, char * data, char * topic)
{
    esp_mqtt_client_publish(mqttClient, topic, data, len, 1, 0);
}


TaskHandle_t mqtt_service()
{
    ESP_LOGI(TAG, "starting mqtt task");
    TaskHandle_t task_handle;
    xTaskCreate(&mqtt_task, "mqtt_task", 8192, NULL, 9, &task_handle);
    return task_handle;
}