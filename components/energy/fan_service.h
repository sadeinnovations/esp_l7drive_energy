#ifndef _FAN_SERVICE_H_
#define _FAN_SERVICE_H_

float fan_rpm_read(void);
TaskHandle_t fan_rpm_service();

#endif
