#include "cJSON.h"
#include "aws_iot_shadow_interface.h"
#include "sensor_cache.h"
/*!
 * 
 */
#ifndef ENERGY_H_
#define ENERGY_H_

/*! CPP guard */
#ifdef __cplusplus
extern "C" {
#endif

#define FANHUMMIN 90
#define FANHUMMAX 95
#define FANPWMOVERRIDE 38
#define FANTEMPHYSTSTOP 45
#define FANTEMPHYSTSTART 50
#define POLEFANPWMZERO 38
#define POLEFANDIRECTION 0
#define RELAYOFF 0
#define DEFAULT_LATITUDE "60.3892275"
#define DEFAULT_LONGITUDE "23.1038751"
#define DEFAULT_LOCATIONUUID "9e070544-96bf-5672-bec0-3658dbf46ebb"
#define DALI_IO PCF8574_GPIO_P5
#define FAN_PWM_OVERRIDE_MIN 30
#define FAN_PWM_OVERRIDE_MAX 50


void send_to_broker(int len, char * data, int topic);
void update_topics(char * uuid);

TaskHandle_t energy_service();
// QueueHandle_t xQueue_luxLongitude;
void iot_dcAcRelay_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_daliRelay_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_fanTempHystStop_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_fanTempHystStart_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_fanHumMin_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_fanHumMax_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_poleFanPwm_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_latitude_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_longitude_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_locationUuid_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void * register_energy_item(QueueHandle_t qHandle, char * queueItem);

extern uint16_t fanPwmOverride;
extern int fanHumMin ;
extern int fanHumMax;
extern int fanTempHystStop;
extern int fanTempHystStart;
extern bool poleFanDirection;

#endif