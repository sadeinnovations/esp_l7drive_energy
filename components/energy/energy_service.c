#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "energy_service.h"
#include "aws_wifi.h"
#include "cJSON.h"
#include "pcf8574.h"
#include "sensor_cache.h"
#include "fan.h"
#include "time.h"
#include "aws_wifi.h"
#include "mqtt_service.h"

#define OTA_SHADOW_FIELD_MAX_SIZE   150
#define TOPICCOUNT 4
#define TOPICSIZE 110




static const char *TAG = "ENERGY_SERVICE";
char topics[TOPICCOUNT][TOPICSIZE] ={{0}};
char mac[13] ={'0'};
char * subtopics [TOPICCOUNT]= {"/aws", "/battery","/drive", "/pole"};

QueueHandle_t poleFanPwm_xQueue,fanTempHystStart_xQueue,fanTempHystStop_xQueue, fanHumMin_xQueue, fanHumMax_xQueue, longitude_xQueue, latitude_xQueue, 
    poleFanDirection_xQueue;
sensor_data_item dataitem;

const static int CONNECTED_BIT = BIT0;
int dcAcRelay;  // 0  off, 1 on
int daliRelay;  // 0  off, 1 on
uint16_t fanPwmOverride = FANPWMOVERRIDE;

jsonStruct_t daliRelayJson = {"daliRelay", &daliRelay, sizeof(int), SHADOW_JSON_INT32, iot_daliRelay_callback_handler};
struct jsonStruct *pdaliRelayJson = &daliRelayJson ;

int fanHumMin = FANHUMMIN;  // 
jsonStruct_t fanHumMinJson = {"fanHumMin", &fanHumMin, sizeof(int), SHADOW_JSON_INT32, iot_fanHumMin_callback_handler};
struct jsonStruct *pfanHumMinJson = &fanHumMinJson ;

int fanHumMax = FANHUMMAX;  // 
jsonStruct_t fanHumMaxJson = {"fanHumMax", &fanHumMax, sizeof(int), SHADOW_JSON_INT32, iot_fanHumMax_callback_handler};
struct jsonStruct *pfanHumMaxJson = &fanHumMaxJson ;

int poleFanPwm = POLEFANPWMZERO;  // 0-100, fan stopped 31-44. Below 31 blows towards bottom label
jsonStruct_t poleFanPwmJson = {"poleFanPwm", &poleFanPwm, sizeof(int), SHADOW_JSON_INT32, iot_poleFanPwm_callback_handler};
struct jsonStruct *ppoleFanPwmJson = &poleFanPwmJson ;

int fanTempHystStop = FANTEMPHYSTSTOP;  // 
jsonStruct_t fanTempHystStopJson = {"fanTempHystStop", &fanTempHystStop, sizeof(int), SHADOW_JSON_INT32, iot_fanTempHystStop_callback_handler};
struct jsonStruct *pfanTempHystStopJson = &fanTempHystStopJson ;

int fanTempHystStart = FANTEMPHYSTSTART;  // 
jsonStruct_t fanTempHystStartJson = {"fanTempHystStart", &fanTempHystStart, sizeof(int), SHADOW_JSON_INT32, iot_fanTempHystStart_callback_handler};
struct jsonStruct *pfanTempHystStartJson = &fanTempHystStartJson ;

static char latitude[] = DEFAULT_LATITUDE;  // 
static jsonStruct_t latitudeJson = {"latitude", latitude, sizeof(latitude), SHADOW_JSON_STRING, iot_latitude_callback_handler};
static struct jsonStruct *platitudeJson = &latitudeJson;

static char longitude[] = DEFAULT_LONGITUDE;  //  
static jsonStruct_t longitudeJson = {"longitude", longitude, sizeof(longitude), SHADOW_JSON_STRING, iot_longitude_callback_handler};
static struct jsonStruct *plongitudeJson = &longitudeJson;

char locationUuid[] = DEFAULT_LOCATIONUUID    ;  //  defaulting to pole101, changes from shadow
jsonStruct_t locationUuidJson = {"locationUuid", &locationUuid, sizeof(locationUuid) * sizeof(char) + 1, SHADOW_JSON_STRING, iot_locationUuid_callback_handler};
struct jsonStruct *plocationUuidJson = &locationUuidJson;

bool poleFanDirection = POLEFANDIRECTION; // 0: normal mode, 1: blowing upwards


void iot_daliRelay_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("daliRelay: %d\n",  daliRelay);

   if (daliRelay == 0) {
       printf("ioexp dali relay open\n");
       pcf8574_pinWrite(DALI_IO, 0);
   }
   else if (daliRelay == 1){
       pcf8574_pinWrite(DALI_IO, 1);
       printf("ioexp dali relay closed\n");
   }
   QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pdaliRelayJson, 10);
    }
}

void iot_fanTempHystStop_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("fanTempHystStop: %d\n",  fanTempHystStop);

   QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pfanTempHystStopJson, 10);
        time(&dataitem.timestamp); 
        dataitem.data = fanTempHystStop;
        xQueueSendToBack( fanTempHystStop_xQueue, &dataitem, 10 );
    }
}

void iot_fanTempHystStart_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("fanTempHystStart: %d\n",  fanTempHystStart);
   
    QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pfanTempHystStartJson, 10);
        time(&dataitem.timestamp);               
        dataitem.data = fanTempHystStart;
        xQueueSendToBack( fanTempHystStart_xQueue   , &dataitem, 10 );
    }
}

void iot_fanHumMin_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("fanHumMin: %d\n",  fanHumMin);
   
    QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pfanHumMinJson, 10);
        time(&dataitem.timestamp);               
        dataitem.data = fanHumMin;
        xQueueSendToBack(fanHumMin_xQueue, &dataitem, 10);
    }
}

void iot_fanHumMax_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("fanHumMax: %d\n",  fanHumMax);
   
    QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pfanHumMaxJson, 10);
        time(&dataitem.timestamp);               
        dataitem.data = fanHumMax;
        xQueueSendToBack(fanHumMax_xQueue, &dataitem, 10);
    }
}

void iot_poleFanPwm_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("poleFanPwm: %d\n",  poleFanPwm);

    if (poleFanPwm > FAN_PWM_OVERRIDE_MIN && poleFanPwm < FAN_PWM_OVERRIDE_MAX){
        set_fan_pwm(poleFanPwm);    
    }
    fanPwmOverride = poleFanPwm;
    vTaskDelay(100 / portTICK_PERIOD_MS);
    QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&ppoleFanPwmJson, 10);
        time(&dataitem.timestamp);               
        dataitem.data = poleFanPwm;
        xQueueSendToBack(poleFanPwm_xQueue, &dataitem, 10);
        if (poleFanPwm>50) poleFanDirection=1;
        else poleFanDirection=0;
        dataitem.data = poleFanDirection;
        xQueueSendToBack(poleFanDirection_xQueue, &dataitem, 10);
    }
}

void iot_latitude_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("latitude: %s\n",  latitude);
   
   QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        printf("************** latitude json %s\n", (char*)platitudeJson->pData);
        time(&dataitem.timestamp);               
        dataitem.data = atof(latitude);
        xQueueSendToBack(latitude_xQueue, &dataitem, 10);
    }
}
void iot_longitude_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("longitude: %s\n",  longitude);
   
    QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        printf("************** longitude json %s\n", (char*)plongitudeJson->pData);
        printf("***************longitude pkey %s\n", (char *)plongitudeJson->pKey);
        printf("************** longitude pcont json %s\n", (char*)pContext->pData);
        printf("***************longitude pcont pkey %s\n", (char *)pContext->pKey);
        time(&dataitem.timestamp);               
        dataitem.data = atof(longitude);
        xQueueSendToBack(longitude_xQueue, &dataitem, 10);
    }
}

void iot_locationUuid_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("locationUuid: %s\n",  locationUuid);
   
    QueueHandle_t updatequeue = shadow_update_register();

    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&plocationUuidJson, 10);
     
    }
}

void * register_energy_item(QueueHandle_t qHandle, char * queueItem)
{
    qHandle = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register( qHandle, sizeof(sensor_data_item), queueItem);

    return qHandle;
}

// void mqtt_koe_start()
void energy_task(void * param)
{
    
    // shadow items
    QueueHandle_t deltaqueue = shadow_delta_register();
    xQueueSendToBack(deltaqueue, (void *)&pdaliRelayJson, 10);
    xQueueSendToBack(deltaqueue, (void *)&pfanTempHystStopJson, 10);
    xQueueSendToBack(deltaqueue, (void *)&pfanTempHystStartJson, 10);   
    xQueueSendToBack(deltaqueue, (void *)&pfanHumMinJson, 10);
    xQueueSendToBack(deltaqueue, (void *)&pfanHumMaxJson, 10);
    xQueueSendToBack(deltaqueue, (void *)&ppoleFanPwmJson, 10);
    xQueueSendToBack(deltaqueue, (void *)&plongitudeJson, 10);
    xQueueSendToBack(deltaqueue, (void *)&platitudeJson, 10);    
    xQueueSendToBack(deltaqueue, (void *)&plocationUuidJson, 10);
    printf("jlocationUuid %s\n", (char *)plocationUuidJson->pData);
    printf("jlongitudejson %s\n", (char *)plongitudeJson->pData);
    printf("longitude pkey %s\n", (char *)plongitudeJson->pKey);

     /* Wait for WiFI/LAN to show as connected */
    xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT,
                        false, true, portMAX_DELAY);


    // Sensor items for cloud reporting
    while (1){    
        time(&dataitem.timestamp); 
        if (dataitem.timestamp > TIMESTAMP_EPOCH_CUTOFF) break;
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
                  
    dataitem.data = poleFanPwm;    
    poleFanPwm_xQueue = xQueueCreate(1, sizeof(sensor_data_item));    
    sensor_register( poleFanPwm_xQueue, sizeof(sensor_data_item), "poleFanPwm");
    xQueueSendToBack( poleFanPwm_xQueue, &dataitem, 10 );

    dataitem.data = poleFanDirection;    
    poleFanDirection_xQueue = xQueueCreate(1, sizeof(sensor_data_item));    
    sensor_register( poleFanDirection_xQueue, sizeof(sensor_data_item), "poleFanDirection");
    xQueueSendToBack( poleFanDirection_xQueue, &dataitem, 10 );

    dataitem.data = fanTempHystStart;    
    fanTempHystStart_xQueue = xQueueCreate(1, sizeof(sensor_data_item));    
    sensor_register( fanTempHystStart_xQueue, sizeof(sensor_data_item), "fanTempHystStart");
    xQueueSendToBack( fanTempHystStart_xQueue, &dataitem, 10 );

    dataitem.data = fanTempHystStop;    
    fanTempHystStop_xQueue = xQueueCreate(1, sizeof(sensor_data_item));    
    sensor_register( fanTempHystStop_xQueue, sizeof(sensor_data_item), "fanTempHystStop");
    xQueueSendToBack( fanTempHystStop_xQueue, &dataitem, 10 );

    fanHumMax =85;
    dataitem.data = fanHumMax;    
    fanHumMax_xQueue = xQueueCreate(1, sizeof(sensor_data_item));    
    sensor_register( fanHumMax_xQueue, sizeof(sensor_data_item), "fanHumMax");
    xQueueSendToBack( fanHumMax_xQueue, &dataitem, 10 );

    fanHumMin =70;
    dataitem.data = fanHumMin;    
    fanHumMin_xQueue = xQueueCreate(1, sizeof(sensor_data_item));    
    sensor_register( fanHumMin_xQueue, sizeof(sensor_data_item), "fanHumMin");
    xQueueSendToBack( fanHumMin_xQueue, &dataitem, 10 );

    longitude_xQueue = xQueueCreate(1, sizeof(sensor_data_item));    
    sensor_register( longitude_xQueue, sizeof(sensor_data_item), "longitude");

    latitude_xQueue = xQueueCreate(1, sizeof(sensor_data_item));    
    sensor_register( latitude_xQueue, sizeof(sensor_data_item), "latitude");


    while (1)
    {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
    ESP_LOGI(TAG, "kill task");
    vTaskDelete(NULL);
}

void send_to_broker(int len, char * data, int topic)
{
    if (strlen(locationUuid)<2) return;     
    else if (strlen(topics[topic]) == 0){
        update_topics(locationUuid);
    }
    send_to_broker_with_topic(len, data, topics[topic]);
}

void update_topics(char * uuid)
{
    for (int i=0, j=0;i<strlen(device_parameters.deviceId);i++){
        if (device_parameters.deviceId[i] != ':') {
            mac[j] = device_parameters.deviceId[i];
            j++;
    }
    }    
    mac[strlen(mac)]='\0';
    ESP_LOGI(TAG,"Creating topics:");

    for (int i=1; i < TOPICCOUNT ; i++) {
        strcpy(topics[i],"data/lt5g/");
        strcat(topics[i],uuid);
        strcat(topics[i],"/l7drive/");        
        strcat(topics[i],mac);
        strcat(topics[i],subtopics[i]);
        strcat(topics[i],"/json");

        printf("topics %d: %s\n", i,topics[i]);
    }  
}

TaskHandle_t energy_service()
{
    ESP_LOGI(TAG, "starting energy task");
    TaskHandle_t task_handle;
    xTaskCreate(&energy_task, "energy_task", 8192, NULL, 9, &task_handle);
    return task_handle;
}