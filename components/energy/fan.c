/* LEDC (LED Controller) fade example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include <stdio.h>
#include "freertos/event_groups.h"

#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"

#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_vfs_fat.h"
#include <string.h>

#include "esp_event.h"
#include "esp_event_loop.h"
#include "fan_service.h"
#include "energy_service.h"

#define LEDC_HS_TIMER          LEDC_TIMER_0
#define LEDC_HS_MODE           LEDC_HIGH_SPEED_MODE
#define LEDC_HS_CH0_GPIO       (13)
#define LEDC_HS_CH0_CHANNEL    LEDC_CHANNEL_0
#define LEDC_DUTY               (2048) // 11bit mode, 0-2048
#define FAN_PWM_OVERRIDE_MIN    30
#define FAN_PWM_OVERRIDE_MAX    50
#define POLEFANPWM              38

static const char *TAG = "FAN_C";

uint16_t fanPwmOverride;


esp_err_t event_handler(void *ctx, system_event_t *event)
{
	return ESP_OK;
}


void set_fan_pwm(int fan_pwm)
{
     // check that value is within limits, and that there is a change
    static int current_pwm;
    if (fan_pwm == current_pwm) return;

    if (fan_pwm>100) fan_pwm=100;
    else if (fan_pwm<0) fan_pwm=0;

    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_11_BIT, // resolution of PWM duty
        .freq_hz = 25000,                      // frequency of PWM signal
        .speed_mode = LEDC_HS_MODE,           // timer mode
        .timer_num = LEDC_HS_TIMER            // timer index
    };
    // Set configuration of timer0 for high speed channels
    ledc_timer_config(&ledc_timer);

    ledc_channel_config_t ledc_channel[1] = {
        {
            .channel    = LEDC_HS_CH0_CHANNEL,
            .duty       = 0,
            .gpio_num   = LEDC_HS_CH0_GPIO,
            .speed_mode = LEDC_HS_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_HS_TIMER
        },
    };

    // Set LED Controller with previously prepared configuration
    ledc_channel_config(&ledc_channel[0]);

    // Initialize fade service.
    ledc_fade_func_install(0);    

    ESP_LOGI(TAG,"setting fan pwm %d******************************", fan_pwm);
    ledc_set_duty(ledc_channel[0].speed_mode, ledc_channel[0].channel, (fan_pwm*LEDC_DUTY/100));
    current_pwm = fan_pwm;
    ledc_update_duty(ledc_channel[0].speed_mode, ledc_channel[0].channel);
    vTaskDelay(30 / portTICK_PERIOD_MS);
    
}