#ifndef MQTT_H_
#define MQTT_H_

/*! CPP guard */
#ifdef __cplusplus
extern "C" {
#endif
// void mqtt_koe_start();

TaskHandle_t mqtt_service();
void send_to_broker_with_topic(int len, char * data, char * topic);

#endif