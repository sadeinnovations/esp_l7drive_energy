#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_system.h"
#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "i2c_rw.h"
#include "sensor_cache.h"
#include "lp5569.h"
#include "sensirion_service.h"
#include "sensor_cache.h"
#include <rom/rtc.h>
#include "math.h"

#define redLED 0
#define greenLED 1
#define blueLED 2
#define redLED2 5
#define greenLED2 6
#define blueLED2 7

#define STARTUP_LEDS

#define led_step 1

int W_PWMatMin=0;  //int
int R_PWMatMin=0;  //int
int G_PWMatMin=0;  //int
int B_PWMatMin=0;  //int
int W_PWMatMax=0;  //int
int R_PWMatMax=0;  //int
int G_PWMatMax=0;  //int
int B_PWMatMax=0;  //int
int humidityMin = 0;
int humidityMax = 0;
int brightMin = 0;
int brightMax = 0;
bool ledEnabled = false;
uint32_t infoledColor = 0;
uint32_t infoledCounter = 0;

// dustInterval is how often dust is measured when switching from deep sleep
char RGBPWMatMax[9] = "#00000000";
char RGBPWMatMin[9] = "#00000000";

void RGBPWMatMaxHandler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void RGBPWMatMinHandler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);

jsonStruct_t RGBPWMatMaxJson = {"RGBPWMatMax", &RGBPWMatMax, sizeof(RGBPWMatMax) * sizeof(char) + 1, SHADOW_JSON_STRING, RGBPWMatMaxHandler};
jsonStruct_t RGBPWMatMinJson = {"RGBPWMatMin", &RGBPWMatMin, sizeof(RGBPWMatMin) * sizeof(char) + 1, SHADOW_JSON_STRING, RGBPWMatMinHandler};
jsonStruct_t RGBBrightMaxJson = {"RGBBrightMax", &brightMax, sizeof(int), SHADOW_JSON_INT32, NULL};
jsonStruct_t RGBBrightMinJson = {"RGBBrightMin", &brightMin, sizeof(int), SHADOW_JSON_INT32, NULL};
jsonStruct_t RGBHumidityMaxJson = {"RGBHumidityMax", &humidityMax, sizeof(int), SHADOW_JSON_INT32, NULL};
jsonStruct_t RGBHumidityMinJson = {"RGBHumidityMin", &humidityMin, sizeof(int), SHADOW_JSON_INT32, NULL};
jsonStruct_t ledEnableJson = {"ledEnabled", &ledEnabled, sizeof(bool), SHADOW_JSON_BOOL, NULL};

void RGBPWMatMaxHandler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    char colorhex[6];
    uint32_t colornum;
    memcpy(colorhex, &RGBPWMatMax[1], sizeof(colorhex));
    colornum = strtol(colorhex, NULL, 16);
    W_PWMatMax = (colornum >> 24) & 0xFF;
    R_PWMatMax = (colornum >> 16) & 0xFF;
    G_PWMatMax = (colornum >> 8) & 0xFF;
    B_PWMatMax = colornum & 0xFF;
    QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pContext, 10);
    }
}

void RGBPWMatMinHandler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{

    char colorhex[6];
    uint32_t colornum;
    memcpy(colorhex, &RGBPWMatMin[1], sizeof(colorhex));
    colornum = strtol(colorhex, NULL, 16);
    W_PWMatMin = (colornum >> 24) & 0xFF;
    R_PWMatMin = (colornum >> 16) & 0xFF;
    G_PWMatMin = (colornum >> 8) & 0xFF;
    B_PWMatMin = colornum & 0xFF;
    QueueHandle_t updatequeue = shadow_update_register();
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pContext, 10);
    }

}

void lp5569_setup()
{
    int i;
    for(i=0; i<NumChannels; i++)
    {
        lp5569_SetExtSource(i, true); 
        lp5569_AssignChannelToMasterFader(i, 0);  // Assign all channels to MasterFader 0
        lp5569_SetLogBrightness(i, true);  // Set logarithmc brightness to all channela
        lp5569_SetDriveCurrent(i, 0x10);  //FETin ohjaus: 0x01 liian pieni, 0x10 nayttaisi toimivan
    }
}

void lp5569_task(void * parm)
{

    int R_PWM = 0;
    int G_PWM = 0;
    int B_PWM = 0;
    int R_PWM_new = 0;
    int G_PWM_new = 0;
    int B_PWM_new = 0;
    int Bright_new = 0;
    int Brightness = 0;
    float humidity_ext, humidity_prev;

    

    lp5569_Reset();
    if(device_parameters.single_measurement)
        goto end;
    lp5569_init();
    lp5569_setup();
    

    // Get shadow delta queue handler and add settings json 
    struct jsonStruct *pRGBPWMatMaxJson = &RGBPWMatMaxJson;
    QueueHandle_t deltaqueue = shadow_delta_register();
    xQueueSendToBack(deltaqueue,  ( void * ) &pRGBPWMatMaxJson, 10);

    struct jsonStruct *pledEnableJson = &ledEnableJson;
    xQueueSendToBack(deltaqueue,  ( void * ) &pledEnableJson, 10);

    struct jsonStruct *pRGBPWMatMinJson = &RGBPWMatMinJson;
    xQueueSendToBack(deltaqueue,  ( void * ) &pRGBPWMatMinJson, 10);

    struct jsonStruct *pRGBHumidityMaxJson = &RGBHumidityMaxJson;
    xQueueSendToBack(deltaqueue,  ( void * ) &pRGBHumidityMaxJson, 10);

    struct jsonStruct *pRGBHumidityMinJson = &RGBHumidityMinJson;
    xQueueSendToBack(deltaqueue,  ( void * ) &pRGBHumidityMinJson, 10);

    struct jsonStruct *pRGBBrightMinJson = &RGBBrightMinJson;
    xQueueSendToBack(deltaqueue,  ( void * ) &pRGBBrightMinJson, 10);

    struct jsonStruct *pRGBBrightMaxJson = &RGBBrightMaxJson;
    xQueueSendToBack(deltaqueue,  ( void * ) &pRGBBrightMaxJson, 10);
 
#ifdef STARTUP_LEDS    
    // green startup led
    lp5569_SetMasterFader(0, 0); 
    R_PWM = 200;
    G_PWM = 200;
    B_PWM = 200;
    lp5569_SetChannelPWM(greenLED, G_PWM);
    lp5569_SetChannelPWM(redLED, R_PWM);
    lp5569_SetChannelPWM(blueLED, B_PWM);
    vTaskDelay(500 / portTICK_RATE_MS);
    int counter = 0;
    // wait for last shadow item received
    while(brightMax == 0)
    {
        Brightness = 70-70*cos(6.28*counter/70);
        lp5569_SetMasterFader(0, Brightness);
        counter++;
        vTaskDelay(50 / portTICK_RATE_MS);
        if(device_parameters.enter_deep_sleep)
            goto end;
    }
#else    
    // wait for last shadow item received
    while(brightMax == 0)
    {
        vTaskDelay(500 / portTICK_RATE_MS);
        if(device_parameters.enter_deep_sleep)
            goto end;
    }    
#endif

    humidity_prev = sht31_get_humidity();
    while(!device_parameters.enter_deep_sleep)
    {
        if(ledEnabled)
        {
            humidity_ext = sht31_get_humidity();
            if (humidity_ext < humidityMin) {  // If measured humidity is under set minimum, set humidity to minimum
                humidity_ext = humidityMin;
            }
            if (humidity_ext > humidityMax) {  // If measured humidity is over set maximum, set humidity to maximum
                humidity_ext = humidityMax;
            }

            R_PWM_new = (int)(R_PWMatMin + (humidity_ext - humidityMin) * (R_PWMatMax - R_PWMatMin) / (humidityMax - humidityMin));   // Set to humidityMin and humidityMax
            G_PWM_new = (int)(G_PWMatMin + (humidity_ext - humidityMin) * (G_PWMatMax - G_PWMatMin) / (humidityMax - humidityMin));   // Set to humidityMin and humidityMax
            B_PWM_new = (int)(B_PWMatMin + (humidity_ext - humidityMin) * (B_PWMatMax - B_PWMatMin) / (humidityMax - humidityMin));   // Set to humidityMin and humidityMax
            Bright_new = (int)(brightMin + (humidity_ext - humidityMin) * (brightMax - brightMin) / (humidityMax - humidityMin));  // Set to humidityMin and humidityMax
            /*
            printf(",H= %f\n", humidity_ext);
            printf(" ,HmaxSet= %f\n", humidityMax);
            printf(" ,HminSet= %f\n", humidityMin);
            printf(" ,R_PWM= %d\n", R_PWM_new);
            printf(" ,G_PWM= %d\n", G_PWM_new);
            printf(" ,B_PWM= %d\n", B_PWM_new);
            printf(" ,R_minSet= %f\n", R_PWMatMin);
            printf(" ,G_minSet= %f\n", G_PWMatMin);
            printf(" ,B_minSet= %f\n", B_PWMatMin);
            printf(" ,R_maxSet= %f\n", R_PWMatMax);
            printf(" ,G_maxSet= %f\n", G_PWMatMax);
            printf(" ,B_maxSet= %f\n", B_PWMatMax);
            printf(" ,brightSet= %d\n", Brightness);
            */

        }
        else
        {
            R_PWM_new = 0;
            G_PWM_new = 0;
            B_PWM_new = 0;
            Bright_new = 0;
            // When light is disabled sleep a bit more
            if(Brightness == 0)
                vTaskDelay(1000 / portTICK_RATE_MS);

        }
        if(infoledCounter>0)
        {
            R_PWM_new = (infoledColor >> 16) & 0xFF;
            G_PWM_new = (infoledColor >> 8) & 0xFF;
            B_PWM_new = infoledColor & 0xFF;
            Brightness = 100;
            infoledCounter--;
        }

            if (R_PWM_new != R_PWM) {
                if(R_PWM_new-R_PWM > led_step)
                    R_PWM = R_PWM + led_step;
                else
                    if(R_PWM-R_PWM_new>led_step)
                        R_PWM = R_PWM - led_step;
                else
                    R_PWM = R_PWM_new;
                lp5569_SetChannelPWM(redLED, R_PWM);
                lp5569_SetChannelPWM(redLED2, R_PWM);
                //printf(" ,R_PWM= %d\n", R_PWM);
                //delay(2);
                //lp5569_SetChannelPWM(redLED, R_PWM);      //debug
            }
            vTaskDelay(5 / portTICK_RATE_MS);
    
            if (G_PWM_new != G_PWM) {
                if(G_PWM_new-G_PWM > led_step)
                    G_PWM = G_PWM + led_step;
                else
                    if(G_PWM-G_PWM_new > led_step)
                        G_PWM = G_PWM - led_step;
                else
                    G_PWM = G_PWM_new;
                lp5569_SetChannelPWM(greenLED, G_PWM);
                lp5569_SetChannelPWM(greenLED2, G_PWM);   
                //printf(" ,G_PWM= %d\n", G_PWM_new); 
                //delay(2);
                //lp5569_SetChannelPWM(greenLED, G_PWM);     //debug
            }
            vTaskDelay(5 / portTICK_RATE_MS);
            
            if (B_PWM_new != B_PWM) {
                if(B_PWM_new-B_PWM > led_step)
                    B_PWM = B_PWM + led_step;
                else
                    if(B_PWM-B_PWM_new > led_step)
                        B_PWM = B_PWM - led_step;
                else
                    B_PWM = B_PWM_new; 
                lp5569_SetChannelPWM(blueLED, B_PWM);
                lp5569_SetChannelPWM(blueLED2, B_PWM);
                //printf(" ,B_PWM= %d\n", B_PWM_new);
                //delay(2);
                //lp5569_SetChannelPWM(blueLED, B_PWM);    //debug
            }
            vTaskDelay(5 / portTICK_RATE_MS);  // tsekkaa nopeus  

            if (Bright_new != Brightness) {
                if(Bright_new-Brightness > led_step)
                    Brightness = Brightness + led_step;
                else
                    if(Brightness-Bright_new > led_step)
                        Brightness = Brightness - led_step;
                else
                    Brightness = Bright_new;
                lp5569_SetMasterFader(0, Brightness); 
                //printf(" ,Brightness= %d\n", Brightness);
                //delay(2);
                //ledChip.SetMasterFader(0, Brightness);     //debug
            }
            vTaskDelay(55 / portTICK_RATE_MS);  // Delay added to adjust the loop time to 100ms
    }
end:
    lp5569_Disable();
    vTaskDelete(NULL);
}


TaskHandle_t lp5569_service()
{  
    TaskHandle_t task_handle;
    xTaskCreate(&lp5569_task, "lp5569_task", 2048, NULL, 9, &task_handle);
    return task_handle;
}

void infoled(uint32_t color, uint32_t duration)
{
    infoledColor = color;
    infoledCounter = duration;
}
