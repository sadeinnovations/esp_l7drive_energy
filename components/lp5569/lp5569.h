/******************************************************************************
lp55231.h
Header file for LP55231 breakout board Arduino Library.

Byron Jacquot @ SparkFun Electronics
October 21, 2016
https://github.com/sparkfun/SparkFun_LP55231_Arduino_Library

Arduino library supporting LP55231 breakout board.

Can be instanciated two different ways:
As an Lp55231 object, offering simple control over the IC, with minimum ROM footprint.
As an Lp55231Engines object, adding diagnostic and execution engine support.

Resources:
Written using SparkFun Pro Micro controller, with an LP55231 breakout board.
Development environment specifics:
Written using Arduino 1.6.5

This code is released under the [MIT License](http://opensource.org/licenses/MIT).

Please review the LICENSE.md file included with this example. If you have any questions
or concerns with licensing, please contact techsupport@sparkfun.com.

Distributed as-is; no warranty is given.
******************************************************************************/

#ifndef _LP5569_H_
#define _LP5569_H_

#include <stdint.h>

#define LP5569_I2C_ADDRESS 0x42

// From Section 8.6: Register Maps
#define LP5569_REG_CONFIG                   0x00    // Configuration Register
#define LP5569_REG_LED_ENGINE_CONTROL1      0x01    // Engine Execution Control Register
#define LP5569_REG_LED_ENGINE_CONTROL2      0x02    // Engine Operation Mode Register
#define LP5569_REG_LED0_CONTROL             0x07    // LED0 Control Register
#define LP5569_REG_LED1_CONTROL             0x08    // LED1 Control Register
#define LP5569_REG_LED2_CONTROL             0x09    // LED2 Control Register
#define LP5569_REG_LED3_CONTROL             0x0A    // LED3 Control Register
#define LP5569_REG_LED4_CONTROL             0x0B    // LED4 Control Register
#define LP5569_REG_LED5_CONTROL             0x0C    // LED5 Control Register
#define LP5569_REG_LED6_CONTROL             0x0D    // LED6 Control Register
#define LP5569_REG_LED7_CONTROL             0x0E    // LED7 Control Register
#define LP5569_REG_LED8_CONTROL             0x0F    // LED8 Control Register
#define LP5569_REG_LED0_PWM                 0x16    // LED0 PWM Duty Cycle
#define LP5569_REG_LED1_PWM                 0x17    // LED1 PWM Duty Cycle
#define LP5569_REG_LED2_PWM                 0x18    // LED2 PWM Duty Cycle
#define LP5569_REG_LED3_PWM                 0x19    // LED3 PWM Duty Cycle
#define LP5569_REG_LED4_PWM                 0x1A    // LED4 PWM Duty Cycle
#define LP5569_REG_LED5_PWM                 0x1B    // LED5 PWM Duty Cycle
#define LP5569_REG_LED6_PWM                 0x1C    // LED6 PWM Duty Cycle
#define LP5569_REG_LED7_PWM                 0x1D    // LED7 PWM Duty Cycle
#define LP5569_REG_LED8_PWM                 0x1E    // LED8 PWM Duty Cycle
#define LP5569_REG_LED0_CURRENT             0x22    // LED0 Current Control
#define LP5569_REG_LED1_CURRENT             0x23    // LED1 Current Control
#define LP5569_REG_LED2_CURRENT             0x24    // LED2 Current Control
#define LP5569_REG_LED3_CURRENT             0x25    // LED3 Current Control
#define LP5569_REG_LED4_CURRENT             0x26    // LED4 Current Control
#define LP5569_REG_LED5_CURRENT             0x27    // LED5 Current Control
#define LP5569_REG_LED6_CURRENT             0x28    // LED6 Current Control
#define LP5569_REG_LED7_CURRENT             0x29    // LED7 Current Control
#define LP5569_REG_LED8_CURRENT             0x2A    // LED8 Current Control
#define LP5569_REG_MISC                     0x2F    // I2C Charge Pump and Clock Configuration
#define LP5569_REG_ENGINE1_PC               0x30    // Engine 1 Program Counter
#define LP5569_REG_ENGINE2_PC               0x31    // Engine 2 Program Counter
#define LP5569_REG_ENGINE3_PC               0x32    // Engine 3 Program Counter
#define LP5569_REG_MISC2                    0x33    // Charge Pump and LED Configuration
#define LP5569_REG_ENGINE_STATUS            0x3C    // Engine 1/2/3 Status
#define LP5569_REG_IO_CONTROL               0x3D    // TRIG/INT/CLK Configuration
#define LP5569_REG_VARIABLE_D               0x3E    // Global Variable D
#define LP5569_REG_RESET                    0x3F    // Software Reset
#define LP5569_REG_ENGINE1_VARIABLE_A       0x42    // Engine 1 Local Variable A
#define LP5569_REG_ENGINE2_VARIABLE_A       0x43    // Engine 2 Local Variable A
#define LP5569_REG_ENGINE3_VARIABLE_A       0x44    // Engine 3 Local Variable A
#define LP5569_REG_MASTER_FADER1            0x46    // Engine 1 Master Fader
#define LP5569_REG_MASTER_FADER2            0x47    // Engine 2 Master Fader
#define LP5569_REG_MASTER_FADER3            0x48    // Engine 3 Master Fader
#define LP5569_REG_MASTER_FADER_PWM         0x4A    // PWM Input Duty Cycle
#define LP5569_REG_ENGINE1_PROG_START       0x4B    // Engine 1 Program Starting Address
#define LP5569_REG_ENGINE2_PROG_START       0x4C    // Engine 2 Program Starting Address
#define LP5569_REG_ENGINE3_PROG_START       0x4D    // Engine 3 Program Starting Address
#define LP5569_REG_PROG_MEM_PAGE_SELECT     0x4F    // Program Memory Page Select
#define LP5569_REG_PROGRAM_MEM_00           0x50    // MSB 0
#define LP5569_REG_PROGRAM_MEM_01           0x51    // LSB 0
#define LP5569_REG_PROGRAM_MEM_02           0x52    // MSB 1
#define LP5569_REG_PROGRAM_MEM_03           0x53    // LSB 1
#define LP5569_REG_PROGRAM_MEM_04           0x54    // MSB 2
#define LP5569_REG_PROGRAM_MEM_05           0x55    // LSB 2
#define LP5569_REG_PROGRAM_MEM_06           0x56    // MSB 3
#define LP5569_REG_PROGRAM_MEM_07           0x57    // LSB 3
#define LP5569_REG_PROGRAM_MEM_08           0x58    // MSB 4
#define LP5569_REG_PROGRAM_MEM_09           0x59    // LSB 4
#define LP5569_REG_PROGRAM_MEM_10           0x5A    // MSB 5
#define LP5569_REG_PROGRAM_MEM_11           0x5B    // LSB 5
#define LP5569_REG_PROGRAM_MEM_12           0x5C    // MSB 6
#define LP5569_REG_PROGRAM_MEM_13           0x5D    // LSB 6
#define LP5569_REG_PROGRAM_MEM_14           0x5E    // MSB 7
#define LP5569_REG_PROGRAM_MEM_15           0x5F    // LSB 7
#define LP5569_REG_PROGRAM_MEM_16           0x60    // MSB 8
#define LP5569_REG_PROGRAM_MEM_17           0x61    // LSB 8
#define LP5569_REG_PROGRAM_MEM_18           0x62    // MSB 9
#define LP5569_REG_PROGRAM_MEM_19           0x63    // LSB 9
#define LP5569_REG_PROGRAM_MEM_20           0x64    // MSB 10
#define LP5569_REG_PROGRAM_MEM_21           0x65    // LSB 10
#define LP5569_REG_PROGRAM_MEM_22           0x66    // MSB 11
#define LP5569_REG_PROGRAM_MEM_23           0x67    // LSB 11
#define LP5569_REG_PROGRAM_MEM_24           0x68    // MSB 12
#define LP5569_REG_PROGRAM_MEM_25           0x69    // LSB 12
#define LP5569_REG_PROGRAM_MEM_26           0x6A    // MSB 13
#define LP5569_REG_PROGRAM_MEM_27           0x6B    // LSB 13
#define LP5569_REG_PROGRAM_MEM_28           0x6C    // MSB 14
#define LP5569_REG_PROGRAM_MEM_29           0x6D    // LSB 14
#define LP5569_REG_PROGRAM_MEM_30           0x6E    // MSB 15
#define LP5569_REG_PROGRAM_MEM_31           0x6F    // LSB 15
#define LP5569_REG_ENGINE1_MAPPING1         0x70    // Engine 1 LED [8] and Master Fader Mapping
#define LP5569_REG_ENGINE1_MAPPING2         0x71    // Engine 1 LED [7:0] Mapping
#define LP5569_REG_ENGINE2_MAPPING1         0x72    // Engine 2 LED [8] and Master Fader Mapping
#define LP5569_REG_ENGINE2_MAPPING2         0x73    // Engine 2 LED [7:0] Mapping
#define LP5569_REG_ENGINE3_MAPPING1         0x74    // Engine 3 LED [8] and Master Fader Mapping
#define LP5569_REG_ENGINE3_MAPPING2         0x75    // Engine 3 LED [7:0] Mapping
#define LP5569_REG_PWM_CONFIG               0x80    // PWM Input Configuration
#define LP5569_REG_LED_FAULT1               0x81    // LED [8] Fault Status
#define LP5569_REG_LED_FAULT2               0x82    // LED [7:0] Fault Status
#define LP5569_REG_GENERAL_FAULT            0x83    // CP Cap UVLO and TSD Fault Status 


  // values for dimensioning and input validation
  static const uint8_t NumChannels = 9;
  static const uint8_t NumFaders = 3;
  static const uint8_t NumEngines = 3;
  static const uint8_t NumInstructions = 96;

  enum lp_err_code
  {
    LP_ERR_NONE = 0,
    LP_ERR_INVALID_CHANNEL,
    LP_ERR_INVALID_FADER,
    LP_ERR_INVALID_ENGINE,
    LP_ERR_PROGRAM_LENGTH,
    LP_ERR_PROGRAM_VALIDATION,
    LP_ERR_PROGRAM_PC,
    LP_ERR_GPIO_OFF
  };

  typedef enum lp_err_code lp_err_code; 

  // Initialization routines
  void lp5569_init();
  void lp5569_Disable();
  void lp5569_Reset();

  // control outputs directly
  lp_err_code lp5569_SetChannelPWM(uint8_t channel, uint8_t value);
  lp_err_code lp5569_SetMasterFader(uint8_t fader, uint8_t value);

  // More detailed channel configuration
  lp_err_code lp5569_SetLogBrightness(uint8_t channel, bool enable);
  lp_err_code lp5569_SetExtSource(uint8_t channel, bool enable);
  lp_err_code lp5569_SetDriveCurrent(uint8_t channel, uint8_t value);

  // Configure outputs
  lp_err_code lp5569_AssignChannelToMasterFader(uint8_t channel, uint8_t fader);


//  uint8_t lp5569_ReadReg(uint8_t reg);
//  void    lp5569_WriteReg(uint8_t reg, uint8_t val);

  // private data
//  uint8_t _address;


#endif
