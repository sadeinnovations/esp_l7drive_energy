/******************************************************************************
lp55231.cpp
Implementation file for LP55231 breakout board Arduino Library.

Byron Jacquot @ SparkFun Electronics
October 21, 2016
https://github.com/sparkfun/SparkFun_LP55231_Arduino_Library

Arduino library supporting LP55231 breakout board.

Can be instanciated two different ways:
As an Lp55231 object, offering simple control over the IC, with minimum ROM footprint.
As an Lp55231Engines object, adding diagnostic and execution engine support.

Resources:
Written using SparkFun Pro Micro controller, with an LP55231 breakout board.
Development environment specifics:
Written using Arduino 1.6.5

This code is released under the [MIT License](http://opensource.org/licenses/MIT).

Please review the LICENSE.md file included with this example. If you have any questions
or concerns with licensing, please contact techsupport@sparkfun.com.

Distributed as-is; no warranty is given.
******************************************************************************/

#include "i2c_rw.h"

#include "lp5569.h"


// register stuff
static const uint8_t REG_CONFIG = 0x00;
static const uint8_t REG_CNTRL1 = 0x01;
static const uint8_t REG_CNTRL2 = 0x02;
//static const uint8_t REG_RATIO_MSB = 0x02;
//static const uint8_t REG_RATIO_LSB = 0x03;
static const uint8_t REG_OUTPUT_ONOFF_MSB = 0x04;
static const uint8_t REG_OUTPUT_ONOFF_LSB = 0x05;

// Per LED control channels - fader channel assig, log dimming enable, temperature compensation
static const uint8_t REG_D1_CTRL = 0x07;
static const uint8_t REG_D2_CTRL = 0x08;
static const uint8_t REG_D3_CTRL = 0x09;
static const uint8_t REG_D4_CTRL = 0x0a;
static const uint8_t REG_D5_CTRL = 0x0b;
static const uint8_t REG_D6_CTRL = 0x0c;
static const uint8_t REG_D7_CTRL = 0x0d;
static const uint8_t REG_D8_CTRL = 0x0e;
static const uint8_t REG_D9_CTRL = 0x0f;

// 0x0f to 0x15 reserved

// Direct PWM control registers
static const uint8_t REG_D1_PWM  = 0x16;
static const uint8_t REG_D2_PWM  = 0x17;
static const uint8_t REG_D3_PWM  = 0x18;
static const uint8_t REG_D4_PWM  = 0x19;
static const uint8_t REG_D5_PWM  = 0x1a;
static const uint8_t REG_D6_PWM  = 0x1b;
static const uint8_t REG_D7_PWM  = 0x1c;
static const uint8_t REG_D8_PWM  = 0x1d;
static const uint8_t REG_D9_PWM  = 0x1e;

// 0x1f to 0x25 reserved

// Drive current registers
static const uint8_t REG_D1_I_CTL = 0x22;
static const uint8_t REG_D2_I_CTL  = 0x23;
static const uint8_t REG_D3_I_CTL  = 0x24;
static const uint8_t REG_D4_I_CTL  = 0x25;
static const uint8_t REG_D5_I_CTL  = 0x26;
static const uint8_t REG_D6_I_CTL  = 0x27;
static const uint8_t REG_D7_I_CTL  = 0x28;
static const uint8_t REG_D8_I_CTL  = 0x29;
static const uint8_t REG_D9_I_CTL  = 0x2a;

// 0x2f to 0x35 reserved

static const uint8_t REG_MISC     = 0x2F;
static const uint8_t REG_PC1      = 0x30;
static const uint8_t REG_PC2      = 0x31;
static const uint8_t REG_PC3      = 0x32;
static const uint8_t REG_STATUS_IRQ = 0x3C;
static const uint8_t REG_INT_GPIO   = 0x3D;
static const uint8_t REG_GLOBAL_VAR = 0x3E;
static const uint8_t REG_RESET      = 0x3F;

// 0x43 to 0x44 reserved

static const uint8_t REG_ENGINE_A_VAR = 0x42;
static const uint8_t REG_ENGINE_B_VAR = 0x43;
static const uint8_t REG_ENGINE_C_VAR = 0x44;

static const uint8_t REG_MASTER_FADE_1 = 0x46;
static const uint8_t REG_MASTER_FADE_2 = 0x47;
static const uint8_t REG_MASTER_FADE_3 = 0x48;

// 0x4b Reserved

static const uint8_t REG_PROG1_START = 0x4B;
static const uint8_t REG_PROG2_START = 0x4C;
static const uint8_t REG_PROG3_START = 0x4D;
static const uint8_t REG_PROG_PAGE_SEL = 0x4F;

// Memory is more confusing - there are 6 pages, sel by addr 4f
static const uint8_t REG_PROG_MEM_BASE = 0x50;
//static const uint8_t REG_PROG_MEM_SIZE = 0x;//
static const uint8_t REG_PROG_MEM_END  = 0x6f;

static const uint8_t REG_ENG1_MAP_MSB = 0x70;
static const uint8_t REG_ENG1_MAP_LSB = 0x71;
static const uint8_t REG_ENG2_MAP_MSB = 0x72;
static const uint8_t REG_ENG2_MAP_LSB = 0x73;
static const uint8_t REG_ENG3_MAP_MSB = 0x74;
static const uint8_t REG_ENG3_MAP_LSB = 0x75;

static const uint8_t REG_GAIN_CHANGE = 0x76;

uint8_t lp5569_ReadReg(uint8_t reg);
void lp5569_WriteReg(uint8_t reg, uint8_t val);


void lp5569_init()
{
  // enable internal clock & charge pump & write auto increment
  lp5569_WriteReg(REG_MISC, 0x41);

  // Set enable bit
  lp5569_WriteReg(REG_CONFIG, 0x40 );
}

void lp5569_Disable()
{
  uint8_t val;

  val = lp5569_ReadReg(REG_CONFIG);
  val &= ~0x40;
  lp5569_WriteReg(REG_CONFIG, val);
}

void lp5569_Reset()
{
  // force reset
  lp5569_WriteReg(REG_RESET, 0xff);
}

lp_err_code lp5569_SetChannelPWM(uint8_t channel, uint8_t value)
{
  if(channel >= NumChannels)
  {
    return LP_ERR_INVALID_CHANNEL;
  }

  lp5569_WriteReg(REG_D1_PWM + channel, value);
  return LP_ERR_NONE;
}

lp_err_code lp5569_SetMasterFader(uint8_t fader, uint8_t value)
{
  if(fader >= NumFaders)
  {
    return LP_ERR_INVALID_FADER;
  }

  lp5569_WriteReg(REG_MASTER_FADE_1 + fader, value);

  return LP_ERR_NONE;
}

lp_err_code lp5569_SetLogBrightness(uint8_t channel, bool enable)
{
  uint8_t regVal, bitVal;

  if(channel >= NumChannels)
  {
    return LP_ERR_INVALID_CHANNEL;
  }

  regVal = lp5569_ReadReg(REG_D1_CTRL + channel);
  bitVal = enable?0x08:0x00;
  regVal &= ~0x08;
  regVal |= bitVal;
  lp5569_WriteReg(REG_D1_CTRL + channel, regVal);

  return LP_ERR_NONE;
}

lp_err_code lp5569_SetExtSource(uint8_t channel, bool enable)
{
  uint8_t regVal, bitVal;

  if(channel >= NumChannels)
  {
    return LP_ERR_INVALID_CHANNEL;
  }

  regVal = lp5569_ReadReg(REG_D1_CTRL + channel);
  bitVal = enable?0x04:0x00;
  regVal &= ~0x04;
  regVal |= bitVal;
  lp5569_WriteReg(REG_D1_CTRL + channel, regVal);

  return LP_ERR_NONE;
}

lp_err_code lp5569_SetDriveCurrent(uint8_t channel, uint8_t value)
{
  if(channel >= NumChannels)
  {
    return LP_ERR_INVALID_CHANNEL;
  }

  lp5569_WriteReg(REG_D1_I_CTL + channel, value);
  return LP_ERR_NONE;
}


lp_err_code lp5569_AssignChannelToMasterFader(uint8_t channel, uint8_t fader)
{
  uint8_t regVal, bitVal;

  if(channel >= NumChannels)
  {
    return LP_ERR_INVALID_CHANNEL;
  }
  else if(fader >= NumFaders)
  {
    return LP_ERR_INVALID_FADER;
  }

  regVal = lp5569_ReadReg(REG_D1_CTRL + channel);
  bitVal = (fader + 1) & 0x03;
  bitVal <<= 5;
  regVal &= ~0xe0;
  regVal |= bitVal;
  lp5569_WriteReg(REG_D1_CTRL + channel, regVal);

  return LP_ERR_NONE;
}


/********************************************************************************/
/**  private base class member functions. **/
/********************************************************************************/

uint8_t lp5569_ReadReg(uint8_t reg)
{
  uint8_t RxBuffer;
  esp32_i2c_read_byte(LP5569_I2C_ADDRESS, reg, &RxBuffer);

  return RxBuffer;

}

void lp5569_WriteReg(uint8_t reg, uint8_t val)
{
  esp32_i2c_write_byte(LP5569_I2C_ADDRESS, reg, val);
	
}
