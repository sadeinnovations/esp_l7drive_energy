#ifndef _LP5569_SERVICE
#define _LP5569_SERVICE
TaskHandle_t lp5569_service();
double lp5569_write_led();
void infoled(uint32_t color, uint32_t duration);
#endif