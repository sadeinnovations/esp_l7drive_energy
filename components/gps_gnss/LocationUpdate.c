/* NMEA parsing example for ESP32.
 * Based on "parse_stdin.c" example from libnmea.
 * Copyright (c) 2015 Jack Engqvist Johansson.
 * Additions Copyright (c) 2017 Ivan Grokhotkov.
 * See "LICENSE" file in libnmea directory for license.
 */

#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "AwsDataUpdate.h"
#include "LocationUpdate.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "esp_task_wdt.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "gpgga.h"
#include "gpgll.h"
#include "gprmc.h"
#include "nmea.h"
#include <libGSM.h>
#include <stdio.h>

#ifdef CONFIG_GSM_DUAL_UART
static int gps_uart_num = UART_NUM_2;
#else //#elif CONFIG_GSM_SINGLE_UART
static int gps_uart_num = UART_NUM_1;
#endif
#define UART_TX_PIN 27
#define UART_RX_PIN 26
#define UART_RX_BUF_SIZE (1024)

#define EXAMPLE_TASK_PAUSE 300     // pause between task runs in seconds
#define TASK_SEMAPHORE_WAIT 140000 // time to wait for mutex in miliseconds

static const char *GPS_TAG = "[GPS]";


// ===============================================================================================
// ==== local variables for GPS data =============================================================
// ===============================================================================================
char gps_data[128] = {'\0'};
char *gpsdata = gps_data;

static gpsnmeadata_t nmea_data;
static gpsdata_t shadowGpsData;

static void uart_setup();
static int read_and_parse_nmea();

static void uart_setup() {
    uart_config_t uart_config = {.baud_rate = 115200,
                                 .data_bits = UART_DATA_8_BITS,
                                 .parity = UART_PARITY_DISABLE,
                                 .stop_bits = UART_STOP_BITS_1,
                                 .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
    uart_param_config(gps_uart_num, &uart_config);
    uart_set_pin(gps_uart_num, UART_TX_PIN, UART_RX_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(gps_uart_num, UART_RX_BUF_SIZE * 2, 0, 0, NULL, 0);
    // inserted flush 2.1.2019
    uart_flush(gps_uart_num);
}

bool get_gps_data(gpsnmeadata_t *data) {
    bool ret_val = false;
    if (xSemaphoreTake(GPSUpdateSemaphore, 10) == pdTRUE) {
        if (ggadataready) {
            memcpy(data, &nmea_data, sizeof(gpsnmeadata_t));
            ggadataready = false;
            ESP_LOGI(GPS_TAG, "nmea.longitude %s", nmea_data.longitude);
            ESP_LOGI(GPS_TAG, "data.longitude %s", data->longitude);
            ret_val = true;
        }
        xSemaphoreGive(GPSUpdateSemaphore);
    } else {
        ESP_LOGE(GPS_TAG, "Get GPS data: Failed to take GPSUpdateSemaphore!!!");
    }
    printf("get_gps_data ret: %s", ret_val ? "true" : "false");
    return ret_val;
}

static int read_and_parse_nmea(gpsnmeadata_t *parsed_data) {
    int res = 0;
    // Configure a temporary buffer for the incoming data
    char *buffer = (char *)malloc(UART_RX_BUF_SIZE + 1);
    char fmt_buf[32];
    size_t total_bytes = 0;
    bool ggadatafound = false;

    // let's insert flush to ensure data is not old
    uart_flush(gps_uart_num);
    int retryCounter = 4;
    while (!ggadatafound && retryCounter > 0) {
        // Read data from the UART
        retryCounter--;
        int read_bytes = uart_read_bytes(gps_uart_num, (uint8_t *)buffer + total_bytes, UART_RX_BUF_SIZE - total_bytes, 100 / portTICK_RATE_MS);
        if (read_bytes <= 0) {
            continue;
        }
        //printf("gps buffer: %s \n", buffer);
        printf("gps retry counter: %d \n", retryCounter);
        nmea_s *data;
        total_bytes += read_bytes;

        /* find start (a dollar sign) */
        char *start = memchr(buffer, '$', total_bytes);
        if (start == NULL) {
            total_bytes = 0;
            continue;
        }
        /* find end of line */
        char *end = memchr(start, '\r', total_bytes - (start - buffer));
        if (NULL == end || '\n' != *(++end)) {
            continue;
        }
        end[-1] = NMEA_END_CHAR_1;
        end[0] = NMEA_END_CHAR_2;
        /* handle data */
        data = nmea_parse(start, end - start + 1, 0);

        if (data != NULL) {
            if (data->errors != 0) {
                printf("WARN: The sentence struct contains parse errors!\n");
            }

            if (NMEA_GPGGA == data->type) {
                //return if we have found GGA data
                if (parseNmeaData(parsed_data, data)) {
                    printf("GPGGA found\n");
                    ggadatafound = true;
                }
            }

            if (NMEA_GPGLL == data->type) {
                //printf("GPGLL sentence\n");
                nmea_gpgll_s *pos = (nmea_gpgll_s *)data;
                //printf("Longitude:\n");
                //printf("  Degrees: %d\n", pos->longitude.degrees);
                //printf("  Minutes: %f\n", pos->longitude.minutes);
                //printf("  Cardinal: %c\n", (char)pos->longitude.cardinal);
                //printf("Latitude:\n");
                //printf("  Degrees: %d\n", pos->latitude.degrees);
                //printf("  Minutes: %f\n", pos->latitude.minutes);
                //printf("  Cardinal: %c\n", (char)pos->latitude.cardinal);
                strftime(fmt_buf, sizeof(fmt_buf), "%H:%M:%S", &pos->time);
                //printf("Time: %s\n", fmt_buf);
            }

            if (NMEA_GPRMC == data->type) {
                //printf("GPRMC sentence\n");
                nmea_gprmc_s *pos = (nmea_gprmc_s *)data;
                //printf("Longitude:\n");
                //printf("  Degrees: %d\n", pos->longitude.degrees);
                //printf("  Minutes: %f\n", pos->longitude.minutes);
                //printf("  Cardinal: %c\n", (char)pos->longitude.cardinal);
                //printf("Latitude:\n");
                //printf("  Degrees: %d\n", pos->latitude.degrees);
                //printf("  Minutes: %f\n", pos->latitude.minutes);
                //printf("  Cardinal: %c\n", (char)pos->latitude.cardinal);
                strftime(fmt_buf, sizeof(fmt_buf), "%H:%M:%S", &pos->time);
                //printf("Time: %s\n", fmt_buf);
            }

            nmea_free(data);
        }

        /* buffer empty? */
        if (end == buffer + total_bytes) {
            total_bytes = 0;
            continue;
        }

        /* copy rest of buffer to beginning */
        if (buffer != memmove(buffer, end, total_bytes - (end - buffer))) {
            total_bytes = 0;
            continue;
        }

        total_bytes -= end - buffer;
    }
    free(buffer);
    if (ggadatafound) {
        return 1;
    }
    // TODO: timeout
    else {
        return 0;
    }
}

bool ParseGpsData(gpsdata_t *data) {

    printf("start global gps parse");
    int value = 0;
    // int init_size = strlen(gps_data);
    // const char dummy_gps_data[] = "+CGPSINF: 0,6023.193800,2307.342600,29.000000,20121120093957.000,0,14,0.703760,284.519989";
    char delim[] = ", \t\r\n\v\f";

    if (strlen(gps_data) == 0) {
        printf("no data");
        return false;
    }

    char *ptr = strtok(gps_data, delim);
    // char *ptr = strtok(dummy_gps_data, delim);

    if (data == NULL || ptr == NULL) {
        printf("NULL tokens!");
        return false;
    }

    while (ptr != NULL) {
        ptr = strtok(NULL, delim);

        if (ptr == NULL) {
            continue;
        }
        if (value == 0) {
            strcpy(data->mode, ptr);
            printf("mode strcpy: '%s'\n", data->mode);
        } else if (value == 1) {
            strcpy(data->latitude, ptr);
            printf("latitude strcpy: '%s'\n", data->latitude);
            /*error check to ignore invalid satelite data as we see sometimes 0.000000 with simulator*/
            if (!strcmp(data->latitude, "0.000000")) {
                gps_data[0] = NULL;
                return false;
            }
        } else if (value == 2) {
            strcpy(data->longitude, ptr);
            printf("longitude strcpy: '%s'\n", data->longitude);
            /*error check to ignore invalid satelite data as we see sometimes 0.000000 with simulator*/
            if (!strcmp(data->longitude, "0.000000")) {
                gps_data[0] = NULL;
                return false;
            }
        } else if (value == 3) {
            strcpy(data->altitude, ptr);
            printf("altitude strcpy: '%s'\n", data->altitude);
        } else if (value == 4) {
            strcpy(data->UTC_time, ptr);
            printf("UTC_time strcpy: '%s'\n", data->UTC_time);
        } else if (value == 5) {
            strcpy(data->TTFF, ptr);
            printf("TTFF strcpy: '%s'\n", data->TTFF);
        } else if (value == 6) {
            strcpy(data->num_satelites, ptr);
            printf("num_satelites strcpy: '%s'\n", data->num_satelites);
        } else if (value == 7) {
            strcpy(data->speed, ptr);
            printf("speed strcpy: '%s'\n", data->speed);
        } else if (value == 8) {
            strcpy(data->course, ptr);
            printf("course strcpy: '%s'\n", data->course);
        } else if (ptr != NULL) {
            printf("end of data %s\n", ptr);
        }
        value++;
    }
    printf("GPS location data parsed\r\n");

    gps_data[0] = NULL;

    printf("global gps parse done!");
    return true;
}

bool parseNmeaData(gpsnmeadata_t *parsed_data, nmea_s *data) {
    char fmt_buf[32];

    if (xSemaphoreTake(GPSUpdateSemaphore, 10) == pdTRUE) {
        printf("GPGGA sentence\n");
        nmea_gpgga_s *gpgga = (nmea_gpgga_s *)data;
        printf("Number of satellites: %d\n", gpgga->n_satellites);
        printf("Altitude: %d %c\n", gpgga->altitude, gpgga->altitude_unit);
        printf("Longitude:\n");
        printf("  Degrees: %d\n", gpgga->longitude.degrees);
        printf("  Minutes: %f\n", gpgga->longitude.minutes);
        printf("  Cardinal: %c\n", (char)gpgga->longitude.cardinal);
        printf("Latitude:\n");
        printf("  Degrees: %d\n", gpgga->latitude.degrees);
        printf("  Minutes: %f\n", gpgga->latitude.minutes);
        printf("  Cardinal: %c\n", (char)gpgga->latitude.cardinal);
        strftime(fmt_buf, sizeof(fmt_buf), "%A, %B %d %Y %H:%M:%S", &gpgga->time);
        printf("Time: %s\n", fmt_buf);
        strcpy(parsed_data->time, fmt_buf);
        sprintf(parsed_data->altitude, "%d %c", gpgga->altitude, gpgga->altitude_unit);
        sprintf(parsed_data->longitude, "%d %f %c", gpgga->longitude.degrees, gpgga->longitude.minutes, gpgga->longitude.cardinal);
        sprintf(parsed_data->latitude, "%d %f %c", gpgga->latitude.degrees, gpgga->latitude.minutes, gpgga->latitude.cardinal);
        parsed_data->num_satelites = gpgga->n_satellites;
        printf("  time shadow: %s\n", parsed_data->time);
        printf("  altitude shadow: %s\n", parsed_data->altitude);
        printf("  longitude shadow: %s\n", parsed_data->longitude);
        printf("  latitude shadow: %s\n", parsed_data->latitude);
        printf("  num_satelites shadow: %i\n", parsed_data->num_satelites);

        xSemaphoreGive(GPSUpdateSemaphore);
        //return if we have found GGA data
        if (gpgga->longitude.degrees != 0 && gpgga->latitude.degrees != 0) {
            return true;
        }
        printf("GPS position degree values are 0\n");
    }
    return false;
}

// ==========================================================================================
void gps_task(void *pvParameters) {

    noGPSSemaphore = xSemaphoreCreateBinary();
    GPSUpdateSemaphore = xSemaphoreCreateBinary();
    xSemaphoreGive(GPSUpdateSemaphore);
    CelluStatusSemaphore = xSemaphoreCreateBinary();
    CANItemUpdateSemaphore = xSemaphoreCreateBinary();

    ESP_LOGI(GPS_TAG, "===== GPS INIT =================================================\n");
    esp_err_t err = esp_task_wdt_add(NULL);
    ESP_ERROR_CHECK(err);

    if (DUAL_UART == true) {
        uart_setup();
    }

    while (1) {

        ESP_LOGE(GPS_TAG, "===== WHILE LOOP ==================================\n");
        if (AT_GPS_UART == true) {
            if (!(xSemaphoreTake(http_mutex, TASK_SEMAPHORE_WAIT))) {
                ESP_LOGE(GPS_TAG, "===== ERROR: CANNOT GET MUTEX ==================================\n");
                vTaskDelay(30000 / portTICK_PERIOD_MS);
                continue;
            }
            // ** For AT command based GPS operations we have to go off line **
            ppposDisconnect(0, 0);
            vTaskDelay(2000 / portTICK_RATE_MS);
            // FIXME:Need to think how cellular status is updated in future...
            /*int res = get_cell_info(cellCsq, cellBand);
            if (res) {
                xSemaphoreGive(CelluStatusSemaphore);
                printf("cellCsq: %s cellBand: %s\r\n", cellCsq, cellBand);
            }*/
            /* REMOVE: GPS enable moved to SIM init loop...
            if (open_gps() == 1) {
                printf("GPS opened successfully\r\n");
            } else {
                printf("GPS open FAILED!\r\n");
            }
            */
            if (get_gps_fix_status() == 1) {
                printf("GPS 3D location fix \r\n");

                if (get_gps_location(&nmea_data) == 1) {
                    printf("GPS location data received\r\n");
                    ESP_LOGI(GPS_TAG, "GPSDATA: %s", nmea_data.longitude);
                    ggadataready = true;
                    xSemaphoreGive(GPSUpdateSemaphore);
                } else {
                    printf("GPS location data FAILED!\r\n");
                    xSemaphoreGive(noGPSSemaphore);
                }

            } else {
                printf("waiting for GPS location fix!\r\n");
                xSemaphoreGive(noGPSSemaphore);
            }
            ppposInit();
            xSemaphoreGive(http_mutex);
        }

        if (DUAL_UART == true) {
            ggadataready = read_and_parse_nmea(&nmea_data);
        }

        /*if (AT_GPS_UART == true) {
            if (ParseGpsData(&shadowGpsData)) {
                xSemaphoreGive(GPSUpdateSemaphore);
            }
        }*/

        // TODO: temp solution to tricker CAN update from GPS task
        // UpdateCanVehicleItems = true;
        // xSemaphoreGive(CANItemUpdateSemaphore);

        if (AT_GPS_UART == true) {
            // takes 10s to get MQTT back after disconnect, so lets not update faster than 30s.
            printf("GPS sleep 30 s \n");
            vTaskDelay(30000 / portTICK_RATE_MS);
        } else {
            vTaskDelay(5000 / portTICK_RATE_MS);
        }
        ESP_LOGE("HEAP check", "Free HEAP: %i", xPortGetFreeHeapSize());

        err = esp_task_wdt_reset();
        ESP_ERROR_CHECK(err);
    }

    ESP_LOGI(GPS_TAG, "Waiting %d sec...", EXAMPLE_TASK_PAUSE);
    ESP_LOGI(GPS_TAG, "================================================================\n\n");

    xSemaphoreGive(http_mutex);
    for (int countdown = EXAMPLE_TASK_PAUSE; countdown >= 0; countdown--) {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

/**
 * expected input format: $GNGGA,130338.000,0061.8663,N,00029.0804,E,1,8,0.99,106.4,M,18.3,M,,
 */
bool convertWGS84DegreeCoordsToGGAFormat(char *nmea_string) {
    size_t total_size = strlen(nmea_string) + 4;
    printf("input: %d; %s\n", total_size, nmea_string);
    char *tmp = malloc(total_size);
    char *iterator;
    int index = 0;
    iterator = strtok(nmea_string, ",");
    sprintf(tmp, "%s", iterator);
    iterator = strtok(NULL, ",");
    index++;
    while (iterator != NULL && strlen(iterator) > 0) {
        if (index == 2) {  //longitude
            char *degree = malloc(3);
            strncpy(degree, iterator+2, 2);
            degree[2] = '\0';
            char *minutes = malloc(5);
            strncpy(minutes, iterator+5, 4);
            minutes[4] = '\0';
            int minutes_value;
            sscanf(minutes, "%d", &minutes_value);
            double minute = (double)minutes_value / 10000 * 60;
            gcvt(minute, 7, minutes);
            if (minute < 10) {
                sprintf(tmp, "%s,%s0%s", tmp, degree, minutes);
            } else {
                sprintf(tmp, "%s,%s%s", tmp, degree, minutes);
            }
        } else if (index == 4) {  //latitude
            char *degree = malloc(4);
            strncpy(degree, iterator+2, 3);
            degree[3] = '\0';
            char *minutes = malloc(5);
            strncpy(minutes, iterator+6, 4);
            minutes[4] = '\0';
            int minutes_value;
            sscanf(minutes, "%d", &minutes_value);
            double minute = (double)minutes_value / 10000 * 60;
            gcvt(minute, 7, minutes);
            if (minute < 10) {
                sprintf(tmp, "%s,%s0%s", tmp, degree, minutes);
            } else {
                sprintf(tmp, "%s,%s%s", tmp, degree, minutes);
            }
        } else {
            sprintf(tmp, "%s,%s", tmp, iterator);
        }
        iterator = strtok(NULL, ",");
        index++;
    }
    sprintf(nmea_string, tmp);
    return true;
}