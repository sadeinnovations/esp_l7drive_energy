#ifndef _LOCATION_UPDATE_H_
#define _LOCATION_UPDATE_H_

#include "freertos/semphr.h"
#include "nmea.h"

#ifdef CONFIG_GSM_DUAL_UART
#define AT_GPS_UART false
#define DUAL_UART true
#else //#elif CONFIG_GSM_SINGLE_UART
#define AT_GPS_UART true
#define DUAL_UART false
#endif

SemaphoreHandle_t noGPSSemaphore;
SemaphoreHandle_t GPSUpdateSemaphore;
SemaphoreHandle_t CelluStatusSemaphore;
SemaphoreHandle_t CANItemUpdateSemaphore;

typedef struct
{
    char mode[2];
    char latitude[15];
    char longitude[15];
    char altitude[15];
    char UTC_time[15];
    char TTFF[15];
    char num_satelites[4];
    char speed[10];
    char course[10];
} gpsdata_t;

typedef struct
{
    char time[32];
    char latitude[32];
    char longitude[32];
    char altitude[32];
    int num_satelites;
} gpsnmeadata_t;

static bool ggadataready = false;

/*nmea data is based on this...
typedef struct {
        nmea_s base;
        struct tm time;
        nmea_position longitude;
        nmea_position latitude;
        int n_satellites;
        int altitude;
        char altitude_unit;
} nmea_gpgga_s;
*/
bool get_gps_data(gpsnmeadata_t *data);

bool ParseGpsData(gpsdata_t *data);

void gps_task(void *pvParameters);

bool parseNmeaData(gpsnmeadata_t *parsed_data, nmea_s *data);

bool convertWGS84DegreeCoordsToGGAFormat(char *nmea_string);

#endif