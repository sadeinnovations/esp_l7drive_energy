#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

// Log level (must be first than esp_log include) (choose one and comment anothers)
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
// #define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
// #define LOG_LOCAL_LEVEL ESP_LOG_INFO
// #define LOG_LOCAL_LEVEL ESP_LOG_WARN
// #define LOG_LOCAL_LEVEL ESP_LOG_ERROR

#include "esp_log.h"

// #define LOG_LEVEL LOG_LEVEL_VERBOSE
#define LOG_LEVEL LOG_LEVEL_LOGV    

#define UNUSED_PARAM(x)         (void)(x)

#define LOGV(TAG, fmt, ...) logV(TIME_STAMP, TAG, fmt, ##__VA_ARGS__)
#define logV(ts, TAG, fmt, ...) \
ESP_LOGV(TAG,  "%d:%02d:%02d:%03d - [%s, %d] %s: " fmt, \
GET_HOURS(ts), GET_MINUTES(ts), GET_SECONDS(ts), GET_MILLIS(ts), pcTaskGetTaskName(NULL), xPortGetCoreID(),  __func__, ##__VA_ARGS__)


#define LOGD(TAG, fmt, ...) logD(TIME_STAMP, TAG, fmt, ##__VA_ARGS__)
#define logD(ts, TAG, fmt, ...) \
ESP_LOGD(TAG,  "%d:%02d:%02d:%03d - [%s, %d] %s: " fmt, \
GET_HOURS(ts), GET_MINUTES(ts), GET_SECONDS(ts), GET_MILLIS(ts), pcTaskGetTaskName(NULL), xPortGetCoreID(),  __func__, ##__VA_ARGS__)


#define LOGI(TAG, fmt, ...) logI(TIME_STAMP, TAG, fmt, ##__VA_ARGS__)
#define logI(ts, TAG, fmt, ...) \
ESP_LOGI(TAG,  "%d:%02d:%02d:%03d - [%s, %d] %s: " fmt, \
GET_HOURS(ts), GET_MINUTES(ts), GET_SECONDS(ts), GET_MILLIS(ts), pcTaskGetTaskName(NULL), xPortGetCoreID(),  __func__, ##__VA_ARGS__)


#define LOGW(TAG, fmt, ...) logW(TIME_STAMP, TAG, fmt, ##__VA_ARGS__)
#define logW(ts, TAG, fmt, ...) \
ESP_LOGW(TAG,  "%d:%02d:%02d:%03d - [%s, %d] %s: " fmt, \
GET_HOURS(ts), GET_MINUTES(ts), GET_SECONDS(ts), GET_MILLIS(ts), pcTaskGetTaskName(NULL), xPortGetCoreID(),  __func__, ##__VA_ARGS__)


#define LOGE(TAG, fmt, ...) logE(TIME_STAMP, TAG, fmt, ##__VA_ARGS__)
#define logE(ts, TAG, fmt, ...) \
ESP_LOGE(TAG,  "%d:%02d:%02d:%03d - [%s, %d] %s: " fmt, \
GET_HOURS(ts), GET_MINUTES(ts), GET_SECONDS(ts), GET_MILLIS(ts), pcTaskGetTaskName(NULL), xPortGetCoreID(),  __func__, ##__VA_ARGS__)


#define TIME_STAMP (xTaskGetTickCount() * portTICK_PERIOD_MS)

#define GET_MILLIS(ts) ts % 1000
#define GET_SECONDS(ts) (ts / 1000) % 60
#define GET_MINUTES(ts) (ts / 60000) % 60
#define GET_HOURS(ts) ((ts / 60000) / 60)

#define LOG_INT(level, fmt, ts, ...) printf(level ": %d:%02d:%02d:%03d - [C:%d, T:%s - %s %s] " fmt "\n", GET_HOURS(ts), GET_MINUTES(ts), GET_SECONDS(ts), GET_MILLIS(ts), xPortGetCoreID(), pcTaskGetTaskName(NULL), __FILE__, __func__, ##__VA_ARGS__)

