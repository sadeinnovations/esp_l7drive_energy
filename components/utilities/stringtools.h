#ifndef _STRING_TOOLS_H
#define _STRING_TOOLS_H

#include <stdlib.h>
#include <string.h>

void remove_char(char* input, char c) {
    int len = strlen(input);
    int result_index = 0;
    for (int i = 0; i < len; i++) {
       if (input[i] != c) {
          input[result_index] = input[i];
          result_index++;
       }
    }
    input[result_index] = '\0';
}

void replace_char(char* input, char s, char d) {
    int len = strlen(input);
    for (int i = 0; i < len; i++) {
       if (input[i] == s) {
          input[i] = d;
       }
    }
}

#endif