#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "esp32/pm.h"
#include "esp_bt.h"
#include "esp_err.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_pm.h"
#include "esp_system.h"
#include "esp_vfs_fat.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "DataStorageHandler.h"

#define TAG "DataStorage"

void store_data_str_to_nvs(char *key, char *data) {

    esp_err_t err = ESP_OK;

    // Open
    nvs_handle storage_handle;
    err = nvs_open(STORAGE_NAME, NVS_READWRITE, &storage_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        return;
    }

    // Write
    err = nvs_set_str(storage_handle, key, data);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Data storing failed: key %s, data %s", key, data);
        nvs_close(storage_handle);
        return;
    }

    // Commit
    err = nvs_commit(storage_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Data storing commit failed: key %s, data %s", key, data);
        nvs_close(storage_handle);
        return;
    }

    ESP_LOGI(TAG, "Data stored: key %s, data %s", key, data);
    // Close
    nvs_close(storage_handle);
}

bool get_manufacturing_data_str(char *key, size_t *length, char *data) {
    return get_data_str_from_nvs_namespace(MANUFACTURING_NAMESPACE, key, length, data);
}

bool get_manufacturing_data_blob(char *key, size_t *length, uint8_t *data) {
    return get_data_blob_from_nvs_namespace(MANUFACTURING_NAMESPACE, key, length, data);
}

bool get_data_str_from_nvs(char *key, size_t *length, char *data) {
    return get_data_str_from_nvs_namespace(STORAGE_NAME, key, length, data);
}

bool get_data_str_from_nvs_namespace(char* nspace, char *key, size_t *length, char *data) {

    esp_err_t err = ESP_OK;

    ESP_LOGI(TAG, "Read str: %s/%s", nspace, key);
    // Open
    nvs_handle storage_handle;
    err = nvs_open(nspace, NVS_READWRITE, &storage_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        return false;
    }

    // Read
    printf("Reading data from NVS ...\n");
    err = nvs_get_str(storage_handle, key, data, length);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) reading NVS!\n", esp_err_to_name(err));
        nvs_close(storage_handle);
        return false;
    }

    // Close
    nvs_close(storage_handle);
    return true;
}

bool get_data_blob_from_nvs_namespace(char* nspace, char *key, size_t *length, uint8_t *data) {

    esp_err_t err = ESP_OK;

    ESP_LOGI(TAG, "Read blob: %s/%s", nspace, key);
    // Open
    nvs_handle storage_handle;
    err = nvs_open(nspace, NVS_READWRITE, &storage_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        return false;
    }

    // Read
    printf("Reading data from NVS ...\n");

    err = nvs_get_blob(storage_handle, key, data, length);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) reading NVS!\n", esp_err_to_name(err));
        nvs_close(storage_handle);
        return false;
    }

    // Close
    nvs_close(storage_handle);
    return true;
}
