#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MANUFACTURING_NAMESPACE     "manufacturing"
#define SERIAL_NUMBER_KEY           "serial"
#define DEVICE_IDENTITY_KEY         "deviceKey"
#define DEVICE_CERTIFICATE_KEY      "deviceCert"
#define AWS_ROOT_CERTIFICATE_KEY    "awsRootCert"
#define SERVER_ROOT_CERTIFICATE_KEY "serverCert"

#define STORAGE_NAME                "storage"
#define DISPLAY_NAME_KEY            "displayName"
#define APN_NAME_KEY                "apnName"
#define APN_USER_KEY                "apnUser"
#define APN_PWD_KEY                 "apnPwd"

void store_data_str_to_nvs(char *key, char *data);
bool get_manufacturing_data_str(char *key, size_t *length, char *data);
bool get_manufacturing_data_blob(char *key, size_t *length, uint8_t *data);
bool get_data_str_from_nvs(char *key, size_t *length, char *data);
bool get_data_str_from_nvs_namespace(char* nspace, char *key, size_t *length, char *data);
bool get_data_blob_from_nvs_namespace(char* nspace, char *key, size_t *length, uint8_t *data);