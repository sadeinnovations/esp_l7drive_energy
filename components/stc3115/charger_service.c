#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_system.h"
#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "i2c_rw.h"
#include "sensor_cache.h"
#include "DataStorageHandler.h"
#include "stc3115_Driver.h"
#include "Log.h"
#include <rom/rtc.h>

#define TAG "Charger"
#define CHARGERDETPIN GPIO_NUM_39

static uint16_t chargerUpdatePeriod = 900;
RTC_DATA_ATTR static bool prevChargerConnected = false;
static bool chargerConnected = true;
static int batterySoC = 100;
static jsonStruct_t chargerStatusjson  =    {"chargerConnected",            &chargerConnected,      sizeof(bool),     SHADOW_JSON_BOOL,     NULL};
// shadow delta item key has to be less than 15 chars, because it will be written to NVS and there is a limitation
static jsonStruct_t chargerUpdatejson  =    {"chgUpdtPer",            &chargerUpdatePeriod,      sizeof(int),     SHADOW_JSON_INT32,     NULL};
static jsonStruct_t batterySoCjson  =       {"batterySoc",            &batterySoC,      sizeof(int),     SHADOW_JSON_INT32,     NULL};

static struct jsonStruct *pchargerStatusjson = &chargerStatusjson;
static struct jsonStruct *pchargerUpdatejson = &chargerUpdatejson;
static struct jsonStruct *pbatterySoCjson = &batterySoCjson;

static int charger_wait(int waitseconds)
{   
    int elapsed = 0;
    while(elapsed<waitseconds)
    {
        vTaskDelay(1000 / portTICK_RATE_MS);
        elapsed++;
        if(device_parameters.enter_deep_sleep)
            break;
    }
    if(device_parameters.enter_deep_sleep)
        return 1;
    else
        return 0;

}

void charger_task(void * parm)
{
    int counter = 0;

    STC3115_ConfigData_TypeDef STC3115_ConfigData; 
	STC3115_BatteryData_TypeDef STC3115_BatteryData; 
    esp_err_t err = GasGauge_Initialization(&STC3115_ConfigData, &STC3115_BatteryData);
    if(err)
    {
        LOGE(TAG, "Gas gauge init failed\n");
    }
    else{

        QueueHandle_t xQueue_batterySoc;
        QueueHandle_t xQueue_batteryVolt;
        QueueHandle_t xQueue_batteryCurr;
        QueueHandle_t xQueue_charger;
        sensor_data_item dataitem;

        xQueue_batterySoc = xQueueCreate(1, sizeof(sensor_data_item));
        sensor_register(xQueue_batterySoc, sizeof(sensor_data_item), "batteryLevel");
        xQueue_batteryVolt = xQueueCreate(1, sizeof(sensor_data_item));
        sensor_register(xQueue_batteryVolt, sizeof(sensor_data_item), "batteryVoltage");
        xQueue_batteryCurr = xQueueCreate(1, sizeof(sensor_data_item));
        sensor_register(xQueue_batteryCurr, sizeof(sensor_data_item), "batteryCurrent");
        xQueue_charger = xQueueCreate(1, sizeof(sensor_data_item));
        sensor_register(xQueue_charger, sizeof(sensor_data_item), "chargerConnected");

        counter = chargerUpdatePeriod;
        
        // shadow is not read when woken up from deep sleep
        if(device_parameters.reset_reason != DEEPSLEEP_RESET)
        {
            // Get shadow delta queue handler and add settings json 
            QueueHandle_t deltaqueue = shadow_delta_register();
            xQueueSendToBack(deltaqueue,  ( void * ) &pchargerUpdatejson, 10);
        }
        // get shadow update queue handler
        QueueHandle_t updatequeue = shadow_update_register();

        // Set charger detection pin as input
        gpio_set_direction(CHARGERDETPIN, GPIO_MODE_INPUT);
        
        while(!device_parameters.enter_deep_sleep)
        {
            if(GasGauge_Task(&STC3115_ConfigData, &STC3115_BatteryData)==1)
            {
                printf("Vbat: %i mV, I=%i mA SoC=%i, C=%i, P=%i A=%i , CC=%d\r\n", 
                    STC3115_BatteryData.Voltage, 
                    STC3115_BatteryData.Current, 
                    STC3115_BatteryData.SOC, 
                    STC3115_BatteryData.ChargeValue, 
                    STC3115_BatteryData.Presence, 
                    STC3115_BatteryData.StatusWord >> 13, 
                    STC3115_BatteryData.ConvCounter); 

                time(&dataitem.timestamp);
                dataitem.data = (double)STC3115_BatteryData.SOC / 10;
                xQueueSendToBack(xQueue_batterySoc, &dataitem, 10);
                dataitem.data = STC3115_BatteryData.Voltage;
                xQueueSendToBack(xQueue_batteryVolt, &dataitem, 10);
                dataitem.data = STC3115_BatteryData.Current;
                xQueueSendToBack(xQueue_batteryCurr, &dataitem, 10);
                dataitem.data = gpio_get_level(CHARGERDETPIN);
                xQueueSendToBack(xQueue_charger, &dataitem, 10);
            }
            if(counter >= chargerUpdatePeriod)
            {
                // Read charger status gpio here
                chargerConnected = gpio_get_level(CHARGERDETPIN);

                // battery SoC is reported as 0.1% units
                batterySoC = STC3115_BatteryData.SOC/10;

                // send data to be updated to shadow
                if(chargerConnected != prevChargerConnected){
                    xQueueSendToBack(updatequeue, ( void * ) &pchargerStatusjson, 10);
                    prevChargerConnected = chargerConnected;
                }
                xQueueSendToBack(updatequeue, ( void * ) &pbatterySoCjson, 10);
                counter = 0; 
            }
            counter+=10;
            charger_wait(10);
            //printf("Charger update period %d\n", chargerUpdatePeriod);
        }
        STC3115_SetPowerSavingMode();
    }
    LOGI(TAG, "Charger shutdown\n");
    vTaskDelete(NULL);
}

TaskHandle_t charger_service()
{
    TaskHandle_t task_handle;
    xTaskCreate(&charger_task, "charger_task", 4096, NULL, 9, &task_handle);
    return task_handle;
}
