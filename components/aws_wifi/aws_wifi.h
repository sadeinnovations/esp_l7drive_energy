#ifndef _AWS_WIFI
#define _AWS_WIFI

#define WIFI_CONNECTED_BIT BIT0
/* FreeRTOS event group to signal when we are connected & ready to make a request */
EventGroupHandle_t s_wifi_event_group;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
//const int WIFI_CONNECTED_BIT = BIT0;

esp_err_t initialise_wifi();
void deinitialise_wifi();
#endif