#include "freertos/FreeRTOS.h"
#include <stdlib.h>
#include <stdio.h>

#include "SmartLock.h"
// #include "GattServer.h"
#include "AwsDataUpdate.h"
#include "Log.h"

#define TAG "SmartLock"

static bool lockState = true;

bool smartLockIsLocked(){
    return lockState;
}

void smartLockSet(bool locked, bool updateBle, bool updateShadow){
    LOGI(TAG, "setLock: %d", locked);
    if(lockState != locked){
       lockState = locked;
    }

    // if (updateBle) {
    //     bleUpdateSmartLockState(lockState);
    // }

    if (updateShadow) {
        //updateLockStatus(lockState ? false : true);
    }
}
