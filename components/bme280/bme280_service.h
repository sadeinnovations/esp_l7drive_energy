#ifndef _BME280_SERVICE
#define _BME280_SERVICE
TaskHandle_t bme280_service();
double bme280_get_temperature();
double bme280_get_humidity();
double bme280_get_pressure();
#endif