#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_system.h"
#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "i2c_rw.h"
#include "bme280.h"
#include "bme280_service.h"
#include "sensor_cache.h"

#define BME280_DEBUG

static struct bme280_data comp_data;
static struct bme280_dev dev;
static SemaphoreHandle_t bme280_data_mutex = NULL;

static void user_delay_ms(uint32_t milliseconds)
{
    // For some reason the deafult delay is not enoungh for BME280
    vTaskDelay(10*milliseconds / portTICK_RATE_MS);
}

void bme280_task(void * parm)
{
    
    int8_t rslt = BME280_OK;
    uint8_t settings_sel;
    int send_period = 15;
    char *string;
    QueueHandle_t xQueue_temp, xQueue_hum, xQueue_press;
    sensor_data_item dataitem;
    uint8_t retrycount = 10;

    // Register to aws task with handler to data queue
    // in return aws task gives handler to settings queue 
    // Settings queue is checked on every measurement cycle 
    // new settings are applied instantly
    // Settings could be also handled with a callback function that is given directly to shadow 

    dev.dev_id = BME280_I2C_ADDR_SEC;
    dev.intf = BME280_I2C_INTF;
    dev.read = sensor_i2c_read;
    dev.write = sensor_i2c_write;
    dev.delay_ms = user_delay_ms;
    bme280_soft_reset(&dev);
    while (bme280_init(&dev) != BME280_OK && retrycount != 0) {
        
        vTaskDelay(500 / portTICK_RATE_MS);
        retrycount--;
    }
    if(retrycount == 0)
    {
        printf("BME280 init failed\n");
    }
    else
    {   
 
        xQueue_temp = xQueueCreate( 1, sizeof( sensor_data_item ) );
        sensor_register(xQueue_temp, sizeof( sensor_data_item ), "poleSurfTemperature");
        // xQueue_hum = xQueueCreate( 1, sizeof( sensor_data_item ) );
        // sensor_register(xQueue_hum, sizeof( sensor_data_item ), "humidity");
        // xQueue_press = xQueueCreate( 1, sizeof( sensor_data_item ) );
        // sensor_register(xQueue_press, sizeof( sensor_data_item ), "pressure");
        /* Recommended mode of operation: Indoor navigation */
        dev.settings.osr_h = BME280_OVERSAMPLING_1X;
        dev.settings.osr_p = BME280_OVERSAMPLING_16X;
        dev.settings.osr_t = BME280_OVERSAMPLING_2X;
        dev.settings.filter = BME280_FILTER_COEFF_16;

        settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;

        bme280_set_sensor_settings(settings_sel, &dev);
        int counter = 0;
        while(!device_parameters.enter_deep_sleep)
        {
            bme280_set_sensor_mode(BME280_FORCED_MODE, &dev);
            vTaskDelay(5000 / portTICK_RATE_MS);
            xSemaphoreTake(bme280_data_mutex, portMAX_DELAY);
            rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, &dev);
            // convert pressure from Pa to mbar
            comp_data.pressure = comp_data.pressure/100; 
            xSemaphoreGive(bme280_data_mutex);
            //vTaskDelay(1000 / portTICK_RATE_MS);
            // Queue data to aws task if send time has elapsed
            if(rslt == BME280_OK)
            {
                time(&dataitem.timestamp);
                dataitem.data = comp_data.temperature;
                xQueueSendToBack( xQueue_temp, &dataitem, 10 );
                // dataitem.data = comp_data.humidity;
                // xQueueSendToBack( xQueue_hum, &dataitem, 10 );
                // dataitem.data = comp_data.pressure;
                // xQueueSendToBack( xQueue_press, &dataitem, 10 );
                counter = 0; 
                #ifdef BME280_DEBUG
                printf("bme280: temperature: %0.2f degreeCelsius, "
                     " humidity: %0.2f, pressure: %0.2f %%RH\n",
                     comp_data.temperature, comp_data.humidity, comp_data.pressure);
                #endif
            }

//            counter++;
        }
    }
    bme280_set_sensor_mode(BME280_SLEEP_MODE, &dev);
    printf("BME280 shutdown\n");
    vTaskDelete(NULL);
}

TaskHandle_t bme280_service()
{
    TaskHandle_t task_handle;
    bme280_data_mutex = xSemaphoreCreateMutex();
    xTaskCreate(&bme280_task, "bme280_task", 2048, NULL, 9, &task_handle);
    return task_handle;
}

double bme280_get_temperature()
{
    xSemaphoreTake(bme280_data_mutex, portMAX_DELAY);
    double data = comp_data.temperature;
    xSemaphoreGive(bme280_data_mutex);
    return data;
}

double bme280_get_pressure()
{
    xSemaphoreTake(bme280_data_mutex, portMAX_DELAY);
    double data = comp_data.pressure;
    xSemaphoreGive(bme280_data_mutex);
    return data;
}

double bme280_get_humidity()
{
    xSemaphoreTake(bme280_data_mutex, portMAX_DELAY);
    double data = comp_data.humidity;
    xSemaphoreGive(bme280_data_mutex);
    return data;
}