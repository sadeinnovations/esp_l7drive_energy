#ifndef _PCF8574
#define _PCF8574

#define PCF8574_GPIO_P0 (0x1<<0)
#define PCF8574_GPIO_P1 (0x1<<1)
#define PCF8574_GPIO_P2 (0x1<<2)
#define PCF8574_GPIO_P3 (0x1<<3)
#define PCF8574_GPIO_P4 (0x1<<4)
#define PCF8574_GPIO_P5 (0x1<<5)
#define PCF8574_GPIO_P6 (0x1<<6)
#define PCF8574_GPIO_P7 (0x1<<7)


esp_err_t pcf8574_config(uint8_t config);
esp_err_t pcf8574_pinWrite(uint8_t mask, uint8_t value);
uint8_t pcf8574_pinRead(uint8_t mask);

#endif