
#include <stdio.h>
#include "esp_log.h"
#include "esp_err.h"
#include "driver/i2c.h"
#include "sdkconfig.h"
#include "i2c_rw.h"

#include "pcf8574.h"

#define PCF8574A_ADDR 0x38

static uint8_t pcf8574_current_config;

/*
    Chip init:
    The PCF8574 has Quasi-bidirectional pins
    Pin has either weak pull-up or strong current draw
    ie. open collector with integrated pull up connected to input buffer with interrupt
    write output to 1 or 0 and input to 1
*/
esp_err_t pcf8574_config(uint8_t config)
{
  if(esp32_i2c_write_byte(PCF8574A_ADDR, config, config))
  {
    pcf8574_current_config = config;
    return 0;
  }
  else
  {
    return 1;
  }
}


/*
    mask is the bitmask where the value is to be written
*/
esp_err_t pcf8574_pinWrite(uint8_t mask, uint8_t data)
{
  // because there are no registers in PCF8574 we cannot read the current config
  // instead we have to compare it to previously written setup
  uint8_t new = data ? pcf8574_current_config | mask : pcf8574_current_config & ~mask; 

  if(esp32_i2c_write_byte(PCF8574A_ADDR, new, new))
  {
    pcf8574_current_config = new;
    return 0;
  }
  else
  {
    return 1;
  }
}


/*
    mask is the bitmask where data is read from
*/
uint8_t pcf8574_pinRead(uint8_t mask)
{
  uint8_t value;

  // chip returns NAK after first data so error checking cannot be used
  esp32_i2c_read_byte(PCF8574A_ADDR, pcf8574_current_config, &value);
  return (mask & value) / mask;
}

