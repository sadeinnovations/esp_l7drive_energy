#
# For a description of the syntax of this configuration file,
# see kconfig/kconfig-language.txt.
#
menu "SADE IoT Platform"

config SADE_IOT_CAN_ENABLED
    bool "Enable CAN Controller"

config SADE_IOT_BT_ENABLED
    bool "Enable Bluetooth"

config SADE_IOT_WLAN_ENABLED
    bool "Enable WLAN"

config SADE_IOT_MODEM_ENABLED
    bool "Enable Modem"

choice SADE_IOT_MODEM
    depends on SADE_IOT_MODEM_ENABLED
    bool "Select used Modem HW"
    default SADE_IOT_MODEM_7000
    help
        Select used modem HW

config SADE_IOT_MODEM_868
    bool "SADE_IOT_MODEM_868"
    help
        Use SIMCOM868 modem

config SADE_IOT_MODEM_7000
    bool "SADE_IOT_MODEM_7000"
    help
        Use SIMCOM7000E modem
endchoice

config SADE_IOT_SDCARD_CONFIGURATION_PATH
    string "Configuration file path"
    default "/sdcard/iot_platform_config.json"
    help
        Path and filename to the configuration file.
   
config SADE_IOT_ETHERNET_ENABLED
    bool "Enable Ethernet"

menu "Ethernet Sade"
    depends on SADE_IOT_ETHERNET_ENABLED
    choice PHY_MODEL
        prompt "Ethernet PHY Device"
        default PHY_LAN8720
        help
            Select the PHY driver to use for the example.
        config PHY_IP101
            bool "IP101"
            help
                IP101 is a single port 10/100 MII/RMII/TP/Fiber Fast Ethernet Transceiver.
        config PHY_TLK110
            bool "TLK110"
            help
                TLK110 is an Industrial 10/100Mbps Ethernet Physical Layer Transceiver.
        config PHY_LAN8720
            bool "LAN8720"
            help
                LAN8720 is a small footprint RMII 10/100 Ethernet Transceiver with HP Auto-MDIX Support.
    endchoice

    config PHY_ADDRESS
        int "Ethernet PHY Address"
        default 0
        range 0 31
        help
            PHY Address of your PHY device. It dependens on your schematic design.
    choice PHY_CLOCK_MODE
        prompt "Ethernet PHY Clock Mode"
        default PHY_CLOCK_GPIO0_OUT
        help
            Select external (input on GPIO0) or internal (output on GPIO0, GPIO16 or GPIO17) RMII clock.
        config PHY_CLOCK_GPIO0_IN
            bool "GPIO0 Input"
            help
                Input of 50MHz RMII clock on GPIO0.
        config PHY_CLOCK_GPIO0_OUT
            bool "GPIO0 Output"
            help
                Output the internal 50MHz RMII clock on GPIO0.     
    endchoice
    config PHY_CLOCK_MODE
        int
        default 0 if PHY_CLOCK_GPIO0_IN
        default 1 if PHY_CLOCK_GPIO0_OUT
    config PHY_USE_POWER_PIN
        bool "Use PHY Power (enable / disable) pin"
        default n
        help
            Use a GPIO "power pin" to power the PHY on/off during operation.
            When using GPIO0 to input RMII clock, the reset process will be interfered by this clock.
            So we need another GPIO to control the switch on / off of the RMII clock.
    if PHY_USE_POWER_PIN
        config PHY_POWER_PIN
            int "PHY Power GPIO"
            default 12
            range 0 33
            depends on PHY_USE_POWER_PIN
            help
                GPIO number to use for powering on/off the PHY.
    endif
    config PHY_SMI_MDC_PIN
        int "SMI MDC Pin Number"
        default 33
        range 0 33
        help
            GPIO number used for SMI clock signal.
    config PHY_SMI_MDIO_PIN
        int "SMI MDIO Pin Number"
        default 18
        range 0 33
        help
            GPIO number used for SMI data signal.
endmenu

config SADE_IOT_MQTT_ENABLED
    bool "Enable MQTT client other than AWS"

menu "MQTT Configuration"
    depends on SADE_IOT_MQTT_ENABLED
    config BROKER_URL
        string "Broker URL"
        default "mqtt://192.168.0.28:1883"
        help
            URL of the broker to connect to
    config TOPIC_COUNT
        int "Topic count for MQTT Broker"
        default 0
        range 0 4
        help    
            Amount of topics for MQTT Broker, in addtion to AWS
    
endmenu


endmenu