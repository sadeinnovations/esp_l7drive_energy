#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_system.h"
#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "sensor_cache.h"
#include "led_strip.h"
#include "Log.h"

#define TAG "LED STRIP SERVICE"

struct led_color_t ledbuf1[2];
struct led_color_t ledbuf2[2];

struct led_strip_t board_leds = {
    .rgb_led_type = RGB_LED_TYPE_SK6812,
    .rmt_channel = RMT_CHANNEL_1,
    .rmt_interrupt_num = 32,
    .gpio = GPIO_NUM_5,
    .led_strip_buf_1 = ledbuf1,
    .led_strip_buf_2 = ledbuf2,
    .led_strip_length = 2,
};

char ledColor[7] = "#000000";
bool ledEnabled = false;

void iot_led_color_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_led_enable_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);

jsonStruct_t ledColorJson = {"ledColor", ledColor, sizeof(ledColor) * sizeof(char) + 1, SHADOW_JSON_STRING, iot_led_color_callback_handler};
jsonStruct_t ledEnableJson = {"ledEnabled", &ledEnabled, sizeof(bool), SHADOW_JSON_BOOL, iot_led_enable_callback_handler};

void iot_led_show()
{
    char colorhex[6];
    uint32_t colornum;
    uint8_t r, g, b;
    memcpy(colorhex, &ledColor[1], sizeof(colorhex));
    colornum = strtol(colorhex, NULL, 16);
    r = (colornum >> 16) & 0xFF;
    g = (colornum >> 8) & 0xFF;
    b = colornum & 0xFF;
    ESP_LOGD(TAG, "LED enabled %d", ledEnabled);
    if (ledEnabled)
    {
        ESP_LOGD(TAG, "LED RGB values (R: %d, G: %d, B: %d)", r, g, b);
        led_strip_set_pixel_rgb(&board_leds, 0, r, g, b);
        led_strip_set_pixel_rgb(&board_leds, 1, r, g, b);
        led_strip_show(&board_leds);
    }
    else
    {
        led_strip_set_pixel_rgb(&board_leds, 0, 0, 0, 0);
        led_strip_set_pixel_rgb(&board_leds, 1, 0, 0, 0);
        led_strip_show(&board_leds);
    }
}

void iot_led_color_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    //memcpy(colorhex, *(int *)(pContext->pData+1), sizeof(colorhex));
    iot_led_show();
    QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pContext, 10);
    }
}

void iot_led_enable_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    //bool enabled = *(bool *)pContext->pData;
    iot_led_show();
    QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        xQueueSendToBack(updatequeue, (void *)&pContext, 10);
    }
}

void led_service()
{
    board_leds.access_semaphore = xSemaphoreCreateBinary();
    led_strip_init(&board_leds);
    // Get shadow delta queue handler and add settings json
    QueueHandle_t deltaqueue = shadow_delta_register();
    struct jsonStruct *pledColorJson = &ledColorJson;
    struct jsonStruct *pledEnableJson = &ledEnableJson;
    xQueueSendToBack(deltaqueue, (void *)&pledColorJson, 10);
    xQueueSendToBack(deltaqueue, (void *)&pledEnableJson, 10);
}