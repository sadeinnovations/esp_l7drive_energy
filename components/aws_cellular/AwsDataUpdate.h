#ifndef _AWSDATAUPDATE
#define _AWSDATAUPDATE
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


extern QueueHandle_t http_mutex;

void aws_update_init();
#endif