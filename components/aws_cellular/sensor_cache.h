#ifndef _SENSOR_CACHE
#define _SENSOR_CACHE
// #include "aws_iot_shadow_interface.h"

#define SENSOR_MAX_COUNT 50
#define SENSOR_NAME_LEN 20
#define DISPLAY_NAME_SIZE 30
#define SENSOR_QUEUE_LEN 10
#define SENSOR_SETTINGS_LEN 100
#define SENSOR_SETTINGS_QUEUE_LEN 5
#define SENSOR_DATA_QUEUE_LEN 1
#define DEFAULT_UPDATE_INTERVAL 15
#define TIMESTAMP_EPOCH_CUTOFF  1559520000L // June 3rd, 2019 00:00:00

typedef struct 
{
    QueueHandle_t settingsQueue;
    QueueHandle_t dataQueue;
    uint16_t dataItemLen;
    char sensorName[SENSOR_NAME_LEN];
    int updateInterval;
    time_t latest_timestamp;
    float latest_data;
    int inTopic;
} sensor_item;

typedef struct {
    time_t timestamp;
    float data;
} sensor_data_item;

typedef struct 
{
    bool enter_deep_sleep;
    bool first_data_sent;
    bool single_measurement;
    time_t sessionId;
    char deviceId[18];
    int reset_reason;
} device_parameters_struct;

device_parameters_struct device_parameters;

QueueHandle_t sensor_register(QueueHandle_t dataQueue, uint16_t dataItemLen, char *sensorName);
char* get_sensor_payload(const long int timestamp, char *deviceId, char *deviceName);
char* get_sensor_payload_multiple(char *deviceId, int topicNo);
QueueHandle_t shadow_delta_register();
QueueHandle_t shadow_update_register();

QueueHandle_t shadow_reported_and_desired_register();
#endif