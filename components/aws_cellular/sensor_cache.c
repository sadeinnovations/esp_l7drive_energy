#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "cJSON.h"
#include "math.h"
#include "AwsDataUpdate.h"
#include "aws_iot_shadow_interface.h"
#include "sensor_cache.h"

static const char *TAG = "sensor_cache";
#define TOPICCOUNT 4

static sensor_item sensorItems[SENSOR_MAX_COUNT];
static int sensorCount = 0;
static QueueHandle_t shadow_delta_queue = NULL;
static QueueHandle_t shadow_update_queue = NULL;
int sessionLength = 3600; // in secs
static QueueHandle_t shadow_reported_and_desired_queue = NULL;

 #ifdef CONFIG_L7_DRIVE_MODE_ENERGY
// parameters per topic
// remember to increase sensor_max_count when adding more items/sensors

char * AWS[] = {"batteryLevel", "batteryCapacity","batteryCurrent","batteryVoltage","batteryTemperature","batteryHeating",
                "heatingPower","batteryChargingPower","batteryCycleTest","bcsInputPower","totalOutputPower","operatingMode",
                "bcsTemperature","storageSwitchState","outputVoltage","bcsCooling","bcsCoolingPower","poleSurfTemperature", 
                "poleSurfHumidity", "poleFanPwm", "poleFanDirection", "poleFanSpeed", "poleTemperature", "poleHumidity",
                "minChargeLevel", "maxChargeLevel", "longitude", "latitude", "poleFanDirection"}; //1
int lenAws = (int)(sizeof(AWS)/sizeof(AWS[0])); 

char * BATTERY[] = { "batteryLevel","batteryCapacity", "batteryCurrent","batteryVoltage", "batteryTemperature","batteryHeating",
                "heatingPower", "batteryChargingPower","batteryCycleTest" };  //2
int lenBat = (int)(sizeof(BATTERY)/sizeof(BATTERY[0]));

char * DRIVE[] = { "bcsInputPower","totalOutputPower","operatingMode","bcsTemperature", "storageSwitchState", "outputVoltage", 
                "bcsCooling","bcsCoolingPower", "optimizedPower", "batteryCycleTest", "optimizedModeDuration", "minChargeLevel",
                "maxChargeLevel","shutdownLevel","stopOptimizeLevel"}; //4
int lenDri = (int)(sizeof(DRIVE)/sizeof(DRIVE[0]));

char * POLE[] = { "poleSurfTemperature", "poleSurfHumidity", "poleFanPwm", "poleFanDirection", "poleFanSpeed", "poleTemperature",
                 "poleHumidity","fanTempHystStart", "fanTempHystStop", "fanHumMin", "fanHumMax", "longitude", "latitude", "poleFanDirection" };  //8
int lenPol = (int)(sizeof(POLE)/sizeof(POLE[0]));
#endif
/*
    Registers new sensor to the system
    dataQueue is the handle of the string data queue that is sending measurement data to be sent out
    dataItemLen is length of one string item in the queue
    sensorName is the name of the sensor that is placed to the json as item name
    returned settingsQueue is data pipe from shadow to sensor task for adjusting for example the update interval
*/
QueueHandle_t sensor_register(QueueHandle_t dataQueue, uint16_t dataItemLen, char *sensorName)
{
    vTaskDelay(100 / portTICK_RATE_MS);
    if (sensorCount >= SENSOR_MAX_COUNT) {
        ESP_LOGE(TAG, "Sensor count exceeds max reservation %d, aborting...", SENSOR_MAX_COUNT);
        abort();
    }
    sensorItems[sensorCount].dataQueue = dataQueue;
    sensorItems[sensorCount].dataItemLen = dataItemLen * sizeof(char);
    strcpy(sensorItems[sensorCount].sensorName, sensorName);
    sensorItems[sensorCount].updateInterval = DEFAULT_UPDATE_INTERVAL;
    QueueHandle_t settingsQueue = xQueueCreate(SENSOR_SETTINGS_QUEUE_LEN, SENSOR_SETTINGS_LEN);
    sensorItems[sensorCount].settingsQueue = settingsQueue;
    sensorItems[sensorCount].latest_timestamp = 0;

    sensorItems[sensorCount].inTopic = 0;

    for (int i=0; i<lenAws;i++){
        if(!strcmp(AWS[i], sensorName)){
        sensorItems[sensorCount].inTopic += 1;
        break;
        }
    }    
    for (int i=0; i<lenBat;i++){
        if(!strcmp(BATTERY[i], sensorName)){
        sensorItems[sensorCount].inTopic += 2;
        break;
        }
    }
    for (int i=0; i<lenDri;i++){
        if(!strcmp(DRIVE[i], sensorName)){
        sensorItems[sensorCount].inTopic += 4;
        break;
        }
    }
    for (int i=0; i<lenPol;i++){
        if(!strcmp(POLE[i], sensorName)){
        sensorItems[sensorCount].inTopic += 8;
        break;
        }
    }    
    ESP_LOGI(TAG,"Sensor registration. No: %d Sensor name: %s, To topic: %d.", sensorCount, sensorName, sensorItems[sensorCount].inTopic);
    sensorCount++;
    
    return settingsQueue;
}

char *get_sensor_payload(const long int timestamp, char *deviceId, char *deviceName)
{
    // int timestamp;
    char *string = NULL;
    char buffer[20];
    bool dataInQueue = false;
    uint8_t retrycount = 10;
    cJSON *sensorjson = NULL;

        if(sensorCount == 0)
        {
            goto end;
        }
        /*
        cJSON *payloadjson = cJSON_CreateObject();
        cJSON *payloadArray = cJSON_AddArrayToObject(payloadjson, "payload");
        if (payloadArray == NULL)
        {
            goto end;
        }
        */
        sensorjson = cJSON_CreateObject();
        if (cJSON_AddStringToObject(sensorjson, "deviceId", deviceId) == NULL)
        {
            goto end;
        }
        time(&timestamp);
        sprintf(buffer, "%ld", timestamp);
        if (cJSON_AddStringToObject(sensorjson, "timestamp", buffer) == NULL)
        {
            goto end;
        }

        if(device_parameters.sessionId > 0)
        {
            sprintf(buffer, "%ld", device_parameters.sessionId);
            if (cJSON_AddStringToObject(sensorjson, "sessionId", buffer) == NULL)
            {
                goto end;
            }
        }
        
        //cJSON *valuesArray = cJSON_AddArrayToObject(sensorjson, "values");
        cJSON *valuesArray = cJSON_CreateArray();
        if (valuesArray == NULL)
        {
            goto end;
        }
        for(int c=0; c<sensorCount; c++)
        {
            sensor_data_item ulValReceived;
            // Check if there is data waiting in the queue
            if( uxQueueMessagesWaiting( sensorItems[c].dataQueue) == 0)
            {
                dataInQueue = false;
                // If waiting for sensor data for the first time
                if(sensorItems[c].latest_timestamp == 0)
                {
                    while(uxQueueMessagesWaiting( sensorItems[c].dataQueue) == 0 && retrycount > 0)
                    {
                        vTaskDelay(100 / portTICK_RATE_MS);
                        retrycount--;
                    }
                    if(retrycount == 0)
                    {
                        goto end;
                    }
                    else
                    {
                        dataInQueue = true;
                    }
                }              
            }
            else
            {
                dataInQueue = true;
            }

            cJSON* item  = cJSON_CreateObject(); 
            if(dataInQueue)
            {
                xQueueReceive( sensorItems[c].dataQueue, &ulValReceived, portMAX_DELAY );     
                sprintf(buffer, "%ld", ulValReceived.timestamp);
                sensorItems[c].latest_timestamp = ulValReceived.timestamp;
            }
            else
            {
                sprintf(buffer, "%ld", sensorItems[c].latest_timestamp);
            }
            if (cJSON_AddStringToObject(item, "ts", buffer) == NULL)
            {
                goto end;
            }

            if(dataInQueue)
            {
                sprintf(buffer, "%f", ulValReceived.data);
                sensorItems[c].latest_data = ulValReceived.data;
            }
            else
            {
                sprintf(buffer, "%f", sensorItems[c].latest_data);
            }
            if (cJSON_AddStringToObject(item, sensorItems[c].sensorName, buffer) == NULL)
            {
                goto end;
            }
            cJSON_AddItemToArray(valuesArray, item);

        }
        cJSON_AddItemToObject(sensorjson, "values", valuesArray);
        //cJSON_AddItemToArray(payloadArray, sensorjson);
        //string = cJSON_PrintUnformatted(payloadjson);
        string = cJSON_PrintUnformatted(sensorjson);
        if (string == NULL) {
            fprintf(stderr, "Failed to print sensorjson.\n");
        }

    end:
        if(sensorjson != NULL)
            cJSON_Delete(sensorjson);
        return string;

}

char* get_sensor_payload_multiple(char *deviceId, int topicNo)
{
    time_t timestamp;
    char *string = NULL;
    char buffer[20];
    bool dataInQueue = false;
    uint8_t retrycount = 10;
    cJSON *sensorjson = NULL;

        if(sensorCount == 0)
        {
            goto end;
        }
        /*
        cJSON *payloadjson = cJSON_CreateObject();
        cJSON *payloadArray = cJSON_AddArrayToObject(payloadjson, "payload");
        if (payloadArray == NULL)
        {
            goto end;
        }
        */
        sensorjson = cJSON_CreateObject();
        if (cJSON_AddStringToObject(sensorjson, "deviceId", deviceId) == NULL)
        {
            goto end;
        }
        time(&timestamp);        
        sprintf(buffer, "%ld", timestamp);
        if (cJSON_AddStringToObject(sensorjson, "timestamp", buffer) == NULL)
        {
            goto end;
        }

        if (device_parameters.sessionId > 0)
        {
            if (timestamp >= (device_parameters.sessionId + sessionLength)) device_parameters.sessionId = timestamp;
            sprintf(buffer, "%ld", device_parameters.sessionId);
            if (cJSON_AddStringToObject(sensorjson, "sessionId", buffer) == NULL)
            {
                ESP_LOGE(TAG, "sessionId == NULL");
                goto end;
            }
        }
        
        //cJSON *valuesArray = cJSON_AddArrayToObject(sensorjson, "values");
        cJSON *valuesArray = cJSON_CreateArray();
        if (valuesArray == NULL)
        {
            ESP_LOGE(TAG, "valuesArray == NULL");
            goto end;
        }
        bool noData = true;
        for(int c=0; c<sensorCount; c++)
        {
            sensor_data_item ulValReceived;
            if ( !(sensorItems[c].inTopic & (int)pow(2,topicNo))) continue;
            else {
                noData=false;
            }
            
            // Check if there is data waiting in the queue
            if( uxQueueMessagesWaiting( sensorItems[c].dataQueue) == 0)
            {
                dataInQueue = false;
                // If waiting for sensor data for the first time
                if(sensorItems[c].latest_timestamp == 0)
                {
                    while(uxQueueMessagesWaiting( sensorItems[c].dataQueue) == 0 && retrycount > 0)
                    {
                        vTaskDelay(100 / portTICK_RATE_MS);
                        retrycount--;
                    }
                    if(retrycount == 0)
                    {
                        goto end;
                    }
                    else
                    {
                        dataInQueue = true;
                    }
                }              
            }            
            else
            {
                dataInQueue = true;
            }
            if (noData==true) goto end;
            cJSON* item  = cJSON_CreateObject(); 
            if(dataInQueue)
            {
                xQueueReceive( sensorItems[c].dataQueue, &ulValReceived, portMAX_DELAY );
                sprintf(buffer, "%ld", ulValReceived.timestamp);
                sensorItems[c].latest_timestamp = ulValReceived.timestamp;
                if (ulValReceived.timestamp < TIMESTAMP_EPOCH_CUTOFF) goto end;
            }
            else
            {
                sprintf(buffer, "%ld", sensorItems[c].latest_timestamp);
            }
            if (cJSON_AddStringToObject(item, "ts", buffer) == NULL)
            {
                goto end;
            }

            if (dataInQueue)
            {
                sprintf(buffer, "%f", ulValReceived.data);
                sensorItems[c].latest_data = ulValReceived.data;
            }
            else
            {
                sprintf(buffer, "%f", sensorItems[c].latest_data);
            }
            if (cJSON_AddStringToObject(item, sensorItems[c].sensorName, buffer) == NULL)
            {
                goto end;
            }
            cJSON_AddItemToArray(valuesArray, item);

        }
        if (noData==true){
            return NULL;
        }
        cJSON_AddItemToObject(sensorjson, "values", valuesArray);
        //cJSON_AddItemToArray(payloadArray, sensorjson);
        //string = cJSON_PrintUnformatted(payloadjson);
        string = cJSON_PrintUnformatted(sensorjson);
        if (string == NULL) {
            fprintf(stderr, "Failed to print sensorjson.\n");
        }
        // emptying queues after all topics handled
        if (topicNo == TOPICCOUNT-1){
            for (int i = 0;i < sensorCount;i++){
                xQueueReset(sensorItems[i].dataQueue);
            }
        }
    end:
        if(sensorjson != NULL)
            cJSON_Delete(sensorjson);
        return string;
}

QueueHandle_t shadow_delta_register()
{
    if (shadow_delta_queue == NULL)
    {
        shadow_delta_queue = xQueueCreate(20, sizeof(struct jsonStruct *));
    }
    return shadow_delta_queue;
}

QueueHandle_t shadow_update_register()
{
    if (shadow_update_queue == NULL)
    {
        shadow_update_queue = xQueueCreate(20, sizeof(struct jsonStruct *));
    }
    return shadow_update_queue;
}

QueueHandle_t shadow_reported_and_desired_register()
{
    if (shadow_reported_and_desired_queue==NULL)
    {
        shadow_reported_and_desired_queue = xQueueCreate( 20, sizeof( struct jsonStruct * ) );
    }
    return shadow_reported_and_desired_queue;
}
