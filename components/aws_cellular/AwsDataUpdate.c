/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * Additions Copyright 2016 Espressif Systems (Shanghai) PTE LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
/**
 * @file subscribe_publish_sample.c
 * @brief simple MQTT publish and subscribe on the same topic
 *
 * This example takes the parameters from the build configuration and establishes a connection to the AWS IoT MQTT Platform.
 * It subscribes and publishes to the same topic - "test_topic/esp32"
 *
 * Some setup is required. See example README for details.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "cJSON.h"
#include "aws_wifi.h"
#include "AwsDataUpdate.h"
#include "sensor_cache.h"

#include "DataStorageHandler.h"
#include "version.h"

#ifdef CONFIG_L7_DRIVE_MODE_ENERGY
#include "energy_service.h"
#define TOPICCOUNT 4
#else
#define TOPICCOUNT 1
#endif

#define AWS_TOPIC 0
static const char *TAG = "subpub";

/* CA Root certificate, device ("Thing") certificate and device
 * ("Thing") key.

   Example can be configured one of two ways:

   "Embedded Certs" are loaded from files in "certs/" and embedded into the app binary.

   "Filesystem Certs" are loaded from the filesystem (SD card, etc.)

   See example README for more details.
*/

extern const uint8_t aws_root_ca_pem_start[] asm("_binary_aws_root_ca_pem_start");
extern const uint8_t aws_root_ca_pem_end[] asm("_binary_aws_root_ca_pem_end");
extern const uint8_t certificate_pem_crt_start[] asm("_binary_certificate_pem_crt_start");
extern const uint8_t certificate_pem_crt_end[] asm("_binary_certificate_pem_crt_end");
extern const uint8_t private_pem_key_start[] asm("_binary_private_pem_key_start");
extern const uint8_t private_pem_key_end[] asm("_binary_private_pem_key_end");
extern RTC_DATA_ATTR int16_t reset_count;

static char displayName[DISPLAY_NAME_SIZE];
static char deviceFwVersion[10];
static jsonStruct_t displayNamejson = {"displayName", &displayName, sizeof(displayName), SHADOW_JSON_STRING, NULL};
static jsonStruct_t espFwVersionJson    = {"deviceFwVersion", &deviceFwVersion, sizeof(deviceFwVersion), SHADOW_JSON_STRING, NULL};
static jsonStruct_t reset_reasonjson = {"resetReason", &device_parameters.reset_reason, sizeof(device_parameters.reset_reason), SHADOW_JSON_INT32, NULL};
static jsonStruct_t reset_countjson = {"resetCount", &reset_count, sizeof(reset_count), SHADOW_JSON_INT16, NULL};
static struct jsonStruct *presetReason = &reset_reasonjson;
static struct jsonStruct *presetCount = &reset_countjson;

static struct jsonStruct *pdisplayNamejson = &displayNamejson;
static struct jsonStruct *pespFwVersionJson = &espFwVersionJson;

/**
 * @brief Default MQTT HOST URL is pulled from the aws_iot_config.h
 */
char HostAddress[255] = AWS_IOT_MQTT_HOST;

/**
 * @brief Default MQTT port is pulled from the aws_iot_config.h
 */
uint32_t port = AWS_IOT_MQTT_PORT;
cJSON *reported;
cJSON *delta;

static int update_interval = DEFAULT_UPDATE_INTERVAL;
static jsonStruct_t AWSupdateIntervaljson  =  {"cUpdateInterval",            &update_interval,      sizeof(int),     SHADOW_JSON_INT32,     NULL};
static struct jsonStruct *pAWSupdateIntervaljson = &AWSupdateIntervaljson;

void aws_iot_get_shadow_callback(const char *thingName, ShadowActions_t action, Shadow_Ack_Status_t status,
                                 const char *pReceivedJsonDocument, void *pData)
{
    ESP_LOGI(TAG, "Get shadow callback");
    cJSON *json = cJSON_Parse(pReceivedJsonDocument);
    cJSON *state = cJSON_GetObjectItemCaseSensitive(json, "state");
    reported = cJSON_GetObjectItemCaseSensitive(state, "reported");
    if (reported != NULL)
    {
        char *reportedString = cJSON_Print(reported);
        ESP_LOGI(TAG, "Reported shadow: %s", reportedString);
    }
    delta = cJSON_GetObjectItemCaseSensitive(state, "delta");
    if (delta != NULL)
    {
        char *deltaString = cJSON_Print(delta);
        ESP_LOGI(TAG, "Delta shadow: %s", deltaString);
    }
}

void iot_control_mqtt_callback_handler(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen,
                                       IoT_Publish_Message_Params *params, void *pData)
{
    ESP_LOGI(TAG, "Control subscribe callback");
    ESP_LOGI(TAG, "%.*s\t%.*s", topicNameLen, topicName, (int)params->payloadLen, (char *)params->payload);
}

void iot_shadow_mqtt_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    ESP_LOGI(TAG, "Shadow subscribe callback");
    // Save data to NVS
    if (pContext->type == SHADOW_JSON_INT32)
    {
        char datastr[10];
        int value = *(int *)pContext->pData;
        sprintf(datastr, "%d", value);
        store_data_str_to_nvs(pContext->pKey, datastr);
    }
    // Send delta back to aws with update queue
    QueueHandle_t updatequeue = shadow_update_register();

    xQueueSendToBack(updatequeue, (void *)&pContext, 10);
}

void disconnectCallbackHandler(AWS_IoT_Client *pClient, void *data)
{
    ESP_LOGW(TAG, "MQTT Disconnect");
    IoT_Error_t rc = FAILURE;

    if (NULL == pClient)
    {
        return;
    }

    if (aws_iot_is_autoreconnect_enabled(pClient))
    {
        ESP_LOGI(TAG, "Auto Reconnect is enabled, Reconnecting attempt will start now");
    }
    else
    {
        ESP_LOGW(TAG, "Auto Reconnect not enabled. Starting manual reconnect...");
        rc = aws_iot_mqtt_attempt_reconnect(pClient);
        if (NETWORK_RECONNECTED == rc)
        {
            ESP_LOGW(TAG, "Manual Reconnect Successful");
        }
        else
        {
            ESP_LOGW(TAG, "Manual Reconnect Failed - %d", rc);
        }
    }
}

void getShadowObjectValue(struct jsonStruct *newDelta)
{
    ESP_LOGI(TAG, "Key: %s, Type: %d", newDelta->pKey, newDelta->type);
    if (reported != NULL)
    {
        cJSON *deltaValue = cJSON_GetObjectItemCaseSensitive(delta, newDelta->pKey);
        cJSON *reportedValue = cJSON_GetObjectItemCaseSensitive(reported, newDelta->pKey);
        cJSON *value;
        if (deltaValue != NULL)
        {
            value = deltaValue;
        }
        else
        {
            value = reportedValue;
        }
        if (newDelta->type == SHADOW_JSON_STRING && value != NULL)
        {
            strcpy(newDelta->pData, value->valuestring);
        }
        if ((newDelta->type == (JsonPrimitiveType)SHADOW_JSON_BOOL ||
             newDelta->type == (JsonPrimitiveType)SHADOW_JSON_INT32) &&
            value != NULL)
        {
            int *ptr = (int *)newDelta->pData;
            *ptr = value->valueint;
        }
        if (newDelta->type == SHADOW_JSON_FLOAT && value != NULL)
        {
            float *ptr = (float *)newDelta->pData;
            *ptr = (float)value->valuedouble;
        }
        if (newDelta->type == SHADOW_JSON_OBJECT && value != NULL)
        {
            if (cJSON_IsArray(value))
            {
                cJSON *outputArray = cJSON_CreateArray();
                if (value->child != NULL)
                {
                    cJSON *arrayItem = value->child;
                    cJSON_AddItemToArray(outputArray, cJSON_CreateString(arrayItem->valuestring));
                    while (arrayItem->next != NULL)
                    {
                        arrayItem = arrayItem->next;
                        cJSON_AddItemToArray(outputArray, cJSON_CreateString(arrayItem->valuestring));
                    }
                    cJSON_Delete(arrayItem);
                }
                char *outputString = cJSON_PrintUnformatted(outputArray);
                strcpy(newDelta->pData, outputString);
                cJSON_Delete(outputArray);
                free(outputString);
            }
        }
        if (newDelta->cb != NULL)
        {
            void (*callbackPtr)(const char *, uint32_t, jsonStruct_t *) = newDelta->cb;
            callbackPtr(NULL, 0, newDelta);
        }
    }
}

void aws_iot_task(void *param)
{
    snprintf(deviceFwVersion, 10, "%i.%i", FW_VERSION_MAJOR, FW_VERSION_MINOR);
    time_t timestamp = 0;
    time_t timestamp_prev = 0;
    // uint16_t update_interval = DEFAULT_UPDATE_INTERVAL;
    IoT_Error_t rc = FAILURE;

    AWS_IoT_Client client;
    IoT_Client_Init_Params mqttInitParams = iotClientInitParamsDefault;
    IoT_Client_Connect_Params connectParams = iotClientConnectParamsDefault;

    ESP_LOGI(TAG, "AWS IoT SDK Version %d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

     /* Wait for WiFI to show as connected */
    xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT,
                        false, true, portMAX_DELAY);

    // Initiate AWS Shadow connection

    ShadowInitParameters_t sp = ShadowInitParametersDefault;
    sp.pHost = HostAddress;
    sp.port = port;

    sp.pClientCRT = (const char *)certificate_pem_crt_start;
    sp.pClientKey = (const char *)private_pem_key_start;
    sp.pRootCA = (const char *)aws_root_ca_pem_start;
    sp.enableAutoReconnect = false;
    sp.disconnectHandler = NULL;

    ESP_LOGI(TAG, "Shadow Init");
    rc = aws_iot_shadow_init(&client, &sp);
    if (SUCCESS != rc)
    {
        ESP_LOGE(TAG, "aws_iot_shadow_init returned error %d, aborting...", rc);
        abort();
    }

    ShadowConnectParameters_t scp = ShadowConnectParametersDefault;
    scp.pMyThingName = device_parameters.deviceId;
    //scp.pMqttClientId = CONFIG_AWS_EXAMPLE_CLIENT_ID;
    //scp.mqttClientIdLen = (uint16_t)strlen(CONFIG_AWS_EXAMPLE_CLIENT_ID);
    scp.pMqttClientId = device_parameters.deviceId;
    scp.mqttClientIdLen = (uint16_t)strlen(device_parameters.deviceId);

    ESP_LOGI(TAG, "Shadow Connect");
    rc = aws_iot_shadow_connect(&client, &scp);
    if (SUCCESS != rc)
    {
        ESP_LOGE(TAG, "aws_iot_shadow_connect returned error %d, aborting...", rc);
        abort();
    }

    /*
     * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
     *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
     *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
     */
    rc = aws_iot_shadow_set_autoreconnect_status(&client, true);
    if (SUCCESS != rc)
    {
        ESP_LOGE(TAG, "Unable to set Auto Reconnect to true - %d, aborting...", rc);
        abort();
    }

    // Initiate MQTT connection

    mqttInitParams.enableAutoReconnect = false; // We enable this later below
    mqttInitParams.pHostURL = HostAddress;
    mqttInitParams.port = port;

    mqttInitParams.pRootCALocation = (const char *)aws_root_ca_pem_start;
    mqttInitParams.pDeviceCertLocation = (const char *)certificate_pem_crt_start;
    mqttInitParams.pDevicePrivateKeyLocation = (const char *)private_pem_key_start;

    mqttInitParams.mqttCommandTimeout_ms = 20000;
    mqttInitParams.tlsHandshakeTimeout_ms = 5000;
    mqttInitParams.isSSLHostnameVerify = true;
    mqttInitParams.disconnectHandler = disconnectCallbackHandler;
    mqttInitParams.disconnectHandlerData = NULL;

    rc = aws_iot_mqtt_init(&client, &mqttInitParams);
    if (SUCCESS != rc)
    {
        ESP_LOGE(TAG, "aws_iot_mqtt_init returned error : %d ", rc);
        abort();
    }

    connectParams.keepAliveIntervalInSec = 10;
    connectParams.isCleanSession = true;
    connectParams.MQTTVersion = MQTT_3_1_1;
    /* Client ID is set in the menuconfig of the example */
    //connectParams.pClientID = CONFIG_AWS_EXAMPLE_CLIENT_ID;
    //connectParams.clientIDLen = (uint16_t)strlen(CONFIG_AWS_EXAMPLE_CLIENT_ID);
    connectParams.pClientID = device_parameters.deviceId;
    connectParams.clientIDLen = (uint16_t)strlen(device_parameters.deviceId);
    connectParams.isWillMsgPresent = false;

    ESP_LOGI(TAG, "Connecting to AWS...");
    do
    {
        rc = aws_iot_mqtt_connect(&client, &connectParams);
        if (SUCCESS != rc)
        {
            ESP_LOGE(TAG, "Error(%d) connecting to %s:%d", rc, mqttInitParams.pHostURL, mqttInitParams.port);
            vTaskDelay(1000 / portTICK_RATE_MS);
        }
    } while (SUCCESS != rc);

    rc = aws_iot_mqtt_autoreconnect_set_status(&client, true);
    if (SUCCESS != rc)
    {
        ESP_LOGE(TAG, "Unable to set Auto Reconnect to true - %d", rc);
        abort();
    }

    IoT_Publish_Message_Params paramsQOS0;
    paramsQOS0.qos = QOS0;
    paramsQOS0.isRetained = 0;
    #ifndef CONFIG_L7_DRIVE_MODE_ENERGY
    const char *TOPIC_PUB = "sensordata";
    #endif
    #ifdef CONFIG_L7_DRIVE_MODE_ENERGY
    const char *TOPIC_PUB = "energydata";
    #endif
    const int TOPIC_PUB_LEN = strlen(TOPIC_PUB);

    const char *TOPIC_SUB = "control";
    const int TOPIC_SUB_LEN = strlen(TOPIC_SUB);

    const char *TOPIC_HS = "handshake";
    const int TOPIC_HS_LEN = strlen(TOPIC_HS);

    // Subscribe to control channel
    ESP_LOGI(TAG, "Subscribing...");
    rc = aws_iot_mqtt_subscribe(&client, TOPIC_SUB, TOPIC_SUB_LEN, QOS0, iot_control_mqtt_callback_handler, NULL);
    if (SUCCESS != rc)
    {
        ESP_LOGE(TAG, "Error subscribing : %d ", rc);
        abort();
    }
    // TODO: Possibly obsolete can be removed
    // Subscribe to shadow update channel
    // char FullShadowTopic[100];
    // const char *SHTOPICH = "$aws/things/";
    // const char *SHTOPICF = "/shadow/update/documents";
    // const int SHTOPIC_LEN = strlen(SHTOPICH)+strlen(SHTOPICF);
    // strcpy(FullShadowTopic, SHTOPICH);
    // strcat(FullShadowTopic, device_parameters.deviceId);
    // strcat(FullShadowTopic, SHTOPICF);
    // rc = aws_iot_mqtt_subscribe(&client, FullShadowTopic, SHTOPIC_LEN+strlen(device_parameters.deviceId), QOS0, iot_control_mqtt_callback_handler, NULL);
    // if(SUCCESS != rc) {
    //     ESP_LOGE(TAG, "Error subscribing : %d ", rc);
    //     abort();
    // }
    //time(&timestamp_prev);
    //update_interval = 60;

    // Temporary way to generate thing to aws
    // Try to read deviceId NVS key and if it is not foud
    char datastr[18];
    size_t datastrLen = sizeof(datastr);
    if (!get_data_str_from_nvs("deviceId", &datastrLen, datastr))
    {
        store_data_str_to_nvs("deviceId", device_parameters.deviceId);
        char handshakePayload[100];
        sprintf(handshakePayload, "{ \"deviceId\": \"%s\" }", device_parameters.deviceId);
        paramsQOS0.payloadLen = strlen(handshakePayload);
        paramsQOS0.payload = (void *)handshakePayload;
        aws_iot_mqtt_publish(&client, TOPIC_HS, TOPIC_HS_LEN, &paramsQOS0);
    }

    QueueHandle_t shadow_delta_queue = shadow_delta_register();
    QueueHandle_t shadow_update_queue = shadow_update_register();
    QueueHandle_t shadow_reported_and_desired_queue = shadow_reported_and_desired_register();

    xQueueSendToBack(shadow_delta_queue, ( void * ) &pdisplayNamejson, 10);
    xQueueSendToBack(shadow_update_queue, ( void * ) &pespFwVersionJson, 10);
   	xQueueSendToBack(shadow_update_queue, ( void * ) &presetReason, 10);
    xQueueSendToBack(shadow_update_queue, ( void * ) &presetCount, 10);

    aws_iot_shadow_get(&client, device_parameters.deviceId, aws_iot_get_shadow_callback, NULL, 4, true);

    while ((NETWORK_ATTEMPTING_RECONNECT == rc || NETWORK_RECONNECTED == rc || SUCCESS == rc))
    {
        //Max time the yield function will wait for read messages
        rc = aws_iot_mqtt_yield(&client, 100);
        rc = aws_iot_shadow_yield(&client, 100);
        if (NETWORK_ATTEMPTING_RECONNECT == rc)
        {
            rc = aws_iot_mqtt_yield(&client, 1000);
            rc = aws_iot_shadow_yield(&client, 1000);
            // If the client is attempting to reconnect we will skip the rest of the loop.
            continue;
        }

        time(&timestamp);

        if (timestamp - timestamp_prev > update_interval)
        {
            // char *cPayload = get_sensor_payload(device_parameters.deviceId);

            for(int topic=0;topic<TOPICCOUNT;topic++){
                char *cPayload = get_sensor_payload_multiple(device_parameters.deviceId, topic);
                if (cPayload != NULL)
                {
                    ESP_LOGI(TAG, "Sending sensor payload");
                    paramsQOS0.payloadLen = strlen(cPayload);
                    paramsQOS0.payload = (void *)cPayload;
                    if (topic == AWS_TOPIC){
                        rc = aws_iot_mqtt_publish(&client, TOPIC_PUB, TOPIC_PUB_LEN, &paramsQOS0);
                    }
                    else send_to_broker(strlen(paramsQOS0.payload), paramsQOS0.payload, topic);
                    free(cPayload);
                    timestamp_prev = timestamp;
                    device_parameters.first_data_sent = true;
                }
                else
                {
                    ESP_LOGE(TAG, "Sensor payload NULL");
                }
            }
            while (uxQueueMessagesWaiting(shadow_delta_queue) > 0 && reported != NULL)
            {
                struct jsonStruct *newdelta;
                ESP_LOGI(TAG, "Shadow Register Delta");
                xQueueReceive(shadow_delta_queue, &(newdelta), portMAX_DELAY);
                // This adds callback that eventually writes desired data back to shadow
                if (newdelta->cb == NULL)
                {
                    ESP_LOGI(TAG, "Delta callback empty insert default");
                    newdelta->cb = iot_shadow_mqtt_callback_handler;
                }
                getShadowObjectValue(newdelta);
                // register to shadow delta
                rc = aws_iot_shadow_register_delta(&client, newdelta);
            }
            while (uxQueueMessagesWaiting(shadow_update_queue) > 0)
            {
            // char JsonDocumentBuffer[512];
            char JsonDocumentBuffer[768];
                struct jsonStruct_t *newupdate;
                xQueueReceive(shadow_update_queue, &(newupdate), portMAX_DELAY);
                size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);
                aws_iot_shadow_init_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
                aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 1, newupdate);
                aws_iot_finalize_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
                rc = aws_iot_shadow_update(&client, device_parameters.deviceId, JsonDocumentBuffer, NULL, NULL, 4, true);
                ESP_LOGI(TAG, "Shadow update %s", JsonDocumentBuffer);
            }
            if (uxQueueMessagesWaiting(shadow_reported_and_desired_queue) > 0) {
            // char JsonDocumentBuffer[512];
            char JsonDocumentBuffer[768];
                struct jsonStruct_t* update;
                xQueueReceive(shadow_reported_and_desired_queue, &(update), portMAX_DELAY);
                size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);
                aws_iot_shadow_init_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
                aws_iot_shadow_add_desired(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 1, update);
                aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 1, update);
                aws_iot_finalize_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
                rc = aws_iot_shadow_update(&client, device_parameters.deviceId, JsonDocumentBuffer, NULL, NULL, 4, true);
                ESP_LOGI(TAG, "Shadow reported and desired update %s", JsonDocumentBuffer);
            }

            vTaskDelay(1000 / portTICK_RATE_MS);
        }
        vTaskDelay(1000 / portTICK_RATE_MS);
    }

    ESP_LOGE(TAG, "An error occurred in the main loop.");
    abort();
}

void aws_update_init()
{
    xTaskCreatePinnedToCore(&aws_iot_task, "aws_iot_task", 18432, NULL, 5, NULL, 1);
}