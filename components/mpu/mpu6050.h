/*
 * @files mpu6050.c and mpu6050.h
 * @author Gabriel Boni Vicari (133192@upf.br)
 * GEPID - Grupo de Pesquisa em Cultura Digital (http://gepid.upf.br/)
 * Universidade de Passo Fundo (http://www.upf.br/)
 * @brief MPU6050 library for ESP32 ESP-IDF.
 *
 * Based on I2Cdevlib's MPU6050 by Jeff Rowberg.
 */

/*
--------------------------------------------------------------------------------
I2Cdev device library code is placed under the MIT license.

Copyright (c) 2012 Jeff Rowberg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
--------------------------------------------------------------------------------
*/

#ifndef MPU6050_H
#define MPU6050_H

#include "i2c_rw.h"

// Address of MPU6050 (Can be 0x68 or 0x69):
#define MPU6050_ADDRESS_LOW                 0x68 // Address pin low (GND).
#define MPU6050_ADDRESS_HIGH                0x69 // Address pin high (VCC).
#define MPU6050_DEVICE                      MPU6050_ADDRESS_LOW

// Registers:
#define MPU6050_REGISTER_XG_OFFS_TC         0x00
#define MPU6050_REGISTER_YG_OFFS_TC         0x01
#define MPU6050_REGISTER_ZG_OFFS_TC         0x02
#define MPU6050_REGISTER_X_FINE_GAIN        0x03
#define MPU6050_REGISTER_Y_FINE_GAIN        0x04
#define MPU6050_REGISTER_Z_FINE_GAIN        0x05
#define MPU6050_REGISTER_XA_OFFS_H          0x06
#define MPU6050_REGISTER_XA_OFFS_L_TC       0x07
#define MPU6050_REGISTER_YA_OFFS_H          0x08
#define MPU6050_REGISTER_YA_OFFS_L_TC       0x09
#define MPU6050_REGISTER_ZA_OFFS_H          0x0A
#define MPU6050_REGISTER_ZA_OFFS_L_TC       0x0B
#define MPU6050_REGISTER_SELF_TEST_X        0x0D
#define MPU6050_REGISTER_SELF_TEST_Y        0x0E
#define MPU6050_REGISTER_SELF_TEST_Z        0x0F
#define MPU6050_REGISTER_SELF_TEST_A        0x10
#define MPU6050_REGISTER_XG_OFFS_USRH       0x13
#define MPU6050_REGISTER_XG_OFFS_USRL       0x14
#define MPU6050_REGISTER_YG_OFFS_USRH       0x15
#define MPU6050_REGISTER_YG_OFFS_USRL       0x16
#define MPU6050_REGISTER_ZG_OFFS_USRH       0x17
#define MPU6050_REGISTER_ZG_OFFS_USRL       0x18
#define MPU6050_REGISTER_SMPLRT_DIV         0x19
#define MPU6050_REGISTER_CONFIG             0x1A
#define MPU6050_REGISTER_GYRO_CONFIG        0x1B
#define MPU6050_REGISTER_ACCEL_CONFIG       0x1C
#define MPU6050_REGISTER_FF_THR             0x1D
#define MPU6050_REGISTER_FF_DUR             0x1E
#define MPU6050_REGISTER_MOT_THR            0x1F
#define MPU6050_REGISTER_MOT_DUR            0x20
#define MPU6050_REGISTER_ZRMOT_THR          0x21
#define MPU6050_REGISTER_ZRMOT_DUR          0x22
#define MPU6050_REGISTER_FIFO_EN            0x23
#define MPU6050_REGISTER_I2C_MST_CTRL       0x24
#define MPU6050_REGISTER_I2C_SLV0_ADDR      0x25
#define MPU6050_REGISTER_I2C_SLV0_REG       0x26
#define MPU6050_REGISTER_I2C_SLV0_CTRL      0x27
#define MPU6050_REGISTER_I2C_SLV1_ADDR      0x28
#define MPU6050_REGISTER_I2C_SLV1_REG       0x29
#define MPU6050_REGISTER_I2C_SLV1_CTRL      0x2A
#define MPU6050_REGISTER_I2C_SLV2_ADDR      0x2B
#define MPU6050_REGISTER_I2C_SLV2_REG       0x2C
#define MPU6050_REGISTER_I2C_SLV2_CTRL      0x2D
#define MPU6050_REGISTER_I2C_SLV3_ADDR      0x2E
#define MPU6050_REGISTER_I2C_SLV3_REG       0x2F
#define MPU6050_REGISTER_I2C_SLV3_CTRL      0x30
#define MPU6050_REGISTER_I2C_SLV4_ADDR      0x31
#define MPU6050_REGISTER_I2C_SLV4_REG       0x32
#define MPU6050_REGISTER_I2C_SLV4_DO        0x33
#define MPU6050_REGISTER_I2C_SLV4_CTRL      0x34
#define MPU6050_REGISTER_I2C_SLV4_DI        0x35
#define MPU6050_REGISTER_I2C_MST_STATUS     0x36
#define MPU6050_REGISTER_INT_PIN_CFG        0x37
#define MPU6050_REGISTER_INT_ENABLE         0x38
#define MPU6050_REGISTER_DMP_INT_STATUS     0x39
#define MPU6050_REGISTER_INT_STATUS         0x3A
#define MPU6050_REGISTER_ACCEL_XOUT_H       0x3B
#define MPU6050_REGISTER_ACCEL_XOUT_L       0x3C
#define MPU6050_REGISTER_ACCEL_YOUT_H       0x3D
#define MPU6050_REGISTER_ACCEL_YOUT_L       0x3E
#define MPU6050_REGISTER_ACCEL_ZOUT_H       0x3F
#define MPU6050_REGISTER_ACCEL_ZOUT_L       0x40
#define MPU6050_REGISTER_TEMP_OUT_H         0x41
#define MPU6050_REGISTER_TEMP_OUT_L         0x42
#define MPU6050_REGISTER_GYRO_XOUT_H        0x43
#define MPU6050_REGISTER_GYRO_XOUT_L        0x44
#define MPU6050_REGISTER_GYRO_YOUT_H        0x45
#define MPU6050_REGISTER_GYRO_YOUT_L        0x46
#define MPU6050_REGISTER_GYRO_ZOUT_H        0x47
#define MPU6050_REGISTER_GYRO_ZOUT_L        0x48
#define MPU6050_REGISTER_EXT_SENS_DATA_00   0x49
#define MPU6050_REGISTER_EXT_SENS_DATA_01   0x4A
#define MPU6050_REGISTER_EXT_SENS_DATA_02   0x4B
#define MPU6050_REGISTER_EXT_SENS_DATA_03   0x4C
#define MPU6050_REGISTER_EXT_SENS_DATA_04   0x4D
#define MPU6050_REGISTER_EXT_SENS_DATA_05   0x4E
#define MPU6050_REGISTER_EXT_SENS_DATA_06   0x4F
#define MPU6050_REGISTER_EXT_SENS_DATA_07   0x50
#define MPU6050_REGISTER_EXT_SENS_DATA_08   0x51
#define MPU6050_REGISTER_EXT_SENS_DATA_09   0x52
#define MPU6050_REGISTER_EXT_SENS_DATA_10   0x53
#define MPU6050_REGISTER_EXT_SENS_DATA_11   0x54
#define MPU6050_REGISTER_EXT_SENS_DATA_12   0x55
#define MPU6050_REGISTER_EXT_SENS_DATA_13   0x56
#define MPU6050_REGISTER_EXT_SENS_DATA_14   0x57
#define MPU6050_REGISTER_EXT_SENS_DATA_15   0x58
#define MPU6050_REGISTER_EXT_SENS_DATA_16   0x59
#define MPU6050_REGISTER_EXT_SENS_DATA_17   0x5A
#define MPU6050_REGISTER_EXT_SENS_DATA_18   0x5B
#define MPU6050_REGISTER_EXT_SENS_DATA_19   0x5C
#define MPU6050_REGISTER_EXT_SENS_DATA_20   0x5D
#define MPU6050_REGISTER_EXT_SENS_DATA_21   0x5E
#define MPU6050_REGISTER_EXT_SENS_DATA_22   0x5F
#define MPU6050_REGISTER_EXT_SENS_DATA_23   0x60
#define MPU6050_REGISTER_MOT_DETECT_STATUS  0x61
#define MPU6050_REGISTER_I2C_SLV0_DO        0x63
#define MPU6050_REGISTER_I2C_SLV1_DO        0x64
#define MPU6050_REGISTER_I2C_SLV2_DO        0x65
#define MPU6050_REGISTER_I2C_SLV3_DO        0x66
#define MPU6050_REGISTER_I2C_MST_DELAY_CTRL 0x67
#define MPU6050_REGISTER_SIGNAL_PATH_RESET  0x68
#define MPU6050_REGISTER_MOT_DETECT_CTRL    0x69
#define MPU6050_REGISTER_USER_CTRL          0x6A
#define MPU6050_REGISTER_PWR_MGMT_1         0x6B
#define MPU6050_REGISTER_PWR_MGMT_2         0x6C
#define MPU6050_REGISTER_BANK_SEL           0x6D
#define MPU6050_REGISTER_MEM_START_ADDR     0x6E
#define MPU6050_REGISTER_MEM_R_W            0x6F
#define MPU6050_REGISTER_DMP_CFG_1          0x70
#define MPU6050_REGISTER_DMP_CFG_2          0x71
#define MPU6050_REGISTER_FIFO_COUNTH        0x72
#define MPU6050_REGISTER_FIFO_COUNTL        0x73
#define MPU6050_REGISTER_FIFO_R_W           0x74
#define MPU6050_REGISTER_WHO_AM_I           0x75

// Self test values:
#define MPU6050_SELF_TEST_XA_1_BIT          0x07
#define MPU6050_SELF_TEST_XA_1_LENGTH       0x03
#define MPU6050_SELF_TEST_XA_2_BIT          0x05
#define MPU6050_SELF_TEST_XA_2_LENGTH       0x02
#define MPU6050_SELF_TEST_YA_1_BIT          0x07
#define MPU6050_SELF_TEST_YA_1_LENGTH       0x03
#define MPU6050_SELF_TEST_YA_2_BIT          0x03
#define MPU6050_SELF_TEST_YA_2_LENGTH       0x02
#define MPU6050_SELF_TEST_ZA_1_BIT          0x07
#define MPU6050_SELF_TEST_ZA_1_LENGTH       0x03
#define MPU6050_SELF_TEST_ZA_2_BIT          0x01
#define MPU6050_SELF_TEST_ZA_2_LENGTH       0x02
#define MPU6050_SELF_TEST_XG_1_BIT          0x04
#define MPU6050_SELF_TEST_XG_1_LENGTH       0x05
#define MPU6050_SELF_TEST_YG_1_BIT          0x04
#define MPU6050_SELF_TEST_YG_1_LENGTH       0x05
#define MPU6050_SELF_TEST_ZG_1_BIT          0x04
#define MPU6050_SELF_TEST_ZG_1_LENGTH       0x05

// DLPF values:
#define MPU6050_DLPF_BW_256                 0x00
#define MPU6050_DLPF_BW_188                 0x01
#define MPU6050_DLPF_BW_98                  0x02
#define MPU6050_DLPF_BW_42                  0x03
#define MPU6050_DLPF_BW_20                  0x04
#define MPU6050_DLPF_BW_10                  0x05
#define MPU6050_DLPF_BW_5                   0x06

// DHPF values:
#define MPU6050_DHPF_RESET                  0x00
#define MPU6050_DHPF_5                      0x01
#define MPU6050_DHPF_2P5                    0x02
#define MPU6050_DHPF_1P25                   0x03
#define MPU6050_DHPF_0P63                   0x04
#define MPU6050_DHPF_HOLD                   0x07

// Full scale gyroscope range:
#define MPU6050_GYRO_FULL_SCALE_RANGE_250   0x00
#define MPU6050_GYRO_FULL_SCALE_RANGE_500   0x01
#define MPU6050_GYRO_FULL_SCALE_RANGE_1000  0x02
#define MPU6050_GYRO_FULL_SCALE_RANGE_2000  0x03

// Full scale accelerometer range:
#define MPU6050_ACCEL_FULL_SCALE_RANGE_2    0x00
#define MPU6050_ACCEL_FULL_SCALE_RANGE_4    0x01
#define MPU6050_ACCEL_FULL_SCALE_RANGE_8    0x02
#define MPU6050_ACCEL_FULL_SCALE_RANGE_16   0x03

// Interrupt values:
#define MPU6050_INTMODE_ACTIVEHIGH          0x00
#define MPU6050_INTMODE_ACTIVELOW           0x01
#define MPU6050_INTDRV_PUSHPULL             0x00
#define MPU6050_INTDRV_OPENDRAIN            0x01
#define MPU6050_INTLATCH_50USPULSE          0x00
#define MPU6050_INTLATCH_WAITCLEAR          0x01
#define MPU6050_INTCLEAR_STATUSREAD         0x00
#define MPU6050_INTCLEAR_ANYREAD            0x01

// Clock sources:
#define MPU6050_CLOCK_INTERNAL              0x00
#define MPU6050_CLOCK_PLL_XGYRO             0x01
#define MPU6050_CLOCK_PLL_YGYRO             0x02
#define MPU6050_CLOCK_PLL_ZGYRO             0x03
#define MPU6050_CLOCK_PLL_EXTERNAL_32K      0x04
#define MPU6050_CLOCK_PLL_EXTERNAL_19M      0x05
#define MPU6050_CLOCK_KEEP_RESET            0x07

// Wake frequencies:
#define MPU6050_WAKE_FREQ_1P25              0x0
#define MPU6050_WAKE_FREQ_2P5               0x1
#define MPU6050_WAKE_FREQ_5                 0x2
#define MPU6050_WAKE_FREQ_10                0x3

// Decrement values:
#define MPU6050_DETECT_DECREMENT_RESET      0x0
#define MPU6050_DETECT_DECREMENT_1          0x1
#define MPU6050_DETECT_DECREMENT_2          0x2
#define MPU6050_DETECT_DECREMENT_4          0x3

// External sync values:
#define MPU6050_EXT_SYNC_DISABLED           0x0
#define MPU6050_EXT_SYNC_TEMP_OUT_L         0x1
#define MPU6050_EXT_SYNC_GYRO_XOUT_L        0x2
#define MPU6050_EXT_SYNC_GYRO_YOUT_L        0x3
#define MPU6050_EXT_SYNC_GYRO_ZOUT_L        0x4
#define MPU6050_EXT_SYNC_ACCEL_XOUT_L       0x5
#define MPU6050_EXT_SYNC_ACCEL_YOUT_L       0x6
#define MPU6050_EXT_SYNC_ACCEL_ZOUT_L       0x7

// Clock division values:
#define MPU6050_CLOCK_DIV_348               0x0
#define MPU6050_CLOCK_DIV_333               0x1
#define MPU6050_CLOCK_DIV_320               0x2
#define MPU6050_CLOCK_DIV_308               0x3
#define MPU6050_CLOCK_DIV_296               0x4
#define MPU6050_CLOCK_DIV_286               0x5
#define MPU6050_CLOCK_DIV_276               0x6
#define MPU6050_CLOCK_DIV_267               0x7
#define MPU6050_CLOCK_DIV_258               0x8
#define MPU6050_CLOCK_DIV_500               0x9
#define MPU6050_CLOCK_DIV_471               0xA
#define MPU6050_CLOCK_DIV_444               0xB
#define MPU6050_CLOCK_DIV_421               0xC
#define MPU6050_CLOCK_DIV_400               0xD
#define MPU6050_CLOCK_DIV_381               0xE
#define MPU6050_CLOCK_DIV_364               0xF

// CONFIG bits and lengths:
#define MPU6050_CFG_EXT_SYNC_SET_BIT        5
#define MPU6050_CFG_EXT_SYNC_SET_LENGTH     3
#define MPU6050_CFG_DLPF_CFG_BIT            2
#define MPU6050_CFG_DLPF_CFG_LENGTH         3

// GYRO_CONFIG bits and lengths:
#define MPU6050_GCONFIG_FS_SEL_BIT          4
#define MPU6050_GCONFIG_FS_SEL_LENGTH       2

// ACCEL_CONFIG bits and lengths:
#define MPU6050_ACONFIG_XA_ST_BIT           7
#define MPU6050_ACONFIG_YA_ST_BIT           6
#define MPU6050_ACONFIG_ZA_ST_BIT           5
#define MPU6050_ACONFIG_AFS_SEL_BIT         4
#define MPU6050_ACONFIG_AFS_SEL_LENGTH      2
#define MPU6050_ACONFIG_ACCEL_HPF_BIT       2
#define MPU6050_ACONFIG_ACCEL_HPF_LENGTH    3

// FIFO_EN bits:
#define MPU6050_TEMP_FIFO_EN_BIT            7
#define MPU6050_XG_FIFO_EN_BIT              6
#define MPU6050_YG_FIFO_EN_BIT              5
#define MPU6050_ZG_FIFO_EN_BIT              4
#define MPU6050_ACCEL_FIFO_EN_BIT           3
#define MPU6050_SLV2_FIFO_EN_BIT            2
#define MPU6050_SLV1_FIFO_EN_BIT            1
#define MPU6050_SLV0_FIFO_EN_BIT            0

// I2C_MST_CTRL bits and lengths:
#define MPU6050_MULT_MST_EN_BIT             7
#define MPU6050_WAIT_FOR_ES_BIT             6
#define MPU6050_SLV_3_FIFO_EN_BIT           5
#define MPU6050_I2C_MST_P_NSR_BIT           4
#define MPU6050_I2C_MST_CLK_BIT             3
#define MPU6050_I2C_MST_CLK_LENGTH          4

// I2C_SLV* bits and lengths:
#define MPU6050_I2C_SLV_RW_BIT              7
#define MPU6050_I2C_SLV_ADDR_BIT            6
#define MPU6050_I2C_SLV_ADDR_LENGTH         7
#define MPU6050_I2C_SLV_EN_BIT              7
#define MPU6050_I2C_SLV_BYTE_SW_BIT         6
#define MPU6050_I2C_SLV_REG_DIS_BIT         5
#define MPU6050_I2C_SLV_GRP_BIT             4
#define MPU6050_I2C_SLV_LEN_BIT             3
#define MPU6050_I2C_SLV_LEN_LENGTH          4

// I2C_SLV4 bits and lenghts:
#define MPU6050_I2C_SLV4_RW_BIT             7
#define MPU6050_I2C_SLV4_ADDR_BIT           6
#define MPU6050_I2C_SLV4_ADDR_LENGTH        7
#define MPU6050_I2C_SLV4_EN_BIT             7
#define MPU6050_I2C_SLV4_INT_EN_BIT         6
#define MPU6050_I2C_SLV4_REG_DIS_BIT        5
#define MPU6050_I2C_SLV4_MST_DLY_BIT        4
#define MPU6050_I2C_SLV4_MST_DLY_LENGTH     5

// I2C_MST_STATUS bits:
#define MPU6050_MST_PASS_THROUGH_BIT        7
#define MPU6050_MST_I2C_SLV4_DONE_BIT       6
#define MPU6050_MST_I2C_LOST_ARB_BIT        5
#define MPU6050_MST_I2C_SLV4_NACK_BIT       4
#define MPU6050_MST_I2C_SLV3_NACK_BIT       3
#define MPU6050_MST_I2C_SLV2_NACK_BIT       2
#define MPU6050_MST_I2C_SLV1_NACK_BIT       1
#define MPU6050_MST_I2C_SLV0_NACK_BIT       0

// INT_PIN_CFG bits:
#define MPU6050_INTCFG_INT_LEVEL_BIT        7
#define MPU6050_INTCFG_INT_OPEN_BIT         6
#define MPU6050_INTCFG_LATCH_INT_EN_BIT     5
#define MPU6050_INTCFG_INT_RD_CLEAR_BIT     4
#define MPU6050_INTCFG_FSYNC_INT_LEVEL_BIT  3
#define MPU6050_INTCFG_FSYNC_INT_EN_BIT     2
#define MPU6050_INTCFG_I2C_BYPASS_EN_BIT    1
#define MPU6050_INTCFG_CLKOUT_EN_BIT        0

// INT_ENABLE and INT_STATUS bits:
#define MPU6050_INTERRUPT_FF_BIT            7
#define MPU6050_INTERRUPT_MOT_BIT           6
#define MPU6050_INTERRUPT_ZMOT_BIT          5
#define MPU6050_INTERRUPT_FIFO_OFLOW_BIT    4
#define MPU6050_INTERRUPT_I2C_MST_INT_BIT   3
#define MPU6050_INTERRUPT_PLL_RDY_INT_BIT   2
#define MPU6050_INTERRUPT_DMP_INT_BIT       1
#define MPU6050_INTERRUPT_DATA_RDY_BIT      0

// MOT_DETECT_STATUS bits:
#define MPU6050_MOTION_MOT_XNEG_BIT         7
#define MPU6050_MOTION_MOT_XPOS_BIT         6
#define MPU6050_MOTION_MOT_YNEG_BIT         5
#define MPU6050_MOTION_MOT_YPOS_BIT         4
#define MPU6050_MOTION_MOT_ZNEG_BIT         3
#define MPU6050_MOTION_MOT_ZPOS_BIT         2
#define MPU6050_MOTION_MOT_ZRMOT_BIT        0

// I2C_MST_DELAY_CTRL bits:
#define MPU6050_DLYCTRL_DELAY_ES_SHADOW_BIT 7
#define MPU6050_DLYCTRL_I2C_SLV4_DLY_EN_BIT 4
#define MPU6050_DLYCTRL_I2C_SLV3_DLY_EN_BIT 3
#define MPU6050_DLYCTRL_I2C_SLV2_DLY_EN_BIT 2
#define MPU6050_DLYCTRL_I2C_SLV1_DLY_EN_BIT 1
#define MPU6050_DLYCTRL_I2C_SLV0_DLY_EN_BIT 0

// SIGNAL_PATH_RESET bits:
#define MPU6050_PATHRESET_GYRO_RESET_BIT    2
#define MPU6050_PATHRESET_ACCEL_RESET_BIT   1
#define MPU6050_PATHRESET_TEMP_RESET_BIT    0

// MOT_DETECT_CTRL bits and lengths:
#define MPU6050_DETECT_ACCEL_DELAY_BIT      5
#define MPU6050_DETECT_ACCEL_DELAY_LENGTH   2
#define MPU6050_DETECT_FF_COUNT_BIT         3
#define MPU6050_DETECT_FF_COUNT_LENGTH      2
#define MPU6050_DETECT_MOT_COUNT_BIT        1
#define MPU6050_DETECT_MOT_COUNT_LENGTH     2

// USER_CTRL bits:
#define MPU6050_USERCTRL_DMP_EN_BIT         7
#define MPU6050_USERCTRL_FIFO_EN_BIT        6
#define MPU6050_USERCTRL_I2C_MST_EN_BIT     5
#define MPU6050_USERCTRL_I2C_IF_DIS_BIT     4
#define MPU6050_USERCTRL_DMP_RESET_BIT      3
#define MPU6050_USERCTRL_FIFO_RESET_BIT     2
#define MPU6050_USERCTRL_I2C_MST_RESET_BIT  1
#define MPU6050_USERCTRL_SIG_COND_RESET_BIT 0

// PWR_MGMT_1 bits and lengths:
#define MPU6050_PWR1_DEVICE_RESET_BIT       7
#define MPU6050_PWR1_SLEEP_BIT              6
#define MPU6050_PWR1_CYCLE_BIT              5
#define MPU6050_PWR1_TEMP_DIS_BIT           3
#define MPU6050_PWR1_CLKSEL_BIT             2
#define MPU6050_PWR1_CLKSEL_LENGTH          3

// PWR_MGMT_2 bits and lengths:
#define MPU6050_PWR2_LP_WAKE_CTRL_BIT       7
#define MPU6050_PWR2_LP_WAKE_CTRL_LENGTH    2
#define MPU6050_PWR2_STBY_XA_BIT            5
#define MPU6050_PWR2_STBY_YA_BIT            4
#define MPU6050_PWR2_STBY_ZA_BIT            3
#define MPU6050_PWR2_STBY_XG_BIT            2
#define MPU6050_PWR2_STBY_YG_BIT            1
#define MPU6050_PWR2_STBY_ZG_BIT            0

// WHO_AM_I bit and length:
#define MPU6050_WHO_AM_I_BIT                6
#define MPU6050_WHO_AM_I_LENGTH             6

// Undocumented bits and lengths:
#define MPU6050_TC_PWR_MODE_BIT             7
#define MPU6050_TC_OFFSET_BIT               6
#define MPU6050_TC_OFFSET_LENGTH            6
#define MPU6050_TC_OTP_BNK_VLD_BIT          0
#define MPU6050_DMPINT_5_BIT                5
#define MPU6050_DMPINT_4_BIT                4
#define MPU6050_DMPINT_3_BIT                3
#define MPU6050_DMPINT_2_BIT                2
#define MPU6050_DMPINT_1_BIT                1
#define MPU6050_DMPINT_0_BIT                0

typedef struct _mpu6050_acceleration_t
{
    int16_t accel_x;
    int16_t accel_y;
    int16_t accel_z;
} mpu6050_acceleration_t;

typedef struct _mpu6050_rotation_t
{
    int16_t gyro_x;
    int16_t gyro_y;
    int16_t gyro_z;
} mpu6050_rotation_t;

uint8_t device_address;
uint8_t buffer [14];

void mpu6050_init ();
bool mpu6050_test_connection ();

// AUX_VDDIO Register:
uint8_t mpu6050_get_aux_vddio_level ();
void mpu6050_set_aux_vddio_level (uint8_t level);

// SMPLRT_DIV Register:
uint8_t mpu6050_get_rate ();
void mpu6050_set_rate (uint8_t rate);

// CONFIG Register:
uint8_t mpu6050_get_external_frame_sync ();
void mpu6050_set_external_frame_sync (uint8_t sync);
uint8_t mpu6050_get_dlpf_mode ();
void mpu6050_set_dlpf_mode (uint8_t mode);

// GYRO_CONFIG Register:
uint8_t mpu6050_get_full_scale_gyro_range ();
void mpu6050_set_full_scale_gyro_range (uint8_t range);

// SELF_TEST_X Register:
uint8_t mpu6050_get_accel_x_self_test_factory_trim ();
uint8_t mpu6050_get_accel_y_self_test_factory_trim ();
uint8_t mpu6050_get_accel_z_self_test_factory_trim ();
uint8_t mpu6050_get_gyro_x_self_test_factory_trim ();
uint8_t mpu6050_get_gyro_y_self_test_factory_trim ();
uint8_t mpu6050_get_gyro_z_self_test_factory_trim ();

// ACCEL_CONFIG Register:
bool mpu6050_get_accel_x_self_test ();
void mpu6050_set_accel_x_self_test (bool enabled);
bool mpu6050_get_accel_y_self_test ();
void mpu6050_set_accel_y_self_test (bool enabled);
bool mpu6050_get_accel_z_self_test ();
void mpu6050_set_accel_z_self_test (bool enabled);
uint8_t mpu6050_get_full_scale_accel_range ();
void mpu6050_set_full_scale_accel_range (uint8_t range);
uint8_t mpu6050_get_dhpf_mode ();
void mpu6050_set_dhpf_mode (uint8_t mode);

// FF_THR Register:
uint8_t mpu6050_get_freefall_detection_threshold ();
void mpu6050_set_freefall_detection_threshold (uint8_t threshold);

// FF_DUR Register:
uint8_t mpu6050_get_freefall_detection_duration ();
void mpu6050_set_freefall_detection_duration (uint8_t duration);

// MOT_THR Register:
uint8_t mpu6050_get_motion_detection_threshold ();
void mpu6050_set_motion_detection_threshold (uint8_t threshold);

// MOT_DUR Register:
uint8_t mpu6050_get_motion_detection_duration ();
void mpu6050_set_motion_detection_duration (uint8_t duration);

// ZRMOT_THR Register:
uint8_t mpu6050_get_zero_motion_detection_threshold ();
void mpu6050_set_zero_motion_detection_threshold (uint8_t threshold);

// ZRMOT_DUR Register:
uint8_t mpu6050_get_zero_motion_detection_duration ();
void mpu6050_set_zero_motion_detection_duration (uint8_t duration);

// FIFO_EN Register:
bool mpu6050_get_temp_fifo_enabled ();
void mpu6050_set_temp_fifo_enabled (bool enabled);
bool mpu6050_get_x_gyro_fifo_enabled ();
void mpu6050_set_x_gyro_fifo_enabled (bool enabled);
bool mpu6050_get_y_gyro_fifo_enabled ();
void mpu6050_set_y_gyro_fifo_enabled (bool enabled);
bool mpu6050_get_z_gyro_fifo_enabled ();
void mpu6050_set_z_gyro_fifo_enabled (bool enabled);
bool mpu6050_get_accel_fifo_enabled ();
void mpu6050_set_accel_fifo_enabled (bool enabled);
bool mpu6050_get_slave_2_fifo_enabled ();
void mpu6050_set_slave_2_fifo_enabled (bool enabled);
bool mpu6050_get_slave_1_fifo_enabled ();
void mpu6050_set_slave_1_fifo_enabled (bool enabled);
bool mpu6050_get_slave_0_fifo_enabled ();
void mpu6050_set_slave_0_fifo_enabled (bool enabled);

// I2C_MST_CTRL Register:
bool mpu6050_get_multi_master_enabled ();
void mpu6050_set_multi_master_enabled (bool enabled);
bool mpu6050_get_wait_for_external_sensor_enabled ();
void mpu6050_set_wait_for_external_sensor_enabled (bool enabled);
bool mpu6050_get_slave_3_fifo_enabled ();
void mpu6050_set_slave_3_fifo_enabled (bool enabled);
bool mpu6050_get_slave_read_write_transition_enabled ();
void mpu6050_set_slave_read_write_transition_enabled (bool enabled);
uint8_t mpu6050_get_master_clock_speed ();
void mpu6050_set_master_clock_speed (uint8_t speed);

// I2C_SLV* Registers:
uint8_t mpu6050_get_slave_address (uint8_t num);
void mpu6050_set_slave_address (uint8_t num, uint8_t address);
uint8_t mpu6050_get_slave_register (uint8_t num);
void mpu6050_set_slave_register (uint8_t num, uint8_t reg);
bool mpu6050_get_slave_enabled (uint8_t num);
void mpu6050_set_slave_enabled (uint8_t num, bool enabled);
bool mpu6050_get_slave_word_byte_swap (uint8_t num);
void mpu6050_set_slave_word_byte_swap (uint8_t num, bool enabled);
bool mpu6050_get_slave_write_mode (uint8_t num);
void mpu6050_set_slave_write_mode (uint8_t num, bool mode);
bool mpu6050_get_slave_word_group_offset (uint8_t num);
void mpu6050_set_slave_word_group_offset (uint8_t num, bool enabled);
uint8_t mpu6050_get_slave_data_length (uint8_t num);
void mpu6050_set_slave_data_length (uint8_t num, uint8_t length);

// I2C_SLV4 Register:
uint8_t mpu6050_get_slave_4_address ();
void mpu6050_set_slave_4_address (uint8_t address);
uint8_t mpu6050_get_slave_4_register ();
void mpu6050_set_slave_4_register (uint8_t reg);
void mpu6050_set_slave_4_output_byte (uint8_t data);
bool mpu6050_get_slave_4_enabled ();
void mpu6050_set_slave_4_enabled (bool enabled);
bool mpu6050_get_slave_4_interrupt_enabled ();
void mpu6050_set_slave_4_interrupt_enabled (bool enabled);
bool mpu6050_get_slave_4_write_mode ();
void mpu6050_set_slave_4_write_mode (bool mode);
uint8_t mpu6050_get_slave_4_master_delay ();
void mpu6050_set_slave_4_master_delay (uint8_t delay);
uint8_t mpu6050_get_slave_4_input_byte ();

// I2C_MST_STATUS Register:
bool mpu6050_get_passthrough_status ();
bool mpu6050_get_slave_4_is_done ();
bool mpu6050_get_lost_arbitration ();
bool mpu6050_get_slave_4_nack ();
bool mpu6050_get_slave_3_nack ();
bool mpu6050_get_slave_2_nack ();
bool mpu6050_get_slave_1_nack ();
bool mpu6050_get_slave_0_nack ();

// INT_PIN_CFG Register:
bool mpu6050_get_interrupt_mode ();
void mpu6050_set_interrupt_mode (bool mode);
bool mpu6050_get_interrupt_drive ();
void mpu6050_set_interrupt_drive (bool drive);
bool mpu6050_get_interrupt_latch ();
void mpu6050_set_interrupt_latch (bool latch);
bool mpu6050_get_interrupt_latch_clear ();
void mpu6050_set_interrupt_latch_clear (bool clear);
bool mpu6050_get_fsync_interrupt_level ();
void mpu6050_set_fsync_interrupt_level (bool level);
bool mpu6050_get_fsync_interrupt_enabled ();
void mpu6050_set_fsync_interrupt_enabled (bool enabled);
bool mpu6050_get_i2c_bypass_enabled ();
void mpu6050_set_i2c_bypass_enabled (bool enabled);
bool mpu6050_get_clock_output_enabled ();
void mpu6050_set_clock_output_enabled (bool enabled);

// INT_ENABLE Register:
uint8_t mpu6050_get_int_enabled ();
void mpu6050_set_int_enabled (uint8_t enabled);
bool mpu6050_get_int_freefall_enabled ();
void mpu6050_set_int_freefall_enabled (bool enabled);
bool mpu6050_get_int_motion_enabled ();
void mpu6050_set_int_motion_enabled (bool enabled);
bool mpu6050_get_int_zero_motion_enabled ();
void mpu6050_set_int_zero_motion_enabled (bool enabled);
bool mpu6050_get_int_fifo_buffer_overflow_enabled ();
void mpu6050_set_int_fifo_buffer_overflow_enabled (bool enabled);
bool mpu6050_get_int_i2c_master_enabled ();
void mpu6050_set_int_i2c_master_enabled (bool enabled);
bool mpu6050_get_int_data_ready_enabled ();
void mpu6050_set_int_data_ready_enabled (bool enabled);

// INT_STATUS Register:
uint8_t mpu6050_get_int_status ();
bool mpu6050_get_int_freefall_status ();
bool mpu6050_get_int_motion_status ();
bool mpu6050_get_int_zero_motion_status ();
bool mpu6050_get_int_fifo_buffer_overflow_status ();
bool mpu6050_get_int_i2c_master_status ();
bool mpu6050_get_int_data_ready_status ();

// ACCEL_*OUT_* Registers:
void mpu6050_get_acceleration (mpu6050_acceleration_t* data);
int16_t mpu6050_get_acceleration_x ();
int16_t mpu6050_get_acceleration_y ();
int16_t mpu6050_get_acceleration_z ();

// TEMP_OUT_* Registers:
int16_t mpu6050_get_temperature ();

// GYRO_*OUT_* Registers:
void mpu6050_get_rotation (mpu6050_rotation_t* data);
int16_t mpu6050_get_rotation_x ();
int16_t mpu6050_get_rotation_y ();
int16_t mpu6050_get_rotation_z ();

// ACCEL_*OUT_* and GYRO_*OUT_* Registers:
void mpu6050_get_motion (mpu6050_acceleration_t* data_accel,
mpu6050_rotation_t* data_gyro);

// EXT_SENS_DATA_* Registers:
uint8_t mpu6050_get_external_sensor_byte (int position);
uint16_t mpu6050_get_external_sensor_word (int position);
uint32_t mpu6050_get_external_sensor_dword (int position);

// MOT_DETECT_STATUS Register:
uint8_t mpu6050_get_motion_status ();
bool mpu6050_get_x_negative_motion_detected ();
bool mpu6050_get_x_positive_motion_detected ();
bool mpu6050_get_y_negative_motion_detected ();
bool mpu6050_get_y_positive_motion_detected ();
bool mpu6050_get_z_negative_motion_detected ();
bool mpu6050_get_z_positive_motion_detected ();
bool mpu6050_get_zero_motion_detected ();

// I2C_SLV*_DO Register:
void mpu6050_set_slave_output_byte (uint8_t num, uint8_t data);

// I2C_MST_DELAY_CTRL Register:
bool mpu6050_get_external_shadow_delay_enabled ();
void mpu6050_set_external_shadow_delay_enabled (bool enabled);
bool mpu6050_get_slave_delay_enabled (uint8_t num);
void mpu6050_set_slave_delay_enabled (uint8_t num, bool enabled);

// SIGNAL_PATH_RESET Register:
void mpu6050_reset_gyroscope_path ();
void mpu6050_reset_accelerometer_path ();
void mpu6050_reset_temperature_path ();

// MOT_DETECT_CTRL Register:
uint8_t mpu6050_get_accelerometer_power_on_delay ();
void mpu6050_set_accelerometer_power_on_delay (uint8_t delay);
uint8_t mpu6050_get_freefall_detection_counter_decrement ();
void mpu6050_set_freefall_detection_counter_decrement (uint8_t decrement);
uint8_t mpu6050_get_motion_detection_counter_decrement ();
void mpu6050_set_motion_detection_counter_decrement (uint8_t decrement);

// USER_CTRL Register:
bool mpu6050_get_fifo_enabled ();
void mpu6050_set_fifo_enabled (bool enabled);
bool mpu6050_get_i2c_master_mode_enabled ();
void mpu6050_set_i2c_master_mode_enabled (bool enabled);
void mpu6050_switch_spie_enabled (bool enabled);
void mpu6050_reset_fifo ();
void mpu6050_reset_sensors ();

// PWR_MGMT_1 Register:
void mpu6050_reset ();
bool mpu6050_get_sleep_enabled ();
void mpu6050_set_sleep_enabled (bool enabled);
bool mpu6050_get_wake_cycle_enabled ();
void mpu6050_set_wake_cycle_enabled (bool enabled);
bool mpu6050_get_temp_sensor_enabled ();
void mpu6050_set_temp_sensor_enabled (bool enabled);
uint8_t mpu6050_get_clock_source ();
void mpu6050_set_clock_source (uint8_t source);

// PWR_MGMT_2 Register:
uint8_t mpu6050_get_wake_frequency ();
void mpu6050_set_wake_frequency (uint8_t frequency);
bool mpu6050_get_standby_x_accel_enabled ();
void mpu6050_set_standby_x_accel_enabled (bool enabled);
bool mpu6050_get_standby_y_accel_enabled ();
void mpu6050_set_standby_y_accel_enabled (bool enabled);
bool mpu6050_get_standby_z_accel_enabled ();
void mpu6050_set_standby_z_accel_enabled (bool enabled);
bool mpu6050_get_standby_x_gyro_enabled ();
void mpu6050_set_standby_x_gyro_enabled (bool enabled);
bool mpu6050_get_standby_y_gyro_enabled ();
void mpu6050_set_standby_y_gyro_enabled (bool enabled);
bool mpu6050_get_standby_z_gyro_enabled ();
void mpu6050_set_standby_z_gyro_enabled (bool enabled);

// FIFO_COUNT_* Register:
uint16_t mpu6050_get_fifo_count ();

// FIFO_R_W Register:
uint8_t mpu6050_get_fifo_byte ();
void mpu6050_get_fifo_bytes (uint8_t *data, uint8_t length);
void mpu6050_set_fifo_byte (uint8_t data);

// WHO_AM_I Register:
uint8_t mpu6050_get_device_id ();
void mpu6050_set_device_id (uint8_t id);

// Undocumented/DMP Registers/Functions:
uint8_t mpu6050_get_otp_bank_valid ();
void mpu6050_set_otp_bank_valid (int8_t enabled);
int8_t mpu6050_get_x_gyro_offset_tc ();
void mpu6050_set_x_gyro_offset_tc (int8_t offset);
int8_t mpu6050_get_y_gyro_offset_tc ();
void mpu6050_set_y_gyro_offset_tc (int8_t offset);
int8_t mpu6050_get_z_gyro_offset_tc ();
void mpu6050_set_z_gyro_offset_tc (int8_t offset);
int8_t mpu6050_get_x_fine_gain ();
void mpu6050_set_x_fine_gain (int8_t gain);
int8_t mpu6050_get_y_fine_gain ();
void mpu6050_set_y_fine_gain (int8_t gain);
int8_t mpu6050_get_z_fine_gain ();
void mpu6050_set_z_fine_gain (int8_t gain);
int16_t mpu6050_get_x_accel_offset ();
void mpu6050_set_x_accel_offset (int16_t offset);
int16_t mpu6050_get_y_accel_offset ();
void mpu6050_set_y_accel_offset (int16_t offset);
int16_t mpu6050_get_z_accel_offset ();
void mpu6050_set_z_accel_offset (int16_t offset);
int16_t mpu6050_get_x_gyro_offset ();
void mpu6050_set_x_gyro_offset (int16_t offset);
int16_t mpu6050_get_y_gyro_offset ();
void mpu6050_set_y_gyro_offset (int16_t offset);
int16_t mpu6050_get_z_gyro_offset ();
void mpu6050_set_z_gyro_offset (int16_t offset);
bool mpu6050_get_int_pll_ready_enabled ();
void mpu6050_set_int_pll_ready_enabled (bool enabled);
bool mpu6050_get_int_dmp_enabled ();
void mpu6050_set_int_dmp_enabled (bool enabled);
bool mpu6050_get_dmp_int_5_status ();
bool mpu6050_get_dmp_int_4_status ();
bool mpu6050_get_dmp_int_3_status ();
bool mpu6050_get_dmp_int_2_status ();
bool mpu6050_get_dmp_int_1_status ();
bool mpu6050_get_dmp_int_0_status ();
bool mpu6050_get_int_ppl_ready_status ();
bool mpu6050_get_int_dmp_status ();
bool mpu6050_get_dmp_enabled ();
void mpu6050_set_dmp_enabled (bool enabled);
void mpu6050_reset_dmp ();
uint8_t mpu6050_get_dmp_config_1 ();
void mpu6050_set_dmp_config_1 (uint8_t config);
uint8_t mpu6050_get_dmp_config_2 ();
void mpu6050_set_dmp_config_2 (uint8_t config);
#endif
