#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "CanController.h"
#include "sensor_cache.h"
// #include "GattServer.h"
#include "Log.h"
#include "pcf8574.h"

/* --------------------- Definitions and static variables ------------------ */
#define RX_TASK_PRIO 10
#define TX_TASK_PRIO 9
#define CTRL_TSK_PRIO 10

#define TX_GPIO_NUM 32
#define RX_GPIO_NUM 23

#define CAN_DATA_PAYLOAD_SIZE 4
#define DATA_FRAME_SIZE 19
#define DATA_BUFF_SIZE 40

#define TAG "CanController"

#define SELF_TEST_MODE false

#define L7_CAN_REQ_ID 0x7DF
#define L7_CAN_RESP_ID 0x7E8
#define OBD2_RESP_ID 0x40

#define REMOVE_ITEM_INTERVAL 0xFFFF

#define MIN_SEND_INTERVAL 2000 // ms
#define MIN_READ_INTERVAL (MIN_SEND_INTERVAL - 50)

#define CAN_TRANCIEVER_ENABLE_GPIO GPIO_NUM_4 // FIXME: Set correct GPIO value


static bool canConnected =0;

typedef struct CanData {
    uint8_t service;
    uint8_t pid;
    uint8_t data[CAN_DATA_PAYLOAD_SIZE];
} CanData_t;

typedef struct CanTxItem {
    uint8_t service;
    uint8_t pid;
    uint8_t intervalMsb;
    uint8_t intervalLsb;
    uint8_t data[CAN_DATA_PAYLOAD_SIZE];
} CanTxItem_t;

#ifdef CONFIG_L7_DRIVE_MODE_ENERGY
//--> energy shadow parameters
int operatingMode =1;  // 1-4
jsonStruct_t operatingModeJson = {"operatingMode", &operatingMode, sizeof(int), SHADOW_JSON_INT32, iot_operatingMode_callback_handler};
struct jsonStruct *poperatingModeJson = &operatingModeJson ;

int minChargeLevel =1;  // %
jsonStruct_t minChargeLevelJson = {"minChargeLevel", &minChargeLevel, sizeof(int), SHADOW_JSON_INT32, iot_minChargeLevel_callback_handler};
struct jsonStruct *pminChargeLevelJson = &minChargeLevelJson ;

int maxChargeLevel =80;  // %
jsonStruct_t maxChargeLevelJson = {"maxChargeLevel", &maxChargeLevel, sizeof(int), SHADOW_JSON_INT32, iot_maxChargeLevel_callback_handler};
struct jsonStruct *pmaxChargeLevelJson = &maxChargeLevelJson ;

int shutdownLevel =10;  // %
jsonStruct_t shutdownLevelJson = {"shutdownLevel", &shutdownLevel, sizeof(int), SHADOW_JSON_INT32, iot_shutdownLevel_callback_handler};
struct jsonStruct *pshutdownLevelJson = &shutdownLevelJson ;

int stopOptimizeLevel =15;  // %
jsonStruct_t stopOptimizeLevelJson = {"stopOptimizeLevel", &stopOptimizeLevel, sizeof(int), SHADOW_JSON_INT32, iot_stopOptimizeLevel_callback_handler};
struct jsonStruct *pstopOptimizeLevelJson = &stopOptimizeLevelJson ;

int optimizedModeDuration =3600;  // sec, 2bytes
jsonStruct_t optimizedModeDurationJson = {"optimizedModeDuration", &optimizedModeDuration, sizeof(int), SHADOW_JSON_INT32, iot_optimizedModeDuration_callback_handler};
struct jsonStruct *poptimizedModeDurationJson = &optimizedModeDurationJson ;

int optimizedPower =500;  // W, 2bytes, 16bit uint8
jsonStruct_t optimizedPowerJson = {"optimizedPower", &optimizedPower, sizeof(int), SHADOW_JSON_INT32, iot_optimizedPower_callback_handler};
struct jsonStruct *poptimizedPowerJson = &optimizedPowerJson ;

int storageCycleTest =0;  // 0 off, 1 on
jsonStruct_t storageCycleTestJson = {"storageCycleTest", &storageCycleTest, sizeof(int), SHADOW_JSON_INT32, iot_storageCycleTest_callback_handler};
struct jsonStruct *pstorageCycleTestJson = &storageCycleTestJson ;

int batteryCycleTest =0;  // 0 test off, 1 on
jsonStruct_t batteryCycleTestJson = {"batteryCycleTest", &batteryCycleTest, sizeof(int), SHADOW_JSON_INT32, iot_batteryCycleTest_callback_handler};
struct jsonStruct *pbatteryCycleTestJson = &batteryCycleTestJson ;
//<--energy
#endif

static const can_timing_config_t t_config = CAN_TIMING_CONFIG_250KBITS();
static const can_filter_config_t f_config = CAN_FILTER_CONFIG_ACCEPT_ALL();
#if SELF_TEST_MODE
static const can_general_config_t g_config = CAN_GENERAL_CONFIG_DEFAULT(TX_GPIO_NUM, RX_GPIO_NUM, CAN_MODE_NO_ACK);
#else
static const can_general_config_t g_config = CAN_GENERAL_CONFIG_DEFAULT(TX_GPIO_NUM, RX_GPIO_NUM, CAN_MODE_NORMAL);
#endif

static CanOperation_t *canOpList = NULL;
static SemaphoreHandle_t canOpListSemaphore;
static SemaphoreHandle_t canOpRemoveSemaphore;
static SemaphoreHandle_t canTxSemaphore;
static SemaphoreHandle_t awsDataBuffSemaphore;
static SemaphoreHandle_t dataBuffSemaphore;
static CanData_t dataBuff[DATA_BUFF_SIZE];
static CanData_t awsDataBuff[SHADOW_ITEM_LIST_MAX_SIZE];

static uint32_t dataBuffInd = 0;
static bool sendCanRxData = false;

static uint32_t awsDataBuffIndexCount = 0;

void dataBuffAdd(uint8_t service, uint8_t pid, uint8_t *data /*data length is fixed to CAN_DATA_PAYLOAD_SIZE bytes*/);
void shadowBuffAdd(uint8_t service, uint8_t pid, uint8_t *data /*data length is fixed to CAN_DATA_PAYLOAD_SIZE bytes*/);
bool getDataFrame(uint32_t *size, CanData_t *data);

#ifdef CONFIG_L7_CAN_DATA_FAKING
void repeater_process_msg(CanOperation_t *request);
#endif

#ifdef CONFIG_L7_DRIVE_APP_MODE_BT_ONLY
void canHwEnable() {
    gpio_hold_dis(CAN_TRANCIEVER_ENABLE_GPIO);
    gpio_set_direction(CAN_TRANCIEVER_ENABLE_GPIO, GPIO_MODE_INPUT); // Set SN65HVD tranceiver Rs pin high-z for normal mode
}

void canHwDisable() {
    gpio_set_direction(CAN_TRANCIEVER_ENABLE_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_level(CAN_TRANCIEVER_ENABLE_GPIO, 1); // Set SN65HVD tranceiver Rs pin high for sleep mode
    gpio_hold_en(CAN_TRANCIEVER_ENABLE_GPIO);  // Hold pin state during sleep
}
#endif

CanOperation_t *createPeriodicOperation(char service, char pid, uint32_t interval /*ms*/, bool not_used) {
    return createPeriodicOperationWithData(service, pid, interval, not_used, NULL, 0);
}

CanOperation_t *createPeriodicOperationWithData(char service, char pid, uint32_t interval /*ms*/, bool not_used, uint8_t* data, size_t dataLen) {
    UNUSED_PARAM(not_used);
    CanOperation_t *tmpOp = malloc(sizeof(CanOperation_t));
    tmpOp->service = service;
    tmpOp->pid = pid;
    tmpOp->interval = interval; // in ms
    tmpOp->targetTickCnt = xTaskGetTickCount();
    tmpOp->nextItem = NULL;
    // printf("creating periodicoperation with data \n");
    if (interval == 0) {
        LOGI(TAG, "onetimer S: 0x%02X P: 0x%02X", tmpOp->service, tmpOp->pid);
        // printf("  interval == 0 \n");
    } else if (interval >= MIN_READ_INTERVAL || interval == 0) {
        LOGI(TAG, "periodic S: 0x%02X P: 0x%02X interval: %d", tmpOp->service, tmpOp->pid, tmpOp->interval);

    } else {
        tmpOp->interval = MIN_READ_INTERVAL;
        LOGI(TAG, "periodic S: 0x%02X P: 0x%02X interval limited: %d -> %d", tmpOp->service, tmpOp->pid, interval, tmpOp->interval);
    }

    if (dataLen == 0) {
        tmpOp->msg = (can_message_t){
            .identifier = L7_CAN_REQ_ID, .data_length_code = 8, .flags = CAN_MSG_FLAG_SS, .data = {3, service, pid, 0, 0, 0, 0, 0}};
    }
    else if (dataLen == 1) {
        tmpOp->msg = (can_message_t){
            .identifier = L7_CAN_REQ_ID, .data_length_code = 8, .flags = CAN_MSG_FLAG_SS, .data = {7, service, pid, *data, 0,0,0,0}};
    }
    else if (dataLen == 4) {
        tmpOp->msg = (can_message_t){
            .identifier = L7_CAN_REQ_ID, .data_length_code = 8, .flags = CAN_MSG_FLAG_SS, .data = {7, service, pid, data[0], data[1], data[2], data[3], 0}};
    }

#if SELF_TEST_MODE
    tmpOp->msg.data[1] += OBD2_RESP_ID;
    for (int i = 0; i < 4; i++) {
        tmpOp->msg.data[3 + i] = i;
    }
    tmpOp->msg.flags = CAN_MSG_FLAG_SELF;
#endif

    return tmpOp;
}

CanOperation_t *createOneTimeOperation(char service, char pid) {
    return createPeriodicOperation(service, pid, 0, false);
}

CanOperation_t *createOneTimeOperationWithData(char service, char pid, uint8_t* data, size_t dataLen) {
    return createPeriodicOperationWithData(service, pid, 0, false, data, dataLen);
}

bool addCanOperation(CanOperation_t *op) {

    xSemaphoreTake(canOpListSemaphore, portMAX_DELAY);

    if (canOpList == NULL) {
        canOpList = op;
    } else {
        CanOperation_t *iterator = canOpList;

        if (iterator->targetTickCnt >= op->targetTickCnt) {
            // Add new operation as first op
            op->nextItem = iterator;
            canOpList = op;

        } else {
            while (iterator != NULL) {
                if (iterator->nextItem == NULL) {
                    // Add new operation as last
                    iterator->nextItem = op;
                    break;
                }

                if (iterator->nextItem->targetTickCnt > op->targetTickCnt) {
                    // Add new operation to the middle of list
                    op->nextItem = iterator->nextItem;
                    iterator->nextItem = op;
                    break;
                }

                iterator = iterator->nextItem;
            }
        }
    }
    xSemaphoreGive(canOpListSemaphore);

    xSemaphoreGive(canTxSemaphore); // kick Tx task to calculate new sleeping time

    return true;
}

void removeCanOperation(char service, char pid) {
    xSemaphoreTake(canOpRemoveSemaphore, portMAX_DELAY);

    if (canOpList != NULL) {
        CanOperation_t *iterator = canOpList;

        while (iterator->service == service && iterator->pid == pid) {
            // item(s) to be removed is first in list
            CanOperation_t *tmpOp = iterator;
            canOpList = iterator->nextItem;
            free(tmpOp);
            LOGI(TAG, "first op S: 0x%02X P: 0x%02X removed", service, pid);
        }

        while (iterator != NULL && iterator->nextItem != NULL) {
            if (iterator->nextItem->service == service && iterator->nextItem->pid == pid) {
                // remove item
                CanOperation_t *tmpOp = iterator->nextItem;
                iterator->nextItem = iterator->nextItem->nextItem;
                free(tmpOp);
                LOGI(TAG, "S: 0x%02X P: 0x%02X removed", service, pid);
            }
            iterator = iterator->nextItem;
        }
    }
    xSemaphoreGive(canOpRemoveSemaphore);

    xSemaphoreGive(canTxSemaphore); // kick Tx task to calculate new sleeping time
}

void removeAllCanOperation() {
    xSemaphoreTake(canOpRemoveSemaphore, portMAX_DELAY);

    if (canOpList != NULL) {
        CanOperation_t *iterator = canOpList;

        while (iterator != NULL) {
            CanOperation_t *tmpOp = iterator;
            canOpList = iterator->nextItem;
            free(tmpOp);
            iterator = iterator->nextItem;
        }
    }
    xSemaphoreGive(canOpRemoveSemaphore);
    xSemaphoreGive(canTxSemaphore); // kick Tx task to calculate new sleeping time
}

CanOperation_t *popOperation() {

    CanOperation_t *retVal = NULL;
    xSemaphoreTake(canOpListSemaphore, portMAX_DELAY);

    if (canOpList != NULL) {
        retVal = canOpList;
        canOpList = retVal->nextItem;
    }

    xSemaphoreGive(canOpListSemaphore);

    return retVal;
}

uint32_t getOpListSleepTime() {

    uint32_t retVal = 0;

    xSemaphoreTake(canOpListSemaphore, portMAX_DELAY);
    if (!sendCanRxData || canOpList == NULL) {
        retVal = pdMS_TO_TICKS(20000);
    } else {
        uint32_t currTime = xTaskGetTickCount();

        if (currTime < canOpList->targetTickCnt) {
            retVal = canOpList->targetTickCnt - currTime;
            if (retVal > 0x7FFFFFFF) {
                // target exec time is already passed
                retVal = 0;
            }

        } else {
            retVal = currTime - canOpList->targetTickCnt;
            if (retVal > 0x7FFFFFFF) {
                // timer has wrapped, calculate correct sleep time
                retVal = 0xFFFFFFFF - retVal;
            } else {
                // target exec time is already passed
                retVal = 0;
            }
        }
    }

    xSemaphoreGive(canOpListSemaphore);

    return retVal;
}

void printCanOperationList() {
    if (canOpList == NULL) {
        LOGD(TAG, "canOpList is empty");
    } else {
        CanOperation_t *tmp = canOpList;
        uint ind = 0;
        while (tmp != NULL) {
            LOGD(TAG, "canOpList[%d]: ID: 0x%03X, Service: 0x%02X, PID: 0x%04X, interval %u, targetTick: %u", ind, tmp->msg.identifier,
                 tmp->service, tmp->pid, tmp->interval, tmp->targetTickCnt);
            CanOperation_t *test = tmp->nextItem;
            tmp = test;
            ind++;
        }
    }
}

void canSendRxData(bool en) {
    if (sendCanRxData != en) {
        sendCanRxData = en;
        if (en) {
            xSemaphoreGive(canTxSemaphore);
        }
    }
}

void canTxTask(void *arg) {

#if CONFIG_L7_CAN_DATA_FAKING
    LOGI(TAG, "Start CAN Tx task - faking mode");
#elif !SELF_TEST_MODE
    LOGI(TAG, "Start CAN Tx task - normal mode");
#else
    LOGI(TAG, "Start CAN Tx task - self test mode");
#endif

    while (1) {
        uint32_t sleepTime = getOpListSleepTime();

        if (sleepTime <= pdMS_TO_TICKS(20)) {
            xSemaphoreTake(canOpRemoveSemaphore, portMAX_DELAY);

            // perform operation
            CanOperation_t *tmpOp = popOperation(); // grab current operation
            // LOGI(TAG, "TX ID: 0x%03X, S: 0x%02X, P: 0x%02X", tmpOp->msg.identifier, tmpOp->service, tmpOp->pid);
#if CONFIG_L7_CAN_DATA_FAKING
            repeater_process_msg(tmpOp);
#else
            ESP_ERROR_CHECK(can_transmit(&tmpOp->msg, portMAX_DELAY));
#endif

            // LOGI(TAG, "TX done");
            vTaskDelay(250 / portTICK_RATE_MS);

            if (tmpOp->interval != 0) {
                tmpOp->targetTickCnt = xTaskGetTickCount() + pdMS_TO_TICKS(tmpOp->interval);
                tmpOp->nextItem = NULL;
                addCanOperation(tmpOp);
                xSemaphoreTake(canTxSemaphore, 0);
            } else {
                free(tmpOp);
            }

            xSemaphoreGive(canOpRemoveSemaphore);

        } else {
            xSemaphoreTake(canTxSemaphore, sleepTime);
        }
    }

    vTaskDelete(NULL);
}

inline bool can_item_matches(char service, char pid, uint32_t interval, bool includeInShadow, char receivedService, char receivedPid) {
    UNUSED_PARAM(interval);
    UNUSED_PARAM(includeInShadow);
    return service == receivedService && pid == receivedPid;
}

void canRxTask(void *arg) {
    LOGI(TAG, "Start CAN Rx task");
    can_message_t rxMsg;

#ifdef CONFIG_L7_DRIVE_APP_MODE_CELLULAR

#ifdef VEHICLE_SPEED
    QueueHandle_t xQueue_vehicleSpeed = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_vehicleSpeed, sizeof(sensor_data_item), "vehicleSpeed");
#endif

#ifdef MOTOR_POWER2
    QueueHandle_t xQueue_motorPower2 = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_motorPower2, sizeof(sensor_data_item), "motorPower2");
#endif

#ifdef REMAINING_DIST
    QueueHandle_t xQueue_remainingDist = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_remainingDist, sizeof(sensor_data_item), "remainingDist");
#endif

#ifdef ASSIST_LEVEL
    QueueHandle_t xQueue_assistLevel = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_assistLevel, sizeof(sensor_data_item), "assistLevel");
#endif

#ifdef BATTERY_CAPACITY
    QueueHandle_t xQueue_batteryCapacity = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_batteryCapacity, sizeof(sensor_data_item), "batteryCapacity");
#endif
#ifdef CELL_TEMP1
    QueueHandle_t xQueue_cellTemp1 = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_cellTemp1, sizeof(sensor_data_item), "cellTemp1");
#endif
#ifdef CELL_TEMP2
    QueueHandle_t xQueue_cellTemp2 = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_cellTemp2, sizeof(sensor_data_item), "cellTemp2");
#endif
#ifdef MOTOR_TEMP
    QueueHandle_t xQueue_motorTemp = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_motorTemp, sizeof(sensor_data_item), "motorTemp");
#endif
#ifdef BLDC_TEMP
    QueueHandle_t xQueue_bldcTemp = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_bldcTemp, sizeof(sensor_data_item), "bldcTemp");
#endif
#ifdef BATTERY_CURRENT
    QueueHandle_t xQueue_batteryCurrent = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_batteryCurrent, sizeof(sensor_data_item), "battCurrent");
#endif
#ifdef BATTERY_VOLTAGE
    QueueHandle_t xQueue_batteryVoltage = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_batteryVoltage, sizeof(sensor_data_item), "battVoltage");
#endif
#ifdef CHARGING_CURRENT
    QueueHandle_t xQueue_chargingCurrent = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register(xQueue_chargingCurrent, sizeof(sensor_data_item), "chrgCurrent");
#endif
    sensor_data_item dataitem;

#endif // CONFIG_L7_DRIVE_APP_MODE_CELLULAR


#ifdef CONFIG_L7_DRIVE_MODE_ENERGY

    sensor_data_item dataitem;

#endif

    while (1) {
        can_receive(&rxMsg, portMAX_DELAY);
        uint8_t service = (rxMsg.data[1] - OBD2_RESP_ID);
        uint8_t pid = rxMsg.data[2];
        // printf("Received can data for Service: 0x%02x Pid: 0x%02x \n",service, pid );
        dataBuffAdd(service, pid, &rxMsg.data[3]);

#ifdef CONFIG_L7_DRIVE_APP_MODE_CELLULAR

        shadowBuffAdd(service, pid, &rxMsg.data[3]);
        time(&dataitem.timestamp);
        if (dataitem.timestamp < TIMESTAMP_EPOCH_CUTOFF) {
            // We don't want sensor samples with invalid timestamp in our sensor cache -> discard items older than 'June 3rd, 2019 00:00:00'
            continue;
        }
        dataitem.data = parseCanItem(service, pid, &rxMsg.data[3]);

#ifdef VEHICLE_SPEED
        if (can_item_matches(VEHICLE_SPEED, service, pid)) {
            check_session(&dataitem);
            xQueueSendToBack(xQueue_vehicleSpeed, &dataitem, 10);
        }
#endif
#ifdef MOTOR_POWER2
        if (can_item_matches(MOTOR_POWER2, service, pid)) {
            xQueueSendToBack(xQueue_motorPower2, &dataitem, 10);
        }
#endif
#ifdef REMAINING_DIST
        if (can_item_matches(REMAINING_DIST, service, pid)) {
            xQueueSendToBack(xQueue_remainingDist, &dataitem, 10);
        }
#endif
#ifdef ASSIST_LEVEL
        if (can_item_matches(ASSIST_LEVEL, service, pid)) {
            xQueueSendToBack(xQueue_assistLevel, &dataitem, 10);
        }
#endif
#ifdef BATTERY_CAPACITY
        if (can_item_matches(BATTERY_CAPACITY, service, pid)) {
            xQueueSendToBack(xQueue_batteryCapacity, &dataitem, 10);
        }
#endif
#ifdef CELL_TEMP1
        if (can_item_matches(CELL_TEMP1, service, pid)) {
            xQueueSendToBack(xQueue_cellTemp1, &dataitem, 10);
        }
#endif
#ifdef CELL_TEMP2
        if (can_item_matches(CELL_TEMP2, service, pid)) {
            xQueueSendToBack(xQueue_cellTemp2, &dataitem, 10);
        }
#endif
#ifdef MOTOR_TEMP
        if (can_item_matches(MOTOR_TEMP, service, pid)) {
            xQueueSendToBack(xQueue_motorTemp, &dataitem, 10);
        }
#endif
#ifdef BLDC_TEMP
        if (can_item_matches(BLDC_TEMP, service, pid)) {
            xQueueSendToBack(xQueue_bldcTemp, &dataitem, 10);
        }
#endif
#ifdef BATTERY_CURRENT
        if (can_item_matches(BATTERY_CURRENT, service, pid)) {
            xQueueSendToBack(xQueue_batteryCurrent, &dataitem, 10);
        }
#endif
#ifdef BATTERY_VOLTAGE
        if (can_item_matches(BATTERY_VOLTAGE, service, pid)) {
            xQueueSendToBack(xQueue_batteryVoltage, &dataitem, 10);
        }
#endif
#ifdef CHARGING_CURRENT
        if (can_item_matches(CHARGING_CURRENT, service, pid)) {
            xQueueSendToBack(xQueue_chargingCurrent, &dataitem, 10);
        }
#endif
#endif // CONFIG_L7_DRIVE_APP_MODE_CELLULAR


#ifdef CONFIG_L7_DRIVE_MODE_ENERGY

        //shadowBuffAdd(service, pid, &rxMsg.data[3]); aws:ään lisättävät itemit
        time(&dataitem.timestamp);
        if (dataitem.timestamp < TIMESTAMP_EPOCH_CUTOFF) {
            // We don't want sensor samples with invalid timestamp in our sensor cache -> discard items older than 'June 3rd, 2019 00:00:00'
            continue;
        }
        dataitem.data = parseCanItem(service, pid, &rxMsg.data[3]);

        if (can_item_matches(CAN_CHECK_ENERGY, service, pid)) {
            canConnected=1;
            printf("CAN connected \n");

        }

#ifdef BATTERY_LEVEL
        if (can_item_matches(BATTERY_LEVEL, service, pid)) {
            if (xQueue_luxBatteryLevel == NULL) {
                xQueue_luxBatteryLevel = register_can_item(xQueue_luxBatteryLevel,"batteryLevel", dataitem );
            }
            xQueueSendToBack(xQueue_luxBatteryLevel, &dataitem, 10);

        }
#endif
#ifdef REMAINING_BATTERY_CAPACITY
        if (can_item_matches(REMAINING_BATTERY_CAPACITY, service, pid)) {
            if (xQueue_luxBatteryCapacity == NULL) {
                xQueue_luxBatteryCapacity = register_can_item(xQueue_luxBatteryCapacity,"batteryCapacity", dataitem );
            }
             xQueueSendToBack(xQueue_luxBatteryCapacity, &dataitem, 10);
        }
#endif
#ifdef BATTERY_CURRENT
        if (can_item_matches(BATTERY_CURRENT, service, pid)) {

            if (xQueue_luxBatteryCurrent == NULL) {
                xQueue_luxBatteryCurrent = register_can_item(xQueue_luxBatteryCurrent,"batteryCurrent", dataitem );
            }
            xQueueSendToBack(xQueue_luxBatteryCurrent, &dataitem, 10);
        }
#endif
#ifdef BATTERY_VOLTAGE
        if (can_item_matches(BATTERY_VOLTAGE, service, pid)) {

            if (xQueue_luxBatteryVoltage == NULL) {
                xQueue_luxBatteryVoltage = register_can_item(xQueue_luxBatteryVoltage,"batteryVoltage", dataitem );
            }
            xQueueSendToBack(xQueue_luxBatteryVoltage, &dataitem, 10);
        }
#endif
#ifdef SYSTEM_OUTPUT_VOLTAGE
        if (can_item_matches(SYSTEM_OUTPUT_VOLTAGE, service, pid)) {
            if (xQueue_luxOutputVoltage == NULL) {
                xQueue_luxOutputVoltage = register_can_item(xQueue_luxOutputVoltage,"outputVoltage", dataitem );
            }
            xQueueSendToBack(xQueue_luxOutputVoltage, &dataitem, 10);
        }
#endif
#ifdef BATTERY_TEMPERATURE
        if (can_item_matches(BATTERY_TEMPERATURE, service, pid)) {
            if (xQueue_luxBatteryTemperature == NULL) {
                xQueue_luxBatteryTemperature = register_can_item(xQueue_luxBatteryTemperature,"batteryTemperature", dataitem );
            }
            xQueueSendToBack(xQueue_luxBatteryTemperature, &dataitem, 10);
        }
#endif
#ifdef HEATING_BATTERY
        if (can_item_matches(HEATING_BATTERY, service, pid)) {
            if (xQueue_luxBatteryHeating == NULL) {
                xQueue_luxBatteryHeating = register_can_item(xQueue_luxBatteryHeating,"batteryHeating", dataitem );
            }
            xQueueSendToBack(xQueue_luxBatteryHeating, &dataitem, 10);
        }
#endif
#ifdef HEATING_POWER
        if (can_item_matches(HEATING_POWER, service, pid)) {
            if (xQueue_luxHeatingPower == NULL) {
                xQueue_luxHeatingPower = register_can_item(xQueue_luxHeatingPower,"heatingPower", dataitem );
            }
            xQueueSendToBack(xQueue_luxHeatingPower, &dataitem, 10);
        }
#endif
#ifdef COOLING_BCS
        if (can_item_matches(COOLING_BCS, service, pid)) {
            if (xQueue_luxBcsCooling == NULL) {
                xQueue_luxBcsCooling = register_can_item(xQueue_luxBcsCooling,"bcsCooling", dataitem );
            }
            xQueueSendToBack(xQueue_luxBcsCooling, &dataitem, 10);
        }
#endif
#ifdef COOLING_BCS_POWER
        if (can_item_matches(COOLING_BCS_POWER, service, pid)) {
            if (xQueue_luxBcsCoolingPower == NULL) {
                xQueue_luxBcsCoolingPower = register_can_item(xQueue_luxBcsCoolingPower,"bcsCoolingPower", dataitem );
            }
            xQueueSendToBack(xQueue_luxBcsCoolingPower, &dataitem, 10);
        }
#endif
#ifdef BATTERY_CHARGING_POWER
        if (can_item_matches(BATTERY_CHARGING_POWER, service, pid)) {
            if (xQueue_luxBatteryChargingPower == NULL) {
                xQueue_luxBatteryChargingPower = register_can_item(xQueue_luxBatteryChargingPower,"batteryChargingPower", dataitem );
            }
            xQueueSendToBack(xQueue_luxBatteryChargingPower, &dataitem, 10);
        }
#endif
#ifdef BCS_INPUT_POWER
        if (can_item_matches(BCS_INPUT_POWER, service, pid)) {
            if (xQueue_luxBcsInputPower == NULL) {
                xQueue_luxBcsInputPower = register_can_item(xQueue_luxBcsInputPower,"bcsInputPower", dataitem );
            }
            xQueueSendToBack(xQueue_luxBcsInputPower, &dataitem, 10);
        }
#endif
#ifdef TOTAL_OUTPUT_POWER
        if (can_item_matches(TOTAL_OUTPUT_POWER, service, pid)) {
            if (xQueue_luxTotalOutputPower == NULL) {
                xQueue_luxTotalOutputPower = register_can_item(xQueue_luxTotalOutputPower,"totalOutputPower", dataitem );
            }
            xQueueSendToBack(xQueue_luxTotalOutputPower, &dataitem, 10);
        }
#endif
#ifdef OPERATING_MODE
        if (can_item_matches(OPERATING_MODE, service, pid)) {
            if (xQueue_luxOperatingMode == NULL) {
                xQueue_luxOperatingMode = register_can_item(xQueue_luxOperatingMode,"operatingMode", dataitem );
            }
            xQueueSendToBack(xQueue_luxOperatingMode, &dataitem, 10);
        }
#endif
#ifdef L7DRIVE_BCS_TEMPERATURE
        if (can_item_matches(L7DRIVE_BCS_TEMPERATURE, service, pid)) {
            if (xQueue_luxBcsTemperature == NULL) {
                xQueue_luxBcsTemperature = register_can_item(xQueue_luxBcsTemperature,"bcsTemperature", dataitem );
            }
            xQueueSendToBack(xQueue_luxBcsTemperature, &dataitem, 10);
        }
#endif
#ifdef BATTERYCYCLETEST_STATE
        if (can_item_matches(BATTERYCYCLETEST_STATE, service, pid)) {
            if (xQueue_luxBatteryCycleTest == NULL) {
                xQueue_luxBatteryCycleTest = register_can_item(xQueue_luxBatteryCycleTest,"batteryCycleTest", dataitem );
            }
            xQueueSendToBack(xQueue_luxBatteryCycleTest, &dataitem, 10);
        }
#endif
#ifdef STORAGE_SWITCH_STATE
        if (can_item_matches(STORAGE_SWITCH_STATE, service, pid)) {
            if (xQueue_luxStorageSwitchState == NULL) {
                xQueue_luxStorageSwitchState = register_can_item(xQueue_luxStorageSwitchState,"storageSwitchState", dataitem );
            }
            xQueueSendToBack(xQueue_luxStorageSwitchState, &dataitem, 10);
        }
#endif

#ifdef GET_OPTIMIZED_MODE_POWER
        if (can_item_matches(GET_OPTIMIZED_MODE_POWER, service, pid)) {
            if (xQueue_luxOptimizedPower == NULL) {
                xQueue_luxOptimizedPower = register_can_item(xQueue_luxOptimizedPower,"optimizedPower", dataitem );
            }
            xQueueSendToBack(xQueue_luxOptimizedPower, &dataitem, 10);
        }
#endif
#ifdef GET_OPTIMIZED_MODE_DURATION
        if (can_item_matches(GET_OPTIMIZED_MODE_DURATION, service, pid)) {
            if (xQueue_luxOptimizedModeDuration == NULL) {
                xQueue_luxOptimizedModeDuration = register_can_item(xQueue_luxOptimizedModeDuration,"optimizedModeDuration", dataitem );
            }
            xQueueSendToBack(xQueue_luxOptimizedModeDuration, &dataitem, 10);
        }
#endif
#ifdef GET_MIN_CHARGE_LEVEL
        if (can_item_matches(GET_MIN_CHARGE_LEVEL, service, pid)) {
            if (xQueue_luxMinChargeLevel == NULL) {
                xQueue_luxMinChargeLevel = register_can_item(xQueue_luxMinChargeLevel,"minChargeLevel", dataitem );
            }
            xQueueSendToBack(xQueue_luxMinChargeLevel, &dataitem, 10);
        }
#endif
#ifdef GET_MAX_CHARGE_LEVEL
        if (can_item_matches(GET_MAX_CHARGE_LEVEL, service, pid)) {
            if (xQueue_luxMaxChargeLevel == NULL) {
                xQueue_luxMaxChargeLevel = register_can_item(xQueue_luxMaxChargeLevel,"maxChargeLevel", dataitem );
            }
            xQueueSendToBack(xQueue_luxMaxChargeLevel, &dataitem, 10);
        }
#endif
#ifdef GET_SHUTDOWN_LEVEL
        if (can_item_matches(GET_SHUTDOWN_LEVEL, service, pid)) {
            if (xQueue_luxShutdownLevel == NULL) {
                xQueue_luxShutdownLevel = register_can_item(xQueue_luxShutdownLevel,"shutdownLevel", dataitem );
            }
            xQueueSendToBack(xQueue_luxShutdownLevel, &dataitem, 10);
        }
#endif
#ifdef GET_STOP_OPTIMIZE_LEVEL
        if (can_item_matches(GET_STOP_OPTIMIZE_LEVEL, service, pid)) {
            if (xQueue_luxStopOptimizeLevel == NULL) {
                xQueue_luxStopOptimizeLevel = register_can_item(xQueue_luxStopOptimizeLevel,"stopOptimizeLevel", dataitem );
            }
            xQueueSendToBack(xQueue_luxStopOptimizeLevel, &dataitem, 10);
        }
#endif


#endif // CONFIG_L7_DRIVE_ENERGY
    }

    vTaskDelete(NULL);
}

#if CONFIG_L7_CAN_DATA_FAKING
uint8_t assist_level = 5;

void repeater_process_msg(CanOperation_t *operation) {
    LOGI(TAG, "Fake CAN data .........................................");
    uint8_t data[8];
    uint8_t service = operation->service;
    uint8_t pid = operation->pid;
    if (operation->pid % 20 == 0) {
        data[3] = 0xff;
        data[4] = 0xff;
        data[5] = 0xff;
        data[6] = 0xff;
        data[7] = 0xff;
    } else if (operation->service == 0x32 &&
        (operation->pid == 0x01 || operation->pid == 0x02 || operation->pid == 0x03 ||
         operation->pid == 0x04 || operation->pid == 0x05)) {
        service = 0x30;
        pid = 0x08;
        assist_level = operation->pid;
        data[3] = assist_level;
    } else if (service == 0x30 && pid == 0x08) {
        data[3] = assist_level;
    } else {
        int random_data = rand();
        data[7] = random_data >> 8;
        data[6] = random_data >> 16;
        random_data = rand();
        data[5] = random_data >> 16;
        data[4] = random_data >> 8;
        random_data = rand();
        data[3] = random_data >> 16;
    }
    dataBuffAdd(service, pid, &data[3]);
}
#endif

#ifdef CONFIG_L7_DRIVE_APP_MODE_CELLULAR
void canSendDataTask(void *arg) {
    LOGI(TAG, "Start sendDataTask");

    while (1) {

        vTaskDelay(pdMS_TO_TICKS(MIN_SEND_INTERVAL));

         uint32_t size = bleGetMtu() / sizeof(CanData_t);
        CanData_t *data = malloc(size * sizeof(CanData_t));

        if (getDataFrame(&size, data)) {
             if (size > 0) {
                 for (int i = 0; i < size; i++) {
                    LOGI(TAG, "[%d] S: 0x%02X P: 0x%02X data: [%02X, %02X, %02X, %02X]", i, data[i].service, data[i].pid, data[i].data[0],
                        data[i].data[1], data[i].data[2], data[i].data[3]);
                }

                // bleSendCanRxData(size * sizeof(CanData_t), (uint8_t *)data);
            }

             free(data);
         }
    }

    vTaskDelete(NULL);
}
#endif


void dataBuffAdd(uint8_t service, uint8_t pid, uint8_t *data /*data size is fixed to CAN_DATA_PAYLOAD_SIZE bytes*/) {

    xSemaphoreTake(dataBuffSemaphore, portMAX_DELAY);

    // check if this item is already at the buffer
    bool updated = false;
    for (int i = 0; i < dataBuffInd; i++) {
        if (dataBuff[i].service == service && dataBuff[i].pid == pid) {
            LOGD(TAG, "update S: 0x%02X P: 0x%02X, %d / %d", service, pid, dataBuffInd, DATA_BUFF_SIZE);
            memcpy(dataBuff[i].data, data, CAN_DATA_PAYLOAD_SIZE * sizeof(uint8_t));
            updated = true;
            break;
        }
    }

    if (!updated) {
        if (dataBuffInd < DATA_BUFF_SIZE) {
            dataBuff[dataBuffInd].service = service;
            dataBuff[dataBuffInd].pid = pid;
            memcpy(dataBuff[dataBuffInd].data, data, CAN_DATA_PAYLOAD_SIZE * sizeof(uint8_t));
            dataBuffInd++;
            LOGD(TAG, "add: S: 0x%02X P: 0x%02X, %d / %d", service, pid, dataBuffInd, DATA_BUFF_SIZE);

        } else {
            LOGE(TAG, "dataBuff is full -> abort");
            ESP_ERROR_CHECK(ESP_FAIL);
        }
    }
    xSemaphoreGive(dataBuffSemaphore);
}

void shadowBuffAdd(uint8_t service, uint8_t pid, uint8_t *data) {
    // update data items that go to aws shadow
    xSemaphoreTake(awsDataBuffSemaphore, portMAX_DELAY);
    int idx = getShadowItemIndex(service, pid);
    if (idx != NOT_VALID_AWS_ITEM) {
        // let's update service and pid just in case...
        awsDataBuff[idx].service = service;
        awsDataBuff[idx].pid = pid;
        memcpy(awsDataBuff[idx].data, data, CAN_DATA_PAYLOAD_SIZE * sizeof(uint8_t));
        LOGD(TAG, "add: S: 0x%02X P: 0x%02X, %d / 30 to shadow buffer", service, pid, (idx + 1));
    }
    xSemaphoreGive(awsDataBuffSemaphore);
}

bool getDataFrame(uint32_t *size, CanData_t *data) {

    if (data == NULL) {
        *size = 0;
        return false;
    }

    xSemaphoreTake(dataBuffSemaphore, portMAX_DELAY);

    if (*size > dataBuffInd) {
        *size = dataBuffInd;
    }

    memcpy(data, dataBuff, (*size) * sizeof(CanData_t));

    if (dataBuffInd > *size) {
        dataBuffInd -= *size;
        memcpy(dataBuff, &dataBuff[*size], (dataBuffInd) * sizeof(CanData_t));

    } else {
        dataBuffInd = 0;
    }

    xSemaphoreGive(dataBuffSemaphore);

    return true;
}

void canParseTxMsg(uint32_t size, uint8_t *data) {
    uint8_t structSize = ceil((float)size / (float)sizeof(CanTxItem_t));
    CanTxItem_t *items = (CanTxItem_t *)data;

    for (int i = 0; i < structSize; i++) {
        // ESP seems to be LittleEndian device, convert bytes in order to improve readability
        uint16_t interval = (items[i].intervalMsb << 8) | items[i].intervalLsb;
        if (interval == REMOVE_ITEM_INTERVAL) {
            removeCanOperation(items[i].service, items[i].pid);
        } else {
            addCanOperation(createPeriodicOperationWithData(items[i].service, items[i].pid, interval, false /*not important*/, items[i].data, CAN_DATA_PAYLOAD_SIZE));
        }
    }
}

int getShadowItemIndex(uint8_t service, uint8_t pid) {

    for (int i = 0; i < SHADOW_ITEM_LIST_MAX_SIZE; i++) {
        if (awsDataBuff[i].service == service && awsDataBuff[i].pid == pid) {
            LOGD(TAG, "AWS shadow item, index: %i", i);
            return i;
        }
    }
    return NOT_VALID_AWS_ITEM;
}

void insertToStaticAwsBufferIfConfigured(uint8_t service, uint8_t pid, int interval, bool includeInAwsShadow) {
#ifdef CONFIG_L7_DRIVE_APP_MODE_CELLULAR
    if (awsDataBuffIndexCount < SHADOW_ITEM_LIST_MAX_SIZE) {
        if (includeInAwsShadow) {
            awsDataBuff[awsDataBuffIndexCount].service = service;
            awsDataBuff[awsDataBuffIndexCount].pid = pid;
            memset(awsDataBuff[awsDataBuffIndexCount].data, UNDEFINED_CAN_ITEM, CAN_DATA_PAYLOAD_SIZE);
            awsDataBuffIndexCount++;
        }
    } else {
        LOGE(TAG, "CHECK SHADOW_ITEM_LIST_MAX_SIZE !!!");
    }
#endif
}

void initializeCanItems(void) {
#ifdef CONFIG_L7_DRIVE_APP_MODE_CELLULAR

// vehicle rpm and speed
#ifdef ENGINE_RPM
    addCanOperation(createPeriodicOperation(ENGINE_RPM));
    insertToStaticAwsBufferIfConfigured(ENGINE_RPM);
#endif
#ifdef VEHICLE_SPEED
    addCanOperation(createPeriodicOperation(VEHICLE_SPEED));
    insertToStaticAwsBufferIfConfigured(VEHICLE_SPEED);
#endif
#ifdef THROTTLE_POSITION
    addCanOperation(createPeriodicOperation(THROTTLE_POSITION));
    insertToStaticAwsBufferIfConfigured(THROTTLE_POSITION);
#endif
#ifdef ODO
    addCanOperation(createPeriodicOperation(ODO));
    insertToStaticAwsBufferIfConfigured(ODO);
#endif
#ifdef BATTERY_VOLTAGE
    addCanOperation(createPeriodicOperation(BATTERY_VOLTAGE));
    insertToStaticAwsBufferIfConfigured(BATTERY_VOLTAGE);
#endif
#ifdef THROTTLE_POSITION2
    addCanOperation(createPeriodicOperation(THROTTLE_POSITION2));
    insertToStaticAwsBufferIfConfigured(THROTTLE_POSITION2);
#endif
#ifdef MOTOR_RPM
    addCanOperation(createPeriodicOperation(MOTOR_RPM));
    insertToStaticAwsBufferIfConfigured(MOTOR_RPM);
#endif
#ifdef WHEEL_ROTATION_TIME
    addCanOperation(createPeriodicOperation(WHEEL_ROTATION_TIME));
    insertToStaticAwsBufferIfConfigured(WHEEL_ROTATION_TIME);
#endif
#ifdef WHEEL_SIZE_CODE
    addCanOperation(createPeriodicOperation(WHEEL_SIZE_CODE));
    insertToStaticAwsBufferIfConfigured(WHEEL_SIZE_CODE);
#endif
#ifdef MOTOR_POWER
    addCanOperation(createPeriodicOperation(MOTOR_POWER));
    insertToStaticAwsBufferIfConfigured(MOTOR_POWER);
#endif
#ifdef ASSIST_LEVEL
    addCanOperation(createPeriodicOperation(ASSIST_LEVEL));
    insertToStaticAwsBufferIfConfigured(ASSIST_LEVEL);
#endif
#ifdef MOTOR_POWER2
    addCanOperation(createPeriodicOperation(MOTOR_POWER2));
    insertToStaticAwsBufferIfConfigured(MOTOR_POWER2);
#endif
#ifdef MAX_SPEED
    addCanOperation(createPeriodicOperation(MAX_SPEED));
    insertToStaticAwsBufferIfConfigured(MAX_SPEED);
#endif
#ifdef BATTERY_CURRENT
    addCanOperation(createPeriodicOperation(BATTERY_CURRENT));
    insertToStaticAwsBufferIfConfigured(BATTERY_CURRENT);
#endif
#ifdef CHARGING_CURRENT
    addCanOperation(createPeriodicOperation(CHARGING_CURRENT));
    insertToStaticAwsBufferIfConfigured(CHARGING_CURRENT);
#endif
#ifdef U24_VOLTAGE
    addCanOperation(createPeriodicOperation(U24_VOLTAGE));
    insertToStaticAwsBufferIfConfigured(U24_VOLTAGE);
#endif
#ifdef CELL_TEMP1
    addCanOperation(createPeriodicOperation(CELL_TEMP1));
    insertToStaticAwsBufferIfConfigured(CELL_TEMP1);
#endif
#ifdef CELL_TEMP2
    addCanOperation(createPeriodicOperation(CELL_TEMP2));
    insertToStaticAwsBufferIfConfigured(CELL_TEMP2);
#endif
#ifdef MOTOR_TEMP
    addCanOperation(createPeriodicOperation(MOTOR_TEMP));
    insertToStaticAwsBufferIfConfigured(MOTOR_TEMP);
#endif
#ifdef BLDC_TEMP
    addCanOperation(createPeriodicOperation(BLDC_TEMP));
    insertToStaticAwsBufferIfConfigured(BLDC_TEMP);
#endif
#ifdef MOTOR_STATUS
    addCanOperation(createPeriodicOperation(MOTOR_STATUS));
    insertToStaticAwsBufferIfConfigured(MOTOR_STATUS);
#endif
#ifdef POWER_STATUS
    addCanOperation(createPeriodicOperation(POWER_STATUS));
    insertToStaticAwsBufferIfConfigured(POWER_STATUS);
#endif
#ifdef BLDC_STATUS
    addCanOperation(createPeriodicOperation(BLDC_STATUS));
    insertToStaticAwsBufferIfConfigured(BLDC_STATUS);
#endif
#ifdef DRIVE_FW_VERSION
    addCanOperation(createPeriodicOperation(DRIVE_FW_VERSION));
    insertToStaticAwsBufferIfConfigured(DRIVE_FW_VERSION);
#endif
#ifdef ODO2
    addCanOperation(createPeriodicOperation(ODO2));
    insertToStaticAwsBufferIfConfigured(ODO2);
#endif
#ifdef TRIP
    addCanOperation(createPeriodicOperation(TRIP));
    insertToStaticAwsBufferIfConfigured(TRIP);
#endif
#ifdef CHARGER_STATUS
    addCanOperation(createPeriodicOperation(CHARGER_STATUS));
    insertToStaticAwsBufferIfConfigured(CHARGER_STATUS);
#endif
#ifdef BATTERY_CAPACITY
    addCanOperation(createPeriodicOperation(BATTERY_CAPACITY));
    insertToStaticAwsBufferIfConfigured(BATTERY_CAPACITY);
#endif
#ifdef REMAINING_DIST
    addCanOperation(createPeriodicOperation(REMAINING_DIST));
    insertToStaticAwsBufferIfConfigured(REMAINING_DIST);
#endif
#ifdef REMAINING_CHARGE_TIME
    addCanOperation(createPeriodicOperation(REMAINING_CHARGE_TIME));
    insertToStaticAwsBufferIfConfigured(REMAINING_CHARGE_TIME);
#endif
#ifdef GET_RPM_RATIO
    addCanOperation(createPeriodicOperation(GET_RPM_RATIO));
    insertToStaticAwsBufferIfConfigured(GET_RPM_RATIO);
#endif
#endif

#ifdef CONFIG_L7_DRIVE_MODE_ENERGY
// L7DRIVE ENERGY ITEMS

#ifdef BATTERY_LEVEL
    addCanOperation(createPeriodicOperation(BATTERY_LEVEL));
    insertToStaticAwsBufferIfConfigured(BATTERY_LEVEL);
#endif
#ifdef REMAINING_BATTERY_CAPACITY
    addCanOperation(createPeriodicOperation(REMAINING_BATTERY_CAPACITY));
    insertToStaticAwsBufferIfConfigured(REMAINING_BATTERY_CAPACITY);
#endif
#ifdef BATTERY_CURRENT
    addCanOperation(createPeriodicOperation(BATTERY_CURRENT));
    insertToStaticAwsBufferIfConfigured(BATTERY_CURRENT);
#endif
#ifdef BATTERY_VOLTAGE
    addCanOperation(createPeriodicOperation(BATTERY_VOLTAGE));
    insertToStaticAwsBufferIfConfigured(BATTERY_VOLTAGE);
#endif
#ifdef SYSTEM_OUTPUT_VOLTAGE
    addCanOperation(createPeriodicOperation(SYSTEM_OUTPUT_VOLTAGE));
    insertToStaticAwsBufferIfConfigured(SYSTEM_OUTPUT_VOLTAGE);
#endif
#ifdef BATTERY_TEMPERATURE
    addCanOperation(createPeriodicOperation(BATTERY_TEMPERATURE));
    insertToStaticAwsBufferIfConfigured(BATTERY_TEMPERATURE);
#endif
#ifdef HEATING_BATTERY
    addCanOperation(createPeriodicOperation(HEATING_BATTERY));
    insertToStaticAwsBufferIfConfigured(HEATING_BATTERY);
#endif
#ifdef HEATING_POWER
    addCanOperation(createPeriodicOperation(HEATING_POWER));
    insertToStaticAwsBufferIfConfigured(HEATING_POWER);
#endif
#ifdef COOLING_BCS
    addCanOperation(createPeriodicOperation(COOLING_BCS));
    insertToStaticAwsBufferIfConfigured(COOLING_BCS);
#endif
#ifdef COOLING_BCS_POWER
    addCanOperation(createPeriodicOperation(COOLING_BCS_POWER));
    insertToStaticAwsBufferIfConfigured(COOLING_BCS_POWER);
#endif
#ifdef BATTERY_CHARGING_POWER
    addCanOperation(createPeriodicOperation(BATTERY_CHARGING_POWER));
    insertToStaticAwsBufferIfConfigured(BATTERY_CHARGING_POWER);
#endif
#ifdef BCS_INPUT_POWER
    addCanOperation(createPeriodicOperation(BCS_INPUT_POWER));
    insertToStaticAwsBufferIfConfigured(BCS_INPUT_POWER);
#endif
#ifdef TOTAL_OUTPUT_POWER
    addCanOperation(createPeriodicOperation(TOTAL_OUTPUT_POWER));
    insertToStaticAwsBufferIfConfigured(TOTAL_OUTPUT_POWER);
#endif
#ifdef OPERATING_MODE
    addCanOperation(createPeriodicOperation(OPERATING_MODE));
    insertToStaticAwsBufferIfConfigured(OPERATING_MODE);
#endif
#ifdef L7DRIVE_BCS_TEMPERATURE
    addCanOperation(createPeriodicOperation(L7DRIVE_BCS_TEMPERATURE));
    insertToStaticAwsBufferIfConfigured(L7DRIVE_BCS_TEMPERATURE);
#endif
#ifdef BATTERYCYCLETEST_STATE
    addCanOperation(createPeriodicOperation(BATTERYCYCLETEST_STATE));
    insertToStaticAwsBufferIfConfigured(BATTERYCYCLETEST_STATE);
#endif
#ifdef STORAGE_SWITCH_STATE
    addCanOperation(createPeriodicOperation(STORAGE_SWITCH_STATE));
    insertToStaticAwsBufferIfConfigured(STORAGE_SWITCH_STATE);
#endif
#ifdef GET_OPTIMIZED_MODE_POWER
    addCanOperation(createPeriodicOperation(GET_OPTIMIZED_MODE_POWER));
    insertToStaticAwsBufferIfConfigured(GET_OPTIMIZED_MODE_POWER);
#endif
#ifdef GET_OPTIMIZED_MODE_DURATION
    addCanOperation(createPeriodicOperation(GET_OPTIMIZED_MODE_DURATION));
    insertToStaticAwsBufferIfConfigured(GET_OPTIMIZED_MODE_DURATION);
#endif
#ifdef GET_MIN_CHARGE_LEVEL
    addCanOperation(createPeriodicOperation(GET_MIN_CHARGE_LEVEL));
    insertToStaticAwsBufferIfConfigured(GET_MIN_CHARGE_LEVEL);
#endif
#ifdef GET_MAX_CHARGE_LEVEL
    addCanOperation(createPeriodicOperation(GET_MAX_CHARGE_LEVEL));
    insertToStaticAwsBufferIfConfigured(GET_MAX_CHARGE_LEVEL);
#endif
#ifdef GET_SHUTDOWN_LEVEL
    addCanOperation(createPeriodicOperation(GET_SHUTDOWN_LEVEL));
    insertToStaticAwsBufferIfConfigured(GET_SHUTDOWN_LEVEL);
#endif
#ifdef GET_STOP_OPTIMIZE_LEVEL
    addCanOperation(createPeriodicOperation(GET_STOP_OPTIMIZE_LEVEL));
    insertToStaticAwsBufferIfConfigured(GET_STOP_OPTIMIZE_LEVEL);
#endif
#endif
}
float parseCanItem(uint8_t service, uint8_t pid, uint8_t* data) {
    float parsedValue = UNDEFINED_CAN_ITEM;
    if (data != 0) {
        if (service == 0x01) {
#ifdef ENGINE_RPM
            // 2-bytes, 256*A+B/4
            if (pid == 0x0C) {
                parsedValue = ((data[0] << 8) | data[1]) >> 2;
            }
#endif
#ifdef VEHICLE_SPEED
            // 1-byte, km/h
            if (pid == 0x0D) {
                parsedValue = data[0];
            }
#endif
#ifdef THROTTLE_POSITION
            // 1-byte, A*100/255
            if (pid == 0x11) {
                parsedValue = ((int)data[0] * 100) / 255;
            }
#endif
#ifdef ODO
            // 4-bytes, 32-bit unsigned integer, MSB first
            if (pid == 0xA6) {
                // Assuming that also ODO is MSB first...
                parsedValue = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
            }
#endif
        } else if (service == 0x30) {
#ifdef BATTERY_VOLTAGE
            // 2-bytes, 256*A+B)/100
            // V, 325 = 3.25V
            if (pid == 0x02) {
                parsedValue = ((int)(data[0] << 8) | data[1]) / 100;
            }
#endif
#ifdef THROTTLE_POSITION2
            // 1-byte, %
            if (pid == 0x03) {
                parsedValue = data[0];
            }
#endif
#ifdef MOTOR_RPM
            // 2-bytes, (256*A)+B, rpm
            if (pid == 0x04) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif
#ifdef WHEEL_ROTATION_TIME
            // 2-bytes, (256*A)+B, ms
            if (pid == 0x05) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif
#ifdef WHEEL_SIZE_CODE
            // 1-byte
            /*Table A Wheel Size Codes
            Size Code	Size Inch
                    0	16
                    2	12
                    6	14
                    8	20
                    10	8
                    12	23
                    14	10
                    16	24
                    18	6
                    20	26
                    22	5
                    24	28
                    28	28
                    30	29
                    */
            if (pid == 0x06) {
                parsedValue = data[0];
            }
#endif
#ifdef MOTOR_POWER
            //'-8kW…8kW (256 * A + B) - 8000
            if (pid == 0x07) {
                parsedValue = (int)((data[0] << 8) | data[1]) - 8000;
            }
#endif
#ifdef ASSIST_LEVEL
            // 1..5
            if (pid == 0x08) {
                parsedValue = data[0];
            }
#endif
#ifdef MOTOR_POWER2
            //-100%...100%, A - 100
            if (pid == 0x09) {
                parsedValue = (int)data[0] - 100;
            }
#endif
#ifdef MAX_SPEED
            // 0..78
            if (pid == 0x0A) {
                parsedValue = data[0];
            }
#endif
        } else if (service == 0x31) {
#ifdef BATTERY_CURRENT
            // Amps (256 * A + B)
            if (pid == 0x01) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif
#ifdef CHARGING_CURRENT
            // Amps (256 * A + B) / 10 -> 57 = 5.7A
            if (pid == 0x02) {
                parsedValue = (int)((data[0] << 8) | data[1]) / 10;
            }
#endif
#ifdef U24_VOLTAGE
            // Amps (256 * A + B) / 10 -> 57 = 5.7A
            if (pid == 0x03) {
                parsedValue = (int)((data[0] << 8) | data[1]) / 10;
            }
#endif
#ifdef CELL_TEMP1
            // Celsius
            if (pid == 0x04) {
                parsedValue = data[0];
            }
#endif
#ifdef CELL_TEMP2
            // Celsius
            if (pid == 0x05) {
                parsedValue = data[0];
            }
#endif
#ifdef MOTOR_TEMP
            // Celsius
            if (pid == 0x06) {
                parsedValue = data[0];
            }
#endif
#ifdef BLDC_TEMP
            // Celsius
            if (pid == 0x07) {
                parsedValue = data[0];
            }
#endif
#ifdef MOTOR_STATUS
            if (pid == 0x08) {
                parsedValue = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
            }
#endif
#ifdef POWER_STATUS
            if (pid == 0x09) {
                parsedValue = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
            }
#endif
#ifdef BLDC_STATUS
            if (pid == 0x0A) {
                parsedValue = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
            }
#endif
#ifdef DRIVE_FW_VERSION
            // (256 * A + B) / 100 -> 125 = 1.25
            if (pid == 0x0B) {
                parsedValue = (int)((data[0] << 8) | data[1]); // Since integer is used, sending 125 instead of 1.25
            }
#endif
#ifdef ODO2
            // x100m
            if (pid == 0x0C) {
                parsedValue = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
            }
#endif
#ifdef TRIP
            // x100m
            if (pid == 0x0D) {
                parsedValue = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
            }
#endif
#ifdef CHARGER_STATUS
            // Bit Mask
            if (pid == 0x0E) {
                parsedValue = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
            }
#endif
#ifdef BATTERY_CAPACITY
            // %
            if (pid == 0x0F) {
                parsedValue = data[0];
            }
#endif
#ifdef REMAINING_DIST
            // km
            if (pid == 0x10) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif
#ifdef REMAINING_CHARGE_TIME
            // minutes
            if (pid == 0x11) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif
        } else if (service == 0x33) {
#ifdef GET_RPM_RATIO
            if (pid == 0x01) {
                // 0.01 - 10.0 , IEEE754 (we leave it as integer value here)
                parsedValue = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
            }
#endif
        } else if (service == 0x60) {
#ifdef BATTERY_LEVEL
             if (pid == 0x01) {
                parsedValue = data[0];
            }
#endif
#ifdef REMAINING_BATTERY_CAPACITY
             if (pid == 0x02) {
                parsedValue = (data[0] << 8) | data[1];
             }
#endif

#ifdef BATTERY_CURRENT
             if (pid == 0x03) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif

#ifdef BATTERY_VOLTAGE
             if (pid == 0x04) {
                parsedValue = ((float)((data[0] << 8) | data[1]) / 100.0);
            }
#endif

#ifdef SYSTEM_OUTPUT_VOLTAGE
             if (pid == 0x05) {
                parsedValue =  roundf((float)((data[0] << 8) | data[1]))/100;
            }
#endif

#ifdef BATTERY_TEMPERATURE
             if (pid == 0x06) {
                parsedValue = (int8_t)data[0];
            }
#endif

#ifdef HEATING_BATTERY
             if (pid == 0x07) {
                parsedValue = data[0];
            }
#endif

#ifdef HEATING_POWER
             if (pid == 0x08) {
                parsedValue = data[0];
            }
#endif

#ifdef COOLING_BCS
             if (pid == 0x09) {
                parsedValue = data[0];
            }
#endif

#ifdef COOLING_BCS_POWER
             if (pid == 0x0A) {
                parsedValue = data[0];
            }
#endif

#ifdef BATTERY_CHARGING_POWER
             if (pid == 0x0B) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif

#ifdef BCS_INPUT_POWER
             if (pid == 0x0C) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif

#ifdef TOTAL_OUTPUT_POWER
             if (pid == 0x0D) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif

#ifdef OPERATING_MODE
             if (pid == 0x0E) {
                parsedValue = data[0];
            }
#endif

#ifdef L7DRIVE_BCS_TEMPERATURE
             if (pid == 0x0F) {
                parsedValue = (int8_t)data[0];
            }
#endif

#ifdef BATTERYCYCLETEST_STATE
             if (pid == 0x10) {
                parsedValue = data[0];
            }
#endif

#ifdef STORAGE_SWITCH_STATE
             if (pid == 0x11) {
                parsedValue = data[0];
            }
#endif

    } else if (service == 0x62) {

#ifdef GET_OPTIMIZED_MODE_POWER
             if (pid == 0x02) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif
#ifdef GET_OPTIMIZED_MODE_DURATION
             if (pid == 0x03) {
                parsedValue = (data[0] << 8) | data[1];
            }
#endif
#ifdef GET_MIN_CHARGE_LEVEL
             if (pid == 0x04) {
                parsedValue = data[0];
            }
#endif
#ifdef GET_MAX_CHARGE_LEVEL
             if (pid == 0x05) {
                parsedValue = data[0];
            }
#endif
#ifdef GET_SHUTDOWN_LEVEL
             if (pid == 0x06) {
                parsedValue = data[0];
            }
#endif
#ifdef GET_STOP_OPTIMIZE_LEVEL
             if (pid == 0x07) {
                parsedValue = data[0];
            }
#endif
        }

    }
return parsedValue;
}

void * register_can_item(QueueHandle_t qHandle, char * queueItem, sensor_data_item ditem)
{
    qHandle = xQueueCreate(1, sizeof(sensor_data_item));
    sensor_register( qHandle, sizeof(sensor_data_item), queueItem);
    // xQueueSendToBack(qHandle, &ditem, 10);

    return qHandle;
}

int parseCanItemToShadow(uint8_t service, uint8_t pid) {
    uint8_t tempData[CAN_DATA_PAYLOAD_SIZE];

    xSemaphoreTake(awsDataBuffSemaphore, portMAX_DELAY);
    for (int i = 0; i < awsDataBuffIndexCount; i++) {
        if (awsDataBuff[i].service == service && awsDataBuff[i].pid == pid) {
            LOGI(TAG, "Parsing AWS shadow item service:0x%x pid:0x%x, index: %i", service, pid, i);
            // take copy
            memcpy(tempData, awsDataBuff[i].data, CAN_DATA_PAYLOAD_SIZE * sizeof(uint8_t));
            break;
        } else if (i >= SHADOW_ITEM_LIST_MAX_SIZE) {
            LOGE(TAG, "Trying to parse item that's not in list service:0x%x pid:0x%x", service, pid);
            xSemaphoreGive(awsDataBuffSemaphore);
            return NOT_VALID_AWS_ITEM;
        }
    }
    xSemaphoreGive(awsDataBuffSemaphore);

    return parseCanItem(service, pid, tempData);
}

// --> energy shadow handlers
void iot_operatingMode_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("operatingMode: %d\n", operatingMode);

   CanOperation_t *tmpOp1 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};
   tmpData[0]= (uint8_t)(operatingMode);
   tmpOp1 =createOneTimeOperationWithData(0x61, 0x02, tmpData, 4);
   addCanOperation(tmpOp1);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        xQueueSendToBack(updatequeue, (void *)&poperatingModeJson, 10);
    }
}

void iot_minChargeLevel_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("minChargeLevel: %d\n", minChargeLevel);
   CanOperation_t *tmpOp2 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};
   tmpData[0]= (uint8_t)(minChargeLevel);
   tmpOp2 =createOneTimeOperationWithData(0x61, 0x05, tmpData, 4);
   addCanOperation(tmpOp2);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        xQueueSendToBack(updatequeue, (void *)&pminChargeLevelJson, 10);
    }
}

void iot_maxChargeLevel_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("maxChargeLevel: %d\n",  maxChargeLevel);
   CanOperation_t *tmpOp1 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};
   tmpData[0]= (uint8_t)(maxChargeLevel);

   tmpOp1 =createOneTimeOperationWithData(0x61, 0x06, tmpData, 4);
   addCanOperation(tmpOp1);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        xQueueSendToBack(updatequeue, (void *)&pmaxChargeLevelJson, 10);
    }
}

void iot_shutdownLevel_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("shutdownLevel: %d\n",  shutdownLevel);
   CanOperation_t *tmpOp1 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};
   tmpData[0]= (uint8_t)(shutdownLevel);

   tmpOp1 =createOneTimeOperationWithData(0x61, 0x07, tmpData, 4);
   addCanOperation(tmpOp1);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        xQueueSendToBack(updatequeue, (void *)&pshutdownLevelJson, 10);
    }
}

void iot_stopOptimizeLevel_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("stopOptimizeLevel: %d\n",  stopOptimizeLevel);
   CanOperation_t *tmpOp1 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};
   tmpData[0]= (uint8_t)(stopOptimizeLevel);

   tmpOp1 =createOneTimeOperationWithData(0x61, 0x08, tmpData, 4);
   addCanOperation(tmpOp1);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        xQueueSendToBack(updatequeue, (void *)&pstopOptimizeLevelJson, 10);
    }
}

void iot_optimizedModeDuration_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("optimizedModeDuration: %d\n", optimizedModeDuration);
   CanOperation_t *tmpOp1 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};
   tmpData[0]= (uint8_t)((long)optimizedModeDuration / 256);
   tmpData[1]= (uint8_t)((long)optimizedModeDuration % 256);


   tmpOp1 =createOneTimeOperationWithData(0x61, 0x04, tmpData, 4);

   addCanOperation(tmpOp1);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        xQueueSendToBack(updatequeue, (void *)&poptimizedModeDurationJson, 10);
    }
}

void iot_optimizedPower_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("optimizedPower: %d\n",  optimizedPower);
   CanOperation_t *tmpOp1 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};
   tmpData[0]= (uint8_t)((long)optimizedPower / 256);
   tmpData[1]= (uint8_t)((long)optimizedPower % 256);

   tmpOp1 =createOneTimeOperationWithData(0x61, 0x03, tmpData, 4);
   addCanOperation(tmpOp1);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        xQueueSendToBack(updatequeue, (void *)&poptimizedPowerJson, 10);
    }
}

void iot_storageCycleTest_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("storageCycleTest: %d\n", storageCycleTest);
   CanOperation_t *tmpOp1 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};

   tmpData[0]= (uint8_t)(storageCycleTest);
   tmpOp1 =createOneTimeOperationWithData(0x61, 0x02, tmpData, 4);
//    addCanOperation(tmpOp1);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        // xQueueSendToBack(updatequeue, (void *)&pstorageCycleTestJson, 10);
    }
}

void iot_batteryCycleTest_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext)
{
    printf("batteryCycleTest: %d\n",  batteryCycleTest);
    CanOperation_t *tmpOp1 ;
   uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};

   tmpData[0]= (uint8_t)(batteryCycleTest);
   tmpOp1 =createOneTimeOperationWithData(0x61, 0x01, tmpData, 4);
   addCanOperation(tmpOp1);

   QueueHandle_t updatequeue = shadow_update_register();
    // remove callback first
    if (pContext != NULL)
    {
        // xQueueSendToBack(updatequeue, (void *)&pContext, 10);
        xQueueSendToBack(updatequeue, (void *)&pbatteryCycleTestJson, 10);
    }
}

// <-- energy shadow handlers

void can_task(void * parm) {

QueueHandle_t deltaqueue = shadow_delta_register();
xQueueSendToBack(deltaqueue, (void *)&poperatingModeJson, 10);
xQueueSendToBack(deltaqueue, (void *)&pminChargeLevelJson, 10);
xQueueSendToBack(deltaqueue, (void *)&pmaxChargeLevelJson, 10);
xQueueSendToBack(deltaqueue, (void *)&pshutdownLevelJson, 10);
xQueueSendToBack(deltaqueue, (void *)&pstopOptimizeLevelJson, 10);
xQueueSendToBack(deltaqueue, (void *)&poptimizedModeDurationJson, 10);
xQueueSendToBack(deltaqueue, (void *)&poptimizedPowerJson, 10);
xQueueSendToBack(deltaqueue, (void *)&pstorageCycleTestJson, 10);
xQueueSendToBack(deltaqueue, (void *)&pbatteryCycleTestJson, 10);



    printf("Starting CAN controller [heap %d]\n", xPortGetFreeHeapSize());

    canTxSemaphore = xSemaphoreCreateBinary();

    canOpListSemaphore = xSemaphoreCreateBinary();
    xSemaphoreGive(canOpListSemaphore);

    dataBuffSemaphore = xSemaphoreCreateBinary();
    xSemaphoreGive(dataBuffSemaphore);

    awsDataBuffSemaphore = xSemaphoreCreateBinary();
    xSemaphoreGive(awsDataBuffSemaphore);

    canOpRemoveSemaphore = xSemaphoreCreateBinary();
    xSemaphoreGive(canOpRemoveSemaphore);

    LOGI(TAG, "Install can drivers");
    ESP_ERROR_CHECK(can_driver_install(&g_config, &t_config, &f_config));
    ESP_ERROR_CHECK(can_start());

#if CONFIG_L7_CAN_DATA_FAKING
    xTaskCreate(&canTxTask, "CanTx", 8192 /*Stack Size*/, NULL, TX_TASK_PRIO /*Priority*/, NULL /*Task Handle*/);
#else
    xTaskCreate(&canTxTask, "CanTx", 2048 /*Stack Size*/, NULL, TX_TASK_PRIO /*Priority*/, NULL /*Task Handle*/);
#endif
    // xTaskCreate(&canRxTask, "CanRx", 2048 /*Stack Size*/, NULL, RX_TASK_PRIO /*Priority*/, NULL /*Task Handle*/);
    xTaskCreate(&canRxTask, "CanRx", 2048 /*Stack Size*/, NULL, RX_TASK_PRIO /*Priority*/, NULL /*Task Handle*/);

    #ifdef CONFIG_L7_DRIVE_APP_MODE_CELLULAR

    xTaskCreate(&canSendDataTask, "CanSendData", 2048 /*Stack Size*/, NULL, RX_TASK_PRIO + 3 /*Priority*/, NULL /*Task Handle*/);
    #endif
    // tähän alku check, vastaako lainkaan

    while (!canConnected){
            CanOperation_t *tmpOp1 ;
            uint8_t tmpData[CAN_DATA_PAYLOAD_SIZE] = {0};
            tmpOp1 =createOneTimeOperationWithData(0x60, 0x00, tmpData, 4);
            // addCanOperation(tmpOp1);
            can_transmit(&tmpOp1->msg, portMAX_DELAY);
            vTaskDelay(10000/portTICK_RATE_MS);
    }




    initializeCanItems();

    canSendRxData(true);

    printf("CAN started [heap %d]\n", xPortGetFreeHeapSize());
    while(1)
    {
        vTaskDelay(2000/portTICK_RATE_MS);
    }
    ESP_LOGI(TAG, "kill task");
    vTaskDelete(NULL);
}



TaskHandle_t startCanController()
{
    ESP_LOGI(TAG, "starting can task");
    // vTaskDelay(2000/portTICK_RATE_MS);
    TaskHandle_t task_handle;
    xTaskCreate(&can_task, "can_task", 8192, NULL, 9, &task_handle);
    return task_handle;
}