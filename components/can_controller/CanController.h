#include "driver/can.h"
#include "sdkconfig.h"
#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "sensor_cache.h"

TaskHandle_t startCanController();
void * register_can_item(QueueHandle_t handle, char * queueItem, sensor_data_item dataitem);
QueueHandle_t xQueue_luxBatteryLevel, xQueue_luxBatteryCapacity, xQueue_luxBatteryCurrent, xQueue_luxBatteryVoltage, xQueue_luxOutputVoltage,
    xQueue_luxBatteryTemperature, xQueue_luxBatteryHeating, xQueue_luxHeatingPower, xQueue_luxBcsCooling, xQueue_luxBcsCoolingPower, 
    xQueue_luxBatteryChargingPower, xQueue_luxBcsInputPower, xQueue_luxTotalOutputPower, xQueue_luxOperatingMode, xQueue_luxBcsTemperature, 
    xQueue_luxBatteryCycleTest, xQueue_luxStorageSwitchState, xQueue_luxOptimizedPower, xQueue_luxOptimizedModeDuration, xQueue_luxMinChargeLevel,
    xQueue_luxMaxChargeLevel, xQueue_luxShutdownLevel, xQueue_luxStopOptimizeLevel;


#define NOT_IN_AWS_SHADOW       false
#define INCLUDE_IN_AWS_SHADOW   true

#ifdef CONFIG_L7_DRIVE_APP_MODE_CELLULAR

#define NOT_IN_AWS_SHADOW       false
#define INCLUDE_IN_AWS_SHADOW   true

// supported/active CAN items

// Service 0x01
#define ENGINE_RPM              0x01, 0x0C, 0,          NOT_IN_AWS_SHADOW
#define VEHICLE_SPEED           0x01, 0x0D, 600,        NOT_IN_AWS_SHADOW
#define THROTTLE_POSITION       0x01, 0x11, 0,          NOT_IN_AWS_SHADOW
#define ODO                     0x01, 0xA6, 1100,       NOT_IN_AWS_SHADOW
// // service 0x30
#define BATTERY_VOLTAGE         0x30, 0x02, 4000,       NOT_IN_AWS_SHADOW
#define THROTTLE_POSITION2      0x30, 0x03, 0,          NOT_IN_AWS_SHADOW
#define MOTOR_RPM               0x30, 0x04, 0,          NOT_IN_AWS_SHADOW
#define WHEEL_ROTATION_TIME     0x30, 0x05, 0,          NOT_IN_AWS_SHADOW
#define WHEEL_SIZE_CODE         0x30, 0x06, 0,          NOT_IN_AWS_SHADOW
#define MOTOR_POWER             0x30, 0x07, 0,          NOT_IN_AWS_SHADOW
#define ASSIST_LEVEL            0x30, 0x08, 333,        NOT_IN_AWS_SHADOW
#define MOTOR_POWER2            0x30, 0x09, 275,        NOT_IN_AWS_SHADOW
#define MAX_SPEED               0x30, 0x0A, 0,          NOT_IN_AWS_SHADOW
// // Service 0x31
#define BATTERY_CURRENT         0x31, 0x01, 3500,       NOT_IN_AWS_SHADOW
#define CHARGING_CURRENT        0x31, 0x02, 4500,       NOT_IN_AWS_SHADOW
#define U24_VOLTAGE             0x31, 0x03, 0,          NOT_IN_AWS_SHADOW
#define CELL_TEMP1              0x31, 0x04, 13000,      NOT_IN_AWS_SHADOW
#define CELL_TEMP2              0x31, 0x05, 14000,      NOT_IN_AWS_SHADOW
#define MOTOR_TEMP              0x31, 0x06, 15000,      NOT_IN_AWS_SHADOW
#define BLDC_TEMP               0x31, 0x07, 16000,      NOT_IN_AWS_SHADOW
#define MOTOR_STATUS            0x31, 0x08, 0,          NOT_IN_AWS_SHADOW
#define POWER_STATUS            0x31, 0x09, 0,          NOT_IN_AWS_SHADOW
#define BLDC_STATUS             0x31, 0x0A, 0,          NOT_IN_AWS_SHADOW
#define DRIVE_FW_VERSION        0x31, 0x0B, 30000,      INCLUDE_IN_AWS_SHADOW
#define ODO2                    0x31, 0x0C, 0,          NOT_IN_AWS_SHADOW
#define TRIP                    0x31, 0x0D, 1000,       NOT_IN_AWS_SHADOW
#define CHARGER_STATUS          0x31, 0x0E, 2500,       INCLUDE_IN_AWS_SHADOW
#define BATTERY_CAPACITY        0x31, 0x0F, 8000,       INCLUDE_IN_AWS_SHADOW
#define REMAINING_DIST          0x31, 0x10, 10000,      INCLUDE_IN_AWS_SHADOW
#define REMAINING_CHARGE_TIME   0x31, 0x11, 5000,       NOT_IN_AWS_SHADOW

// Service 0x33
#define GET_RPM_RATIO           0x33, 0x01, 0,          NOT_IN_AWS_SHADOW


// remember to increase this if adding more can items to shadow
#define SHADOW_ITEM_LIST_MAX_SIZE 31
// #else
// #define SHADOW_ITEM_LIST_MAX_SIZE 0
#endif


#ifdef CONFIG_L7_DRIVE_MODE_ENERGY

// supported/active CAN items for L7Drive Energy. (uint8_t service, uint8_t pid, int interval, bool includeInAwsShadow)

// Service 0x60
#define CAN_CHECK_ENERGY            0x60, 0x00, 0,          NOT_IN_AWS_SHADOW
#define BATTERY_LEVEL               0x60, 0x01, 600,        INCLUDE_IN_AWS_SHADOW
#define REMAINING_BATTERY_CAPACITY  0x60, 0x02, 600,        NOT_IN_AWS_SHADOW
#define BATTERY_CURRENT             0x60, 0x03, 600,        NOT_IN_AWS_SHADOW
#define BATTERY_VOLTAGE             0x60, 0x04, 600,        INCLUDE_IN_AWS_SHADOW
#define SYSTEM_OUTPUT_VOLTAGE       0x60, 0x05, 600,        NOT_IN_AWS_SHADOW
#define BATTERY_TEMPERATURE         0x60, 0x06, 600,        INCLUDE_IN_AWS_SHADOW
#define HEATING_BATTERY             0x60, 0x07, 600,        NOT_IN_AWS_SHADOW
#define HEATING_POWER               0x60, 0x08, 600,        INCLUDE_IN_AWS_SHADOW
#define COOLING_BCS                 0x60, 0x09, 600,        NOT_IN_AWS_SHADOW
#define COOLING_BCS_POWER           0x60, 0x0A, 600,        NOT_IN_AWS_SHADOW
#define BATTERY_CHARGING_POWER      0x60, 0x0B, 600,        NOT_IN_AWS_SHADOW
#define BCS_INPUT_POWER             0x60, 0x0C, 600,        NOT_IN_AWS_SHADOW
#define TOTAL_OUTPUT_POWER          0x60, 0x0D, 600,        NOT_IN_AWS_SHADOW
#define OPERATING_MODE              0x60, 0x0E, 600,        NOT_IN_AWS_SHADOW
#define L7DRIVE_BCS_TEMPERATURE     0x60, 0x0F, 600,        NOT_IN_AWS_SHADOW
#define BATTERYCYCLETEST_STATE      0x60, 0x10, 600,        NOT_IN_AWS_SHADOW
#define STORAGE_SWITCH_STATE        0x60, 0x11, 600,        NOT_IN_AWS_SHADOW


// Service 0x61
#define SET_BATTERYCYCLETEST_STATE  0x61, 0x01, 0,        NOT_IN_AWS_SHADOW
#define SET_STORAGE_SWITCH_STATE    0x61, 0x02, 0,        NOT_IN_AWS_SHADOW
#define SET_OPERATING_MODE          0x61, 0x03, 0,        NOT_IN_AWS_SHADOW
#define SET_OPTIMIZED_MODE_POWER    0x61, 0x04, 0,        NOT_IN_AWS_SHADOW
#define SET_OPTIMIZED_MODE_DURATION 0x61, 0x05, 0,        NOT_IN_AWS_SHADOW
#define SET_MIN_CHARGE_LEVEL        0x61, 0x06, 0,        NOT_IN_AWS_SHADOW
#define SET_MAX_CHARGE_LEVEL        0x61, 0x07, 0,        NOT_IN_AWS_SHADOW
#define SET_SHUTDOWN_LEVEL          0x61, 0x08, 0,        NOT_IN_AWS_SHADOW
#define STOP_OPTIMIZE_LEVEL         0x61, 0x09, 0,        NOT_IN_AWS_SHADOW

// Service 0x62
#define GET_OPTIMIZED_MODE_POWER    0x62, 0x02, 600,      NOT_IN_AWS_SHADOW
#define GET_OPTIMIZED_MODE_DURATION 0x62, 0x03, 600,      NOT_IN_AWS_SHADOW
#define GET_MIN_CHARGE_LEVEL        0x62, 0x04, 600,      NOT_IN_AWS_SHADOW
#define GET_MAX_CHARGE_LEVEL        0x62, 0x05, 600,      NOT_IN_AWS_SHADOW
#define GET_SHUTDOWN_LEVEL          0x62, 0x06, 600,      NOT_IN_AWS_SHADOW
#define GET_STOP_OPTIMIZE_LEVEL     0x62, 0x07, 600,      NOT_IN_AWS_SHADOW

// remember to increase this if adding more can items to shadow
#define SHADOW_ITEM_LIST_MAX_SIZE 32
#endif


// errorcodes
#define NOT_VALID_AWS_ITEM -1
#define UNDEFINED_CAN_ITEM -2
#define NOT_SUPPORTED_PID -3

typedef struct CanOperation {
    char service;
    char pid;
    can_message_t msg;
    uint32_t interval; // in ticks
    uint32_t targetTickCnt;
    struct CanOperation *nextItem;
} CanOperation_t;

#ifdef CONFIG_L7_DRIVE_APP_MODE_BT_ONLY
void canHwEnable();
void canHwDisable();
#endif

// void startCanController();
void printCanOperationList();
bool addCanOperation(CanOperation_t *op);
void removeCanOperation(char service, char pid);
void removeAllCanOperation();
void canParseTxMsg(uint32_t size, uint8_t *data);
void canSendRxData(bool en);
CanOperation_t *createOneTimeOperation(char service, char pid);
CanOperation_t *createOneTimeOperationWithData(char service, char pid, uint8_t* data, size_t dataLen);
CanOperation_t *createPeriodicOperation(char service, char pid, uint32_t interval /*ms*/, bool includeInShadow);
CanOperation_t *createPeriodicOperationWithData(char service, char pid, uint32_t interval /*ms*/, bool includeInShadow, uint8_t* data, size_t dataLen);
int getShadowItemIndex(uint8_t service, uint8_t pid);
void initializeCanItems(void);
void insertToStaticAwsBufferIfConfigured(uint8_t service, uint8_t pid, int interval, bool includeInAwsShadow);
float parseCanItem(uint8_t service, uint8_t pid, uint8_t* data);
int parseCanItemToShadow(uint8_t service, uint8_t pid);
void iot_operatingMode_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_minChargeLevel_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_maxChargeLevel_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_shutdownLevel_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_stopOptimizeLevel_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_optimizedModeDuration_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_optimizedPower_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_storageCycleTest_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void iot_batteryCycleTest_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);




