#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#include "esp_event_loop.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "sdkconfig.h"
#include "version.h"

#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "sensor_cache.h"

#include "ota_update_service.h"
#include "DataStorageHandler.h"

#define ALLOW_DOWNGRADE             true
#define OTA_SHADOW_FIELD_MAX_SIZE   150

static const char *TAG = "[OTAUpdate]";

extern const uint8_t ota_cert_pem_start[] asm("_binary_ota_root_cert_pem_start");
extern const uint8_t ota_cert_pem_end[] asm("_binary_ota_root_cert_pem_end");

static ota_update_info update_info;

static bool ota_update_ongoing = false;
char otaUpdateShadowConfiguration[OTA_SHADOW_FIELD_MAX_SIZE] = "\0";
void iot_ota_update_callback_handler(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);

jsonStruct_t otaUpdateJson = {"otaUpdate", otaUpdateShadowConfiguration, sizeof(otaUpdateShadowConfiguration) * sizeof(char) + 1, SHADOW_JSON_STRING, iot_ota_update_callback_handler};

bool verify_mode(const char* input) {
    bool ok = false;    
    update_info.mode = atoi(input);
    ok = update_info.mode >= 0 && update_info.mode <= 3;
    ESP_LOGI(TAG, "verify_mode %s (%d) %s", input, update_info.param, ok ? "OK" : "NOK");
    return ok;
}

bool verify_param(const char* input) {
    bool ok = true;
    update_info.param = atoi(input);
    ESP_LOGI(TAG, "verify_param %s (%d) %s", input, update_info.param, ok ? "OK" : "NOK");
    return ok;
}

bool verify_version(const char* input) {
    double version = atof(input);
    char* dot = strstr(input, ".");
    bool ok = (version > 0 && dot != NULL);

    if (ok) {
        char parse_version[10];
        strcpy(parse_version, input);
        char* data = strtok(parse_version, ".");            
        update_info.update_major_ver = atoi(data);
        data = strtok(NULL, "\0");
        update_info.update_minor_ver = atoi(data);

        if (!ALLOW_DOWNGRADE) {
            int currentVersionMajor = FW_VERSION_MAJOR;
            int currentVersionMinor = FW_VERSION_MINOR;

            if (currentVersionMajor <= update_info.update_major_ver) {
                ok = (currentVersionMinor <= update_info.update_minor_ver);
            }
            else {
                ok = false;
            }
        }
    }
    else {
        update_info.update_major_ver = 0;
        update_info.update_minor_ver = 0;
    }

    ESP_LOGI(TAG, "verify_version %s (current %d.%d, update %d.%d, downgrade mode: %s) %s", input, 
        FW_VERSION_MAJOR, FW_VERSION_MINOR, 
        update_info.update_major_ver, update_info.update_minor_ver, 
        ALLOW_DOWNGRADE ? "allowed" : "not allowed", 
        ok ? "OK" : "NOK");

    return ok;
}

bool verify_bucket_url(const char* input) {
    bool ok = false;
    int len = strlen(input);
    if (len > 4) {
        ok = (strncmp(input, "http", 4) == 0);
    }

    if (ok) {
        strcpy(update_info.bucket_url, input);
    }
    else {
        strcpy(update_info.bucket_url, "\0");
    }

    ESP_LOGI(TAG, "verify_bucket_url %s %s", input, ok ? "OK" : "NOK");
    return ok;
}

bool verify_file_name(const char* input) {
    bool ok = false;
    int start_index = strlen(input) - 4;
    if (start_index >= 0) {
        ok = (strncmp((char*)&input[start_index], ".bin", 4) == 0);
    }

    if (ok) {
        strcpy(update_info.image_name, input);
    }
    else {
        strcpy(update_info.image_name, "\0");
    }

    ESP_LOGI(TAG, "verify_file_name %s %s", input, ok ? "OK" : "NOK");
    return ok;
}

bool verify_update_size(const char* input) {
    int size = atoi(input);
    bool ok = size > 0;
    update_info.package_size = ok ? size : 0;
    ESP_LOGI(TAG, "verify_update_size %s %s", input, ok ? "OK" : "NOK");
    return ok;
}

bool verify_ota_config(const char* msg, const int msg_len) {
    
    const int expected_comma_count = 5;

    int comma_count = 0;
    
    bool mode_ok        = false;
    bool param_ok       = false;
    bool version_ok     = false;
    bool bucket_url_ok  = false;
    bool filename_ok    = false;
    bool size_ok        = false;

    char buf[OTA_SHADOW_FIELD_MAX_SIZE];
    int start_index = 0;
    int last_comma_index = -1;
    for (int i = 0; i < msg_len; i++) {
        if (msg[i] == ',') {
            last_comma_index = i;
            memset(buf, 0, OTA_SHADOW_FIELD_MAX_SIZE);
            int token_len = i - start_index;
            if (token_len > 0 && start_index < msg_len) {
                memcpy(buf, (char*)&msg[start_index], token_len);
                switch (comma_count) {
                    case 0:
                        // end of mode
                        mode_ok = verify_mode(buf);
                        if (update_info.mode != OTA_STATUS_CALLBACK_MODE_TRIGGER) {
                            ESP_LOGD(TAG, "OTA trigger not set, no need to continue parsing...");
                            return false;
                        }
                        break;
                    case 1:
                        // end of param
                        param_ok = verify_param(buf);
                        break;
                    case 2:
                        // end of version
                        version_ok = verify_version(buf);
                        break;
                    case 3:
                        // end of bucket URL
                        bucket_url_ok = verify_bucket_url(buf);                   
                        break;
                    case 4:
                        // end of file name
                        filename_ok = verify_file_name(buf);
                        break;
                }
            }

            comma_count++;
            if (comma_count < expected_comma_count) {
                start_index = i + 1;
            }
        }
    }

    if (last_comma_index > -1 && last_comma_index + 1 < msg_len) {
        start_index = last_comma_index + 1;
        memset(buf, 0, OTA_SHADOW_FIELD_MAX_SIZE);
        memcpy(buf, (char*)&msg[start_index], msg_len - start_index);
        size_ok = verify_update_size(buf);
    }

    return (comma_count == expected_comma_count && mode_ok && param_ok && version_ok && bucket_url_ok && filename_ok && size_ok);
}

esp_err_t _http_event_handler(esp_http_client_event_t *evt) {
    switch (evt->event_id) {
    case HTTP_EVENT_ERROR:
        ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
        break;
    }
    return ESP_OK;
}

void clear_ota_shadow() {
    memset(otaUpdateShadowConfiguration, 0, sizeof(otaUpdateShadowConfiguration));
    struct jsonStruct* pOtaUpdateJson = &otaUpdateJson;
    QueueHandle_t queue = shadow_reported_and_desired_register();
    xQueueSendToBack(queue, (void*)&pOtaUpdateJson, 10);
}

void start_ota_service() {
    ESP_LOGI(TAG, "Starting OTA service");
    QueueHandle_t deltaqueue = shadow_delta_register();
    struct jsonStruct* pOtaUpdateJson = &otaUpdateJson;
    xQueueSendToBack(deltaqueue, (void*)&pOtaUpdateJson, 10);
}

void iot_ota_update_callback_handler(const char* pJsonString, uint32_t JsonStringDataLen, jsonStruct_t* pContext)
{
    ESP_LOGI(TAG, "iot_ota_update_callback_handler %s", otaUpdateShadowConfiguration);
    char ota_info[OTA_SHADOW_FIELD_MAX_SIZE] = "\0";
    strcpy(ota_info, otaUpdateShadowConfiguration);
    if (!verify_ota_config(ota_info, strlen(ota_info))) {
        ESP_LOGW(TAG, "OTA update configuration invalid or ignored");
    }    
    else if (update_info.mode == OTA_STATUS_CALLBACK_MODE_TRIGGER) {
        if (!ota_update_ongoing) {
            ESP_LOGI(TAG, "Triggering OTA update");
            initialize_ota();
        }
        else {
            ESP_LOGW(TAG, "OTA already started");
        }
    }

    QueueHandle_t queue = shadow_reported_and_desired_register();

    if (pContext != NULL) {
        xQueueSendToBack(queue, (void*)&pContext, 10);
    }
}

void initialize_ota() {
    ESP_LOGI(TAG, "Initialize OTA");
    xTaskCreate(&ota_task, OTA_UPDATE_TASK_NAME, 8192, &ota_callback, 5, &ota_task_handle);
}

void update_ota_state(int mode, int param) {
    update_info.mode = mode;
    update_info.param = param;
    sprintf(otaUpdateShadowConfiguration, "%d,%d,%d.%d,%s,%s,%d", update_info.mode, 
        update_info.param, 
        update_info.update_major_ver, 
        update_info.update_minor_ver, 
        update_info.bucket_url, 
        update_info.image_name, 
        update_info.package_size);

    struct jsonStruct* pOtaUpdateJson = &otaUpdateJson;

    QueueHandle_t queue = shadow_reported_and_desired_register();
    xQueueSendToBack(queue, (void*)&pOtaUpdateJson, 10);
}

void ota_task(void* ota_status_callback) {
    ESP_LOGI(TAG, "Starting OTA task...");
    ota_update_ongoing = true;
    void (*ota_status_callback_ptr)(int, int) = ota_status_callback;

    char url[256];
    sprintf(url, "%s/%d.%d/%s", update_info.bucket_url, update_info.update_major_ver, update_info.update_minor_ver, update_info.image_name);

    esp_http_client_config_t config = {
        .url = url,
        .cert_pem = (char*)ota_cert_pem_start, 
        .event_handler = _http_event_handler
    };

    vTaskDelay(5000 / portTICK_PERIOD_MS);
    ota_status_callback_ptr(OTA_STATUS_CALLBACK_MODE_STARTED, 0);

    esp_err_t ret = esp_https_ota(&config, update_info.package_size, ota_status_callback);
    if (ret == ESP_OK) {
        ota_status_callback_ptr(OTA_STATUS_CALLBACK_MODE_RESULT, ESP_OK);
    }
    else {
        ESP_LOGE(TAG, "Firmware Upgrade Failed");
        ota_status_callback_ptr(OTA_STATUS_CALLBACK_MODE_RESULT, ret);
    }

    while (1) {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
}

void ota_callback(const int mode, const int param) {
    switch (mode) {
        case OTA_STATUS_CALLBACK_MODE_PROGRESS: {
            ESP_LOGI(TAG, "OTA_STATUS_CALLBACK_MODE_PROGRESS %d", param);
            if (param > 0 && param % 10 == 0) {
                update_ota_state(mode, param);
            }
        }    
        break;
        case OTA_STATUS_CALLBACK_MODE_RESULT: {
            ESP_LOGI(TAG, "OTA_STATUS_CALLBACK_MODE_RESULT %d", param);
            update_ota_state(mode, param);
            ota_update_ongoing = false;
            if (param == ESP_OK) {
                vTaskDelay(5000 / portTICK_PERIOD_MS);
                ESP_LOGI(TAG, "Rebooting device after OTA...");
                abort();
            }
            else {
                vTaskDelay(5000 / portTICK_PERIOD_MS);
                vTaskDelete(ota_task_handle);
            }
        }
        break;
        case OTA_STATUS_CALLBACK_MODE_STARTED: {
            ESP_LOGI(TAG, "OTA_STATUS_CALLBACK_MODE_STARTED %d", param);
            update_ota_state(mode, param);
        }    
        break;
    };
}