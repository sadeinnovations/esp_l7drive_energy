#define OTA_UPDATE_TASK_NAME "OTA update task"

#define OTA_UPDATE_ERROR_INVALID_TRIGGER    1
#define OTA_UPDATE_ERROR_NETWORK_CONNECTION 2

#define OTA_STATUS_CALLBACK_MODE_RESULT     0
#define OTA_STATUS_CALLBACK_MODE_PROGRESS   1
#define OTA_STATUS_CALLBACK_MODE_STARTED    2
#define OTA_STATUS_CALLBACK_MODE_TRIGGER    3

#include "ota_update_info.h"

TaskHandle_t ota_task_handle;

void start_ota_service();
void initialize_ota();
void ota_task(void* ota_status_callback);
void ota_callback(const int mode, const int param);
