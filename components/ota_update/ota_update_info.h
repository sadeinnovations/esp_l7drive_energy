typedef struct ota_update_info {
    int mode;
    int param;
    int update_major_ver;
    int update_minor_ver;
    char bucket_url[256];
    char image_name[64];
    int package_size;

} ota_update_info;