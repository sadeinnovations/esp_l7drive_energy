#ifndef _SENSIRION_SERVICE
#define _SENSIRION_SERVICE
TaskHandle_t sps30_service();
TaskHandle_t scd30_service();
TaskHandle_t sht31_service();
double scd30_get_temperature();
double sht31_get_temperature();
double sht31_get_humidity();
double sht31_get_temperature2();
double sht31_get_humidity2();
#endif