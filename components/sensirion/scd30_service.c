#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_system.h"
#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "i2c_rw.h"
#include "scd30.h"
#include "bme280_service.h"
#include "sensor_cache.h"
#include <rom/rtc.h>

#define SCD30_DEBUG
#define TAG "scd30"

RTC_DATA_ATTR static f32 co2_ppm, temperature, relative_humidity;

// dustSleep is the time in seconds that is waited after first measurement was done
RTC_DATA_ATTR static int co2Sleep = 2*60;
static jsonStruct_t co2Sleepjson  =    {"co2Sleep",            &co2Sleep,      sizeof(int),     SHADOW_JSON_INT32,     NULL};

// dustInterval is how often dust is measured when switching from deep sleep
RTC_DATA_ATTR static int co2Interval = 6;
static jsonStruct_t co2Intervaljson  =    {"co2Interval",            &co2Interval,      sizeof(int),     SHADOW_JSON_INT32,     NULL};
RTC_DATA_ATTR static int MeasCounter = 0;
RTC_DATA_ATTR static time_t scd30MeasTime;

bool co2MeasStarted = false;

static int scd30_wait(int waitseconds)
{   
    int elapsed = 0;
    while(elapsed<waitseconds)
    {
        vTaskDelay(1000 / portTICK_RATE_MS);
        elapsed++;
        if(device_parameters.enter_deep_sleep)
            break;
    }
    if(device_parameters.enter_deep_sleep)
        return 1;
    else
        return 0;

}

void scd30_task(void * parm)
{
    QueueHandle_t co2_xQueue, co2temp_xQueue, co2hum_xQueue;
    u16 data_ready;
    s16 ret;
    s16 interval_in_seconds = 2;
    sensor_data_item sensordata;
    time_t lastmeas;
    time_t now;
    time_t update_interval = DEFAULT_UPDATE_INTERVAL;
    uint8_t retrycount = 10;
    u16 ambient_pressure = 0;

    // shadow is not read when woken up from deep sleep
    if(device_parameters.reset_reason != DEEPSLEEP_RESET)
    {
        // Get shadow delta queue handler and add settings json 
        struct jsonStruct *pco2Sleepjson = &co2Sleepjson;
        QueueHandle_t deltaqueue = shadow_delta_register();
        xQueueSendToBack(deltaqueue,  ( void * ) &pco2Sleepjson, 10);

        struct jsonStruct *pco2Intervaljson = &co2Intervaljson;
        xQueueSendToBack(deltaqueue,  ( void * ) &pco2Intervaljson, 10);
    }

    if((device_parameters.single_measurement && MeasCounter == 0) || !device_parameters.single_measurement)
    {
        // power on co2 sensor
        gpio_set_direction(GPIO_NUM_4, GPIO_MODE_OUTPUT);
        gpio_set_level(GPIO_NUM_4, 1);
        vTaskDelay(1000 / portTICK_RATE_MS);

        while (scd_probe() != STATUS_OK && retrycount != 0) {
            if(retrycount == 5)
            {
                scd_soft_reset();
                vTaskDelay(1000 / portTICK_RATE_MS);
            }
            vTaskDelay(1000 / portTICK_RATE_MS);
            retrycount--;
        }
    }
    if(retrycount == 0)
    {
        ESP_LOGE(TAG, "SCD30 init failed\n");
    }
    else
    {
        co2_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        sensor_register(co2_xQueue, sizeof( sensor_data_item ), "co2"); 
        co2temp_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        sensor_register(co2temp_xQueue, sizeof( sensor_data_item ), "co2Temperature"); 
        co2hum_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        sensor_register(co2hum_xQueue, sizeof( sensor_data_item ), "co2Humidity"); 

        if((device_parameters.single_measurement && MeasCounter == 0) || !device_parameters.single_measurement)
        {
            scd_enable_automatic_self_calibration(false);
            vTaskDelay(50 / portTICK_RATE_MS);  
            scd_set_measurement_interval(interval_in_seconds);
            vTaskDelay(1000 / portTICK_RATE_MS);
            ambient_pressure = bme280_get_pressure();

            while(!device_parameters.enter_deep_sleep)
            {   
                if(co2MeasStarted == false){
                    ret = (u16)scd_start_periodic_measurement(ambient_pressure); 
                    co2MeasStarted = true;
                    vTaskDelay(30*1000 / portTICK_RATE_MS);
                } 
                time(&lastmeas);       
                    
                ret = scd_get_data_ready(&data_ready);
                if(data_ready)
                {
                    ret = scd_read_measurement(&co2_ppm, &temperature, &relative_humidity);
                    if(ret == STATUS_OK)
                    {
                        time(&scd30MeasTime);
                        sensordata.timestamp = scd30MeasTime;
                        sensordata.data = co2_ppm;
                        xQueueSendToBack( co2_xQueue, &sensordata, 10 );
                        sensordata.data = temperature;
                        xQueueSendToBack( co2temp_xQueue, &sensordata, 10 );
                        sensordata.data = relative_humidity;
                        xQueueSendToBack( co2hum_xQueue, &sensordata, 10 );
                        #ifdef SCD30_DEBUG
                        printf("measured co2 concentration: %0.2f ppm, "
                            "measured temperature: %0.2f degreeCelsius, "
                            "measured humidity: %0.2f %%RH\n",
                            co2_ppm, temperature, relative_humidity);
                        #endif
                    }   
                    else
                    {
                        ESP_LOGE(TAG, "scd30 meas read failed %d\n", ret);
                    }
                }
                if(co2Sleep > 60){
                    scd_stop_periodic_measurement();
                    co2MeasStarted = false;
                }    
                if(scd30_wait(co2Sleep)) {goto end;}
            }
        }
        else{
            ESP_LOGI(TAG, "SCD30 insert old data %d / %d\n", MeasCounter, co2Interval);
            sensordata.timestamp = scd30MeasTime;
            sensordata.data = co2_ppm;
            xQueueSendToBack( co2_xQueue, &sensordata, 10 );
            sensordata.data = temperature;
            xQueueSendToBack( co2temp_xQueue, &sensordata, 10 );
            sensordata.data = relative_humidity;
            xQueueSendToBack( co2hum_xQueue, &sensordata, 10 );
        }
        // power off the sensor here

    }
    end:
    printf("SCD30 shutdown\n");
    MeasCounter++;
    if(MeasCounter > co2Interval)
        MeasCounter = 0;
    // Power off co2 sensor
    gpio_set_level(GPIO_NUM_4, 0);    

    vTaskDelete(NULL);
}

TaskHandle_t scd30_service()
{
    TaskHandle_t task_handle;
    xTaskCreate(&scd30_task, "scd30_task", 2048, NULL, 9, &task_handle);
    return task_handle;
}
