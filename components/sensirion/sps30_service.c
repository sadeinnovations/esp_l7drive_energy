#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_system.h"
#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "i2c_rw.h"
#include "sps30.h"
#include "sensor_cache.h"
#include "DataStorageHandler.h"
#include <rom/rtc.h>

#define SPS30_DEBUG
#define TAG "sps30"

// dustSleep is the time in seconds that is waited after first measurement was done
RTC_DATA_ATTR static int dustSleep = 5*60;
static jsonStruct_t dustSleepjson  =    {"dustSleep",            &dustSleep,      sizeof(int),     SHADOW_JSON_INT32,     NULL};

// dustInterval is how often dust is measured when switching from deep sleep
RTC_DATA_ATTR static int dustInterval = 6;
static jsonStruct_t dustIntervaljson  =    {"dustInterval",            &dustInterval,      sizeof(int),     SHADOW_JSON_INT32,     NULL};
RTC_DATA_ATTR static int MeasCounter = 0;

RTC_DATA_ATTR static time_t sps30MeasTime;
RTC_DATA_ATTR static struct sps30_measurement m;

static int sps30_wait(int waitseconds)
{   
    int elapsed = 0;
    while(elapsed<waitseconds)
    {
        vTaskDelay(1000 / portTICK_RATE_MS);
        elapsed++;
        if(device_parameters.enter_deep_sleep)
            break;
    }
    if(device_parameters.enter_deep_sleep)
        return 1;
    else
        return 0;

}

void sps30_task(void * parm)
{
    QueueHandle_t pm1p0_xQueue, pm2p5_xQueue, pm4p0_xQueue, pm10p0_xQueue;
    u16 data_ready;
    s16 ret;
    u8 retrycount = 10;
    time_t now;
    time_t update_interval = DEFAULT_UPDATE_INTERVAL;
    sensor_data_item dataitem;     

    // shadow is not read when woken up from deep sleep
    if(device_parameters.reset_reason != DEEPSLEEP_RESET)
    {
        // Get shadow delta queue handler and add settings json 
        struct jsonStruct *pdustSleepjson = &dustSleepjson;
        QueueHandle_t deltaqueue = shadow_delta_register();
        xQueueSendToBack(deltaqueue,  ( void * ) &pdustSleepjson, 10);

        struct jsonStruct *pdustIntervaljson = &dustIntervaljson;
        xQueueSendToBack(deltaqueue,  ( void * ) &pdustIntervaljson, 10);
    }

    if((device_parameters.single_measurement && MeasCounter == 0) || !device_parameters.single_measurement)
    {
        // power on dust sensor
        gpio_set_direction(GPIO_NUM_25, GPIO_MODE_OUTPUT);
        gpio_set_level(GPIO_NUM_25, 1);
        vTaskDelay(1000 / portTICK_RATE_MS);

        while (sps30_probe() != 0 && retrycount > 0) {
            if(retrycount == 5)
            {
                sps30_reset();
            }
            vTaskDelay(100 / portTICK_RATE_MS);
            retrycount--;
        }
    }

    if(retrycount == 0)
    {
        ESP_LOGE(TAG, "SPS30 init failed\n");
    }
    else
    {   
        // generate queues for measurement data
        pm1p0_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        sensor_register(pm1p0_xQueue, sizeof( sensor_data_item ), "dust1p0");
        pm2p5_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        sensor_register(pm2p5_xQueue, sizeof( sensor_data_item ), "dust2p5");
        pm4p0_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        sensor_register(pm4p0_xQueue, sizeof( sensor_data_item ), "dust4p0");
        pm10p0_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        sensor_register(pm10p0_xQueue, sizeof( sensor_data_item ), "dust10p0"); 

        if((device_parameters.single_measurement && MeasCounter == 0) || !device_parameters.single_measurement)
        {
            while(!device_parameters.enter_deep_sleep)
            {
                ret = sps30_start_measurement();
                if(sps30_wait(30)) {goto end;}
                if(ret == 0)
                {
                    do{
                        vTaskDelay(1000 / portTICK_RATE_MS);
                        sps30_read_data_ready(&data_ready);

                    }while(!data_ready);
                    ret = sps30_read_measurement(&m);
                    if(ret == 0)
                    {
                        time(&sps30MeasTime);
                        dataitem.timestamp = sps30MeasTime;
                        dataitem.data = m.mc_1p0;
                        xQueueSendToBack( pm1p0_xQueue, &dataitem, 10 );
                        dataitem.data = m.mc_2p5;
                        xQueueSendToBack( pm2p5_xQueue, &dataitem, 10 );
                        dataitem.data = m.mc_4p0;
                        xQueueSendToBack( pm4p0_xQueue, &dataitem, 10 );
                        dataitem.data = m.mc_10p0;
                        xQueueSendToBack( pm10p0_xQueue, &dataitem, 10 );
                        #ifdef SPS30_DEBUG
                                    printf("measured values:\n"
                                            "\t%0.2f pm1.0\n"
                                            "\t%0.2f pm2.5\n"
                                            "\t%0.2f pm4.0\n"
                                            "\t%0.2f pm10.0\n"
                                            "\t%0.2f nc0.5\n"
                                            "\t%0.2f nc1.0\n"
                                            "\t%0.2f nc2.5\n"
                                            "\t%0.2f nc4.5\n"
                                            "\t%0.2f nc10.0\n"
                                            "\t%0.2f typical particle size\n\n",
                                            m.mc_1p0, m.mc_2p5, m.mc_4p0, m.mc_10p0,
                                            m.nc_0p5, m.nc_1p0, m.nc_2p5, m.nc_4p0, m.nc_10p0,
                                            m.typical_particle_size);
                        #endif
                    }
                    else
                    {
                        ESP_LOGE(TAG, "sps30 meas read failed %d\n", ret);

                    }

                }
                if((dustSleep - 30)>60){
                    sps30_stop_measurement();
                }
                //vTaskDelay(4000 / portTICK_RATE_MS);
                if(sps30_wait(dustSleep - 30)) {goto end;}
            }
        }
        else
        {
            ESP_LOGI(TAG, "SPS30 insert old data %d / %d\n", MeasCounter, dustInterval);
            dataitem.timestamp = sps30MeasTime;
            dataitem.data = m.mc_1p0;
            xQueueSendToBack( pm1p0_xQueue, &dataitem, 10 );
            dataitem.data = m.mc_2p5;
            xQueueSendToBack( pm2p5_xQueue, &dataitem, 10 );
            dataitem.data = m.mc_4p0;
            xQueueSendToBack( pm4p0_xQueue, &dataitem, 10 );
            dataitem.data = m.mc_10p0;
            xQueueSendToBack( pm10p0_xQueue, &dataitem, 10 );
        }

    }
    end:

    ESP_LOGI(TAG, "SPS30 shutdown\n");

    MeasCounter++;
    if(MeasCounter > dustInterval)
        MeasCounter = 0;

    // Power off dust sensor
    gpio_set_level(GPIO_NUM_25, 0);

    vTaskDelete(NULL);
}


TaskHandle_t sps30_service()
{
    TaskHandle_t task_handle;
    xTaskCreate(&sps30_task, "sps30_task", 4096, NULL, 9, &task_handle);
    return task_handle;
}
