#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_system.h"
#include "cJSON.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
#include "i2c_rw.h"
#include "sht.h"
#include "sensirion_arch_config.h"
#include "sensor_cache.h"

//#define SHT31_DEBUG
#define ADDRESS1 0x44
#define ADDRESS2 0x45

//static f32 sht31_temperature = 0;
static s32 temperatureExt, humidityExt;
static s32 temperature, humidity;
//--------
static s32 temperature2Ext, humidity2Ext;
static s32 temperature2, humidity2;
bool sht31_1=false;
bool sht31_2=false;
//--------

static int sht31_wait(int waitseconds)
{   
    int elapsed = 0;
    while(elapsed<waitseconds)
    {
        vTaskDelay(1000 / portTICK_RATE_MS);
        elapsed++;
        if(device_parameters.enter_deep_sleep)
            break;
    }
    if(device_parameters.enter_deep_sleep)
        return 1;
    else
        return 0;

}
void sht31_task(void * parm)
{
    QueueHandle_t shttemp_xQueue, shthum_xQueue, shttemp2_xQueue, shthum2_xQueue;
    //u16 data_ready;
    s8 ret;
    //s16 interval_in_seconds = 2;
    sensor_data_item sensordata, sensordata2;
    time_t lastmeas;
    //time_t now;
    //time_t update_interval = DEFAULT_UPDATE_INTERVAL;
    uint8_t retrycount = 10;

    

    sht31_soft_reset(ADDRESS1);
    
    
    while (sht_probe(ADDRESS1) != 0 && retrycount > 0) {
        if(retrycount == 5)
        {
            sht31_soft_reset(ADDRESS1);
        }
        vTaskDelay(1000 / portTICK_RATE_MS);
        retrycount--;
    }
    if(retrycount == 0)
    {
        printf("SHT31_1 init failed\n");
        sht31_1=false;
    }
    else sht31_1=true;
    #ifdef CONFIG_L7_DRIVE_MODE_ENERGY

        sht31_soft_reset(ADDRESS2);
        while (sht_probe(ADDRESS2) != 0 && retrycount > 0) {
            if(retrycount == 5)
            {
                sht31_soft_reset(ADDRESS2);
            }
            vTaskDelay(1000 / portTICK_RATE_MS);
            retrycount--;
        }
        if(retrycount == 0)
        {
            printf("SHT31_2 init failed\n");
            sht31_2=false;
        }
        else sht31_2=true;
    #endif

    if (sht31_2==true || sht31_1==true)
    // else
    {
        shttemp_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        if (sht31_1==true) sensor_register(shttemp_xQueue, sizeof( sensor_data_item ), "poleTemperature"); 
        shthum_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
        if (sht31_1==true) sensor_register(shthum_xQueue, sizeof( sensor_data_item ), "poleHumidity");
        #ifdef CONFIG_L7_DRIVE_MODE_ENERGY
            shttemp2_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
            if (sht31_2==true) sensor_register(shttemp2_xQueue, sizeof( sensor_data_item ), "poleSurfTemperature");
            shthum2_xQueue = xQueueCreate( 1, sizeof( sensor_data_item ) );
            if (sht31_2==true) sensor_register(shthum2_xQueue, sizeof( sensor_data_item ), "poleSurfHumidity");            
        #endif

        while(!device_parameters.enter_deep_sleep)
        {
            time(&lastmeas);
            sht_measure(ADDRESS1);
            vTaskDelay(25 / portTICK_RATE_MS);
            ret = sht_read(&temperatureExt, &humidityExt, ADDRESS1);
            if(ret==0)
            {
                temperature = temperatureExt;
                humidity = humidityExt;
                time(&sensordata.timestamp);               
                sensordata.data = temperatureExt/1000.0f;
                xQueueSendToBack( shttemp_xQueue, &sensordata, 10 );
                sensordata.data = humidityExt/1000.0f;
                xQueueSendToBack( shthum_xQueue, &sensordata, 10 );

                #ifdef SHT31_DEBUG
                    printf("sht31_1: temperature: %0.2f degreeCelsius, "
                     " humidity: %0.2f %%RH\n",
                     temperatureExt /1000.0f, humidityExt/1000.0f);
                #endif             
            }
            else
            {
               // #ifdef SHT31_DEBUG
                    printf("sht31_1 meas read failed %d\n", ret);
                //#endif
            }
            #ifdef CONFIG_L7_DRIVE_MODE_ENERGY
                // Energy: read sensor 2
                vTaskDelay(10/portTICK_RATE_MS);
                time(&lastmeas);
                sht_measure(ADDRESS2);
                vTaskDelay(25 / portTICK_RATE_MS);
                ret = sht_read(&temperature2Ext, &humidity2Ext, ADDRESS2);
                if(ret==0)
                {
                    temperature2 = temperature2Ext;
                    humidity2 = humidity2Ext;
                    time(&sensordata2.timestamp);               
                    sensordata2.data = temperature2Ext/1000.0f;
                    xQueueSendToBack( shttemp2_xQueue, &sensordata2, 10 );
                    sensordata2.data = humidity2Ext/1000.0f;
                    xQueueSendToBack( shthum2_xQueue, &sensordata2, 10 );

                    #ifdef SHT31_DEBUG
                        printf("sht31_2: temperature: %0.2f degreeCelsius, "
                        " humidity: %0.2f %%RH\n",
                        temperature2Ext /1000.0f, humidity2Ext/1000.0f);
                    #endif             
                }
                else
                {
                #ifdef SHT31_DEBUG
                        printf("sht31_2 meas read failed %d\n", ret);
                    #endif
                }
            #endif
            vTaskDelay(1000/portTICK_RATE_MS);
        }
    }
//    end:

    printf("SHT31 shutdown\n");
    sht_enable_low_power_mode(1);
    //sht31_stop_measurement();
    vTaskDelete(NULL);
}

double sht31_get_temperature()
{
    //xSemaphoreTake(scd30_data_mutex, portMAX_DELAY);
    double data = temperature /1000.0f;
    //xSemaphoreGive(scd30_data_mutex);
    return data;
}
double sht31_get_humidity()
{
    double data = humidity /1000.0f;
    return data;
}

double sht31_get_temperature2()
{
    //xSemaphoreTake(scd30_data_mutex, portMAX_DELAY);
    double data = temperature2 /1000.0f;
    //xSemaphoreGive(scd30_data_mutex);
    return data;
}
double sht31_get_humidity2()
{
    double data = humidity2 /1000.0f;
    return data;
}

TaskHandle_t sht31_service()
{
    TaskHandle_t task_handle;
    xTaskCreate(&sht31_task, "sht31_task", 2048, NULL, 9, &task_handle);
    return task_handle;
}