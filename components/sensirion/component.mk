#
# Component Makefile
#

COMPONENT_ADD_INCLUDEDIRS := . \
	embedded-common \
	embedded-common/hw_i2c \
	embedded-scd/scd30 \
	embedded-sps/sps30-i2c \
	embedded-sht/sht3x \
	embedded-sht/sht-common

COMPONENT_SRCDIRS := . \
	embedded-common \
	embedded-common/hw_i2c \
	embedded-scd/scd30 \
	embedded-sps/sps30-i2c \
	embedded-sht/sht3x \
	embedded-sht/sht-common