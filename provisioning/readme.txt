Prerequisites for provisioner.py script
=======================================

provisioner.py relies on the AWS CLI tool and openssl

1. Verify that you have openssl installed

$ openssl version

If the command above fails, install openssl by following installation instructions for your specific OS.
If it succeeds (you get a response like 'LibreSSL 2.2.7' or something similar) -> Everything OK! Move on to step 2.

2. Install AWS CLI

$ pip install awscli --upgrade --user

3. Fetch/generate access keys for AWS CLI

You can fetch/generate the needed keys for configuring AWS CLI from AWS Management Console '<Your name> (dropdown menu in the upper right corner) -> My Security Credentials'

"Create access key" button

4. Configure AWS CLI for your user

$ aws configure

AWS Access Key ID: <enter the access key ID from AWS management console>
AWS Secret Access Key: <enter the secret key from AWS management console>
Default region name: <enter the region to use, e.g. us-east-1>
Default output format: json

NOTE: The selected region has impact on the provisioner.py script, i.e. you might have to make a small modification if you want to use e.g. 'eu-west-1' instead of 'us-east-1'
If you want to change the region later, run 'aws configure' again and skip through the access key and secret key steps by just pressing Enter when prompted. 



provisioner.py Usage instructions
=================================

$ python provisioner.py

>>add 24:0A:C4:1D:53:36

adds a device with serial number 24:0A:C4:1D:53:36 for bulk provisioning$

>>add 3C:71:BF:12:29:66

adds a device with serial number 3C:71:BF:12:29:66 for bulk provisioning

>>upload

uploads the bulk provisioning file to S3 bucket

>>execute

executes the bulk provisioning task

>>status

shows the status of the bulk provisioning task. Use command ‘errors’ if status indicates failure.

>>getcerts

in case of successful provisioning task, this retrieves the signed certificates for the provisioned devices. At this stage, the certificates are in a single file.

>>partition

creates individual certificate files for each device, and based on those generates a NVS partition binary for each device (e.g. nvs_240AC41D5336.bin)

>>q

exits the script

===============================================================================================================================================================================

Erase flash:

$ make erase_flash

Flash the NVS partition with the device specific certificates and metadata:

$ python ~/esp/esp-idf/components/esptool_py/esptool/esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 --before default_reset --after hard_reset write_flash 0x9000 ./nvs_240AC41D5336.bin


NOTE!: The command below is already done by the provisioner.py script. Do this only if you want to re-generate the binary image based on some changes you have made in the nvs CSV file.

$ python nvs_partition_gen.py --input ./nvs_initial_values.csv --output nvs.bin --size 0x6000

Creates a binary image for the NVS partition containing initial values as specified in nvs_initial_values.csv


Remember to change 'AWS IoT Certificate Source' from 'Embed into app' to 'Load from NVS partition' 

$ make menuconfig
