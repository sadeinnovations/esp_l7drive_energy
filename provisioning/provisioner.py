import sys
import os
import subprocess
import json
import urllib2

thing_name_prefix = 'L7_demo_'

s3_bucket_us_east_1 = 'l7-iot-bulk-provision'
s3_bucket_eu_west_1 = 'l7-bulk-provisioning'
s3_bucket = s3_bucket_us_east_1
bulk_prov_data_file = 'provdata.json'
provisioning_template_file = 'provisioning-template.json'
device_certs_target_file = 'device-certs-raw.json'
role_arn = 'arn:aws:iam::508014774151:role/BulkProvisioningRole'

provision_data = []
serials = []
task_id = ''


def read_file(file):
    with open(file, 'r') as myfile:
        return myfile.read()


def write_file(file, content):
    with open(file, "w") as myfile:
        myfile.write(content)


def remove_file(file):
    if os.path.isfile(file):
        os.remove(file)


def get_process_output(command):
    p = subprocess.Popen(command, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return p.communicate()[0]


def clear():
    del provision_data[:]
    remove_file(bulk_prov_data_file)

    for serial in serials:
        remove_file('{0}.csr'.format(serial))
        remove_file('{0}.key'.format(serial))

    del serials[:]


def add(device):
    cleaned_device = device.replace(':', '')
    get_process_output(
        'openssl req -new -newkey rsa:2048 -nodes -keyout {0}.key -out {0}.csr -subj \"/C=FI/ST=VS/L=Salo/O=SADE Innovations/CN=SW\"'.format(cleaned_device))
    csr = read_file('{0}.csr'.format(cleaned_device)).replace('\n', '')
    row = '{{ "ThingName": "{0}{1}", "SerialNumber": "{1}", "CSR": "{2}" }}\n'.format(
        thing_name_prefix, device, csr)
    provision_data.append(row)
    serials.append(cleaned_device)
    print('{0}'.format(''.join(provision_data)))
    write_file(bulk_prov_data_file, '{0}'.format(''.join(provision_data)))
    create_nvs_file(device)


def upload():
    cmd = 'aws s3 cp {0} s3://{1}/'.format(bulk_prov_data_file, s3_bucket)
    print(get_process_output(cmd))


def execute():
    global task_id
    cmd = 'aws iot start-thing-registration-task --template-body file://{0} --input-file-bucket {1} --input-file-key {2} --role-arn {3}'.format(
        provisioning_template_file, s3_bucket, bulk_prov_data_file, role_arn)
    output = get_process_output(cmd)
    j = json.loads(output)
    task_id = j['taskId']


def status():
    global task_id
    print(get_process_output(
        'aws iot describe-thing-registration-task --task-id {0}'.format(task_id)))


def errors():
    global task_id
    output = get_process_output(
        'aws iot list-thing-registration-task-reports --task-id {0} --report-type ERRORS'.format(task_id))
    j = json.loads(output)
    link = j['resourceLinks'][0]
    response = urllib2.urlopen(link)
    content = response.read()
    print(content)


def get_certs():
    global task_id
    output = get_process_output(
        'aws iot list-thing-registration-task-reports --task-id {0} --report-type RESULTS'.format(task_id))
    j = json.loads(output)
    link = j['resourceLinks'][0]
    response = urllib2.urlopen(link)
    content = response.read()
    write_file(device_certs_target_file, content)
    print('Done')


def create_cert_file(serial, certificate_pem):
    cert_file = '{0}.pem.crt'.format(serial).replace(':', '')
    write_file(cert_file, certificate_pem)
    print('Certificate file "{0}" for thing "{1}" created.'.format(
        cert_file, serial))


def create_nvs_file(serial):
    content = []
    clean_serial = serial.replace(':', '')
    device_key_file = './{0}.key'.format(clean_serial)
    device_cert_file = './{0}.pem.crt'.format(clean_serial)
    aws_cert_file = './aws-root-ca.pem'
    server_cert_file = './server_root_cert.pem'
    content.append('key,type,encoding,value\n')
    content.append('manufacturing,namespace,,\n')
    content.append('serial,data,string,{0}\n'.format(serial))
    content.append('deviceKey,file,string,{0}\n'.format(device_key_file))
    content.append('deviceCert,file,string,{0}\n'.format(device_cert_file))
    content.append('awsRootCert,file,string,{0}\n'.format(aws_cert_file))
    content.append('serverCert,file,string,{0}\n'.format(server_cert_file))
    content.append('storage,namespace,,\n')
    nvs_csv_file = 'nvs_{0}.csv'.format(clean_serial)
    write_file(nvs_csv_file, '{0}'.format(''.join(content)))
    print('NVS CSV file "{0}" for "{1}" created.'.format(nvs_csv_file, serial))


def generate_nvs_partition_binary(serial):
    cmd = 'python nvs_partition_gen.py --input ./nvs_{0}.csv --output nvs_{0}.bin --size 0x6000'.format(
        serial).replace(':', '')
    print(get_process_output(cmd))


def partition():
    content = read_file(device_certs_target_file)
    lines = content.split('\n')
    for line in lines:
        if len(line) > 0:
            response = json.loads(line)['response']
            thing = response['ResourceArns']['thing']
            pem = response['CertificatePem']
            thing_name = thing[thing.rfind('/')+1:]
            serial = thing_name.replace(thing_name_prefix, '')
            create_cert_file(serial, pem)
            generate_nvs_partition_binary(serial)


def main():
    print('-=PROVISIONER=-')

    while True:
        line = raw_input('>> ')
        tokens = line.split(' ')
        cmd = tokens[0].lower()
        if cmd == 'clear':
            clear()
        elif cmd == 'add':
            add(tokens[1])
        elif cmd == 'upload':
            upload()
        elif cmd == 'execute':
            execute()
        elif cmd == 'status':
            status()
        elif cmd == 'errors':
            errors()
        elif cmd == 'getcerts':
            get_certs()
        elif cmd == 'partition':
            partition()
        elif cmd == 'q':
            exit()
        else:
            print('Unknown command \'{0}\''.format(cmd))


main()
